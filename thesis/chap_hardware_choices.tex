\chapter{Optical Elements}
\label{sec:hardware}
Finding an optical element that is both easy to interface, cheap to get, and can provide eye-like vision is a tough task. In this chapter we will look at a few of the choices available to us, and then look at the OV7670 camera module in more detail.

The component of our choosing needs to satisfy a few constraints:

\begin{description}
 \item[Capture light.] The element in question needs to be able to capture the light in form of a $(r,g,b)$ pixel array

 \item[Frame rate.] The device needs to be able to capture the data at $\geq$ 30\,Hz -- the more the better. This is a difficult constraint for most webcams available on the market today, as most available units are limited to $25$ and $30$\,Hz
 
 \item[Price.] The element of choice needs to be reasonably priced: If one element already costs 1\,\textsterling, and we need 500 units, then we would require $500$\,\textsterling~ alone for the optical elements (excluding any controllers and additional electronics), which is well past our self-set limit of roughly $200$\,\textsterling.
 \item[Availability.] The hardware should be easy to acquire, and shouldn't rely on special one-off manufacturing or specialized prototypes. We only want to use off-the-shelf parts.
\end{description}

With these constraints, only a few kinds of sensors remain: The three obvious choices are either $(a)$ using an optical image arrays such as e.\,g. used in optical mice, $(b)$ using single optical diodes, or $(c)$ using miniature camera modules.

\section{Optical Receptor Arrays}
\IMAGE{optical_arrays}{0.45}{A small array of optical elements as can be found in e.\,g. optical mice in form of a specialized sensor chip which provides extraordinary high frame-rates $\geq 2000$\,Hz}
After some research (see \cite{irp}), we came to the conclusion that optical arrays as used in mice would provide the most reasonable approximation to our use case: They are made up of multiple elements, thus make it easier to actually build up a prototype since we would only have to deal with e.\,g. 20 instead of 500 elements, and every array by itself has a low resolution that does not put us way beyond the 500 pixels of the ant-eye; thus giving us a reasonable bandwidth to deal with. They also provide extra-ordinarily high frame-rates in the region of $\geq 2000$\,Hz. An example of this type of sensor can be see in figure \ref{fig:optical_arrays}.

Unfortunately, however, it turns out that these sensors are no longer being sold to retail customers anymore, and are hard to get in low quantities. Fitting customized lenses and filters on those sensors might have also been problematic, since they do not provide any hardware means of doing so. Thus, alternatives had to be found.

\section{Individual Optical Receptors}
Another approach is to use single optical receptors (e.\,g. RGB ambient light detectors), or maybe even just inversely connected LEDs as a basis for the ommatidia, and to create a fully compound eye from them. 

\IMAGE{ledmatrix}{0.45}{Connecting receptors in form of a matrix can save a lot of pins, but requires a sequential readout}

Connecting five-hundred separate sensors directly to a board would require a lot of pins, and would result in a high routing effort. To simplify this, however, one could e.\,g. connect all LEDs/sensors in form of an LED matrix (see figure \ref{fig:ledmatrix}). By doing this, and using an interleaved pixel-by-pixel readout technique, we would only require $2\cdot \sqrt{500} \approx 44$ pins. So in theory such an approach can be realized.

One issue with this approach though is the fact that these sensors would not have any special lens by themselves, so the light they would capture would be unfocused, and presumably would also overlap with the areas captured by the other diodes, which would violate the constraint of providing a sparse pixel image.

Cost is also of special consideration in this approach: Even if every optical receptor only costs half a pound, it would require extensive effort and space to connect five-hundred sensors to the same control board (special voltage regulators, shift registers, resistors).

While this design approximates the real ant eye best, soldering up 500 elements by hand, and aligning them well enough has been deemed unreasonable. Due to the limited resolution it would also be difficult to determine the alignment error if one occurs.

\section{OV7670 Camera Module}
\IMAGE{ov7670}{0.3}{The OV7670 CMOS camera module provides a $(640,~480)$-pixel VGA output, and uses a small $0.5$\,mm 24 pin flat ribbon connector}
While camera modules provide a much higher resolution compared to what we actually need, they simultaneously also satisfy many of the other requirements we require: They already contain an optical imaging array that saves us the work of installing many individual sensors, and they already contain optics such as lenses that are pre-calibrated and useful for gathering a coherent, "real" image instead of just sensing the ambient lighting levels. A camera module is thus an obvious choice for this project.

The cheapest and smallest module we found was the OV7670 camera module by OmniVision (figure \ref{fig:ov7670}), which can be bought in quantities from Chinese eBay sellers, and also on most hobby-electronics pages.

The camera itself provides a $640 \times 480$ pixel image at a maximum frequency of $30$\,Hz. It can be configured using the I$^2$C protocol (Two-Wire protocol) by reading/writing data in its configuration registers, and outputs data continuously on 8 parallel data lines as long as an input clock signal is applied. The module requires two fixed voltages to work: $1.8\,V$ for the digital core, $2.8\,V$ for the analog part, and a third (custom choose-able) IO voltage for the output pins. The module has 24 pins in total, of which at least 20 have to be connected for normal operation.

Testing of this module and early prototyping was done by using a slightly larger version of this camera (figure \ref{fig:ov7670_dip}) that has a different hardware pin-out, but provides the same camera chip internally, and which could be used on standard breadboards.

\begin{figure}[ht]
\centering
\begin{minipage}[b]{0.45\linewidth}
\IMAGEx{ov7670_dip}{1.0}{A larger version of the OV7670 that is easier to interface}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
\IMAGEx{fpc_breakout}{0.97}{Breakout Board for the 0.5mm Flat Ribbon Connector.}
\end{minipage}
\end{figure}

\subsection*{Pinout}
A disavantage of cheap modules bought from unknown sellers on eBay is documentation: While the OV7670 module itself is reasonable well documented (aside from mistakes in the data sheet such as e.\,g. an inverted RESET pin logic), the pin-out of the physical module itself is not documented at all. Most of the sellers did not publish any pin information at all, and none would not respond to questions. After extensive research for similar sold products three different pin-outs were found on the internet, one of which could be excluded by closely examining the signal paths on the flat ribbon connector and by looking at unconnected pins.

As figuring out the real pin-out was a necessity before designing a camera board, a prototype breakout board was quickly designed, and later milled in the workshop of the University of Edinburgh. The result can be seen in figure \ref{fig:fpc_breakout}.

By probing the different signals, and interfacing the I$^2$C port of the camera, the pin-out as seen in table \ref{tab:pinout} was determined for the "V1.0 BLX" hardware module.

\begin{table}[h]
\begin{tabular}{c|l|l}
 \# & Pin Name & Description  \\ \hline \hline
 1 & - & Not connected \\
 2 & AGND & Analog Ground \\
 3 & SIO\_D & I$^2$C Data \\
 4 & AVDD & Analog Power ($2.8$\,V) \\
 5 & SIO\_C & I$^2$C Clock \\
 6 & RESET & Resets the controller (active low) \\
 7 & VSYNC & Clocked on each completed frame \\
 8 & PWDN & Power-saving mode (active high) \\
 9 & HREF & Low on each completed row \\ 
 10 & DVDD & Digital Power ($1.8$\,V) \\
 11 & DOVDD & Digital IO Power (e.\,g. $2.8$\,V) \\
 12 & Y7 & Data bit 7 \\
\end{tabular}
\begin{tabular}{c|l|l} 
 \# & Pin Name & Description  \\ \hline \hline
 13 & XCLK & Clock input ($24$\,MHz) \\
 14 & Y6 & Data bit 6 \\
 15 & DNG & Digital Ground \\
 16 & Y5 & Data bit 5 \\
 17 & PCLK & Clocked on each pixel \\
 18 & Y4 & Data bit 4 \\
 19 & Y0 & Data bit 0 \\
 20 & Y3 & Data bit 3 \\
 21 & Y1 & Data bit 1 \\
 22 & Y2 & Data bit 2 \\
 23 & - & Not connected \\
 24 & - & Not connected
\end{tabular}
\caption{Pin-out of the OV7670 camera module as sold on eBay}
\label{tab:pinout}
\end{table}

\subsection*{VGA Signal Format}
\IMAGE{ov7670_vga}{1.0}{OV7670 VGA Signal Output: On each frame VSYNC, on each row HREF, and on each pixel PCLK is clocked.}
\IMAGE{ov7670_vga_scan}{0.66}{VGA Scan pattern: Data is presented row-wise.}
\IMAGE{ov7670_row_gap}{1.0}{The actual signal is uneven with large gaps between data blocks, and thus increases the immediate required signal processing frequency capability of the attached controller.}

The camera module outputs data in the VGA signal format as can be seen in figure \ref{fig:ov7670_vga}: Pixel by pixel, row by row, and frame by frame with corresponding \textit{PCLK}, \textit{HREF} and \textit{VSYNC} signals in between. The corresponding vga scan pattern can be found in figure \ref{fig:ov7670_vga_scan}: For each byte of data the data lines $D0$ to $D7$ are set to their appropriate value, and a $LOW$-pulse is presented on the \textit{PCLK} line. When a full frame has been posted to the data line, the \textit{VSYNC} line is toggled once.

One important aspect to notice here is that the spacing between \textit{HREF} and \textit{PCLK} pulses is very uneven: Between each row there is a break of undefined distance that is significantly larger than the pixel-clock period. Unfortunately this aspect is not mentioned anywhere in the data sheet, and has caused us trouble when trying to capture all the data: Instead of evenly making use of the entire clock domain, the output signal is sent at a high frequency within a row (there is zero "unused" delay between two pixels in a row) with fairly large gaps between each row (see figure \ref{fig:ov7670_row_gap}).


Due to this, a frame rate that would require a $10$\,MHz signal when evenly spaced can easily need an actual $40$\,MHz readout frequency (during the active row scan) when these gaps are present. Since these gap width unfortunately cannot be adjusted, the maximum resolution of the image our camera board can process has been limited by this circumstance.

\subsection*{Color Format}
The color format of the camera is adjustable. We decided to go for the RGB565 format. This format requires two bytes of data for a single pixel. For each of the byte pairs, the first 5 bits correspond to the red channel, the next 6 bits to the green color channel, and the remaining 5 bits to the blue channel (see figure \ref{fig:rgb565}).

\IMAGE{rgb565}{0.7}{The RGB565 data format uses two bytes for a single pixel}

The data can be recovered by using simple bit-shifting math:

\begin{eqnarray}
\text{rawR} = & (d_1 >> 5) & \\
\text{rawG} = & ((d_1 ~\&~ \text{0b111}) << 3) ~|~ (d_2 >> 5) \\
\text{rawB} = & (d_2 ~\&~ \text{0b11111}) &
\end{eqnarray}

And can then be transformed to the standard RGB888 color space by shifting the bits correspondingly:

\begin{eqnarray}
R = & rawR << 3 \\
G = & rawG << 2 \\
B = & rawB << 3
\end{eqnarray}

A slightly better approximation can be achieved when also valuing the lost lower two or three bits of the color domain by multiplying in the incoming values. The difference, though, is hardly noticeable and not necessarily worth the additional complexity.

\subsection*{Filters: Taking apart a camera}
As previously mentioned, we know that ants have specialized ommatidia that are able of sensing UV or polarized light. We were interested if the OV7670 camera modules can be altered so that such light can be detected, so we took apart one of the cameras (figure \ref{fig:ov7670_apart}).

It turns out that the round lens is only glued in place with a weak glue, and can be simple screwed off with a set of tweezers. It can also be seen that the default lens has an IR-blocking filter (see figure \ref{fig:ov7670_lens}) at the bottom that can be removed by applying some mechanical force. This can be of interest when using the compound eye for using e.\,g. IR light as a means of navigating. A customized filter could also be placed in the same spot where the IR filter was previously located.

Using a customized filter is of relevance when trying to use polarized light as a means of determining orientation: In this case a selected set of cameras would be fitted with differently polarized filters, thus only detecting certain light waves. By removing the IR filter, and re-applying a polarized filter this should -- in theory -- be possible on these modules.

\IMAGE{ov7670_apart}{0.80}{A OV7670 camera module taken apart: The imaging sensor can be seen on the left, the de-screwable lens on the right.}
\IMAGE{ov7670_lens}{0.30}{The default lens of the OV7670 has an IR filter (red-ish) which can be removed by using a small knife.}

