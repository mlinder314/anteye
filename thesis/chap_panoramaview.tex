\chapter{Camera Sphere}
\label{sec:panorama}
\label{sec:sphere}
So far we have captured the images from all of the single cameras, merged them into one large image in our merge board. We transferred the resulting image to a connected computer using USB, and can visualize it there using a simple API. But at this point the images are just aligned by their camera ID -- we haven't worried about how those images should be correctly placed in our virtual world; how we actually visualize them in a meaningful way.

While the simple merged image is good enough for techniques that e.\,g. just use frame fingerprinting, and do not require a sense of spatial continuity, a continuous image clearly has its advantages. To do this, we first need to worry about how we can physically align all our 16 cameras so that our cameras imitate the ant eye.

\section{Modelling the Ant Eye}
Several approaches have been considered for modeling the ant eye: We know that the two compound ant eyes has a vision close to the 360\textdegree~ field of view, and we know that the ommatidia are allocated in a sparse, non-overlapping fashion. In theory we would just have to create two single eyes of exactly the same shape and form of the ant-eye, place the cameras at even intervals, and we are done.

In practice this won't work as easily though: Instead of having 500 ommatidia, we barely have 16 camera modules, and they are not even squarely shaped, but rectangular (4:3 aspect ratio) instead. Additionally we have to consider the space the camera board itself is taking up, and the flexibility of the $1$\,cm flat ribbon camera connector.

\subsection{Two eye segments}
We have experimented with two possible layouts. The first approach was indeed about modeling a single eye, and using a ball-pit ball as a basis. Holes have been burned into this ball-pit ball half so that 9 cameras could be placed in them at an evenly spaced distance (evenly between all closest points). The ball base used can be seen in figure \ref{fig:ballpit_half}.

\begin{figure}[htbp]
 \centering
 \begin{minipage}[b]{0.41\linewidth}
   \IMAGEx{ballpit_half}{1.0}{The ball-pit ball half proposed for our spherical base.} 
 \end{minipage}
 \quad
 \begin{minipage}[b]{0.5305\linewidth}
   \IMAGEx{ballpit_assembled}{1.0}{The first fully assembled prototype: Cameras are attached in close proximity of each other.}
 \end{minipage}
\end{figure}

As this prototype was built by hand, and since the material of the ball-pit ball is a bit flexible, the resulting shell is not one hundred percent accurate. A better prototype could potentially be acquired by e.\,g. 3D printing a tight base. This, however, was not done due to the short length of the project.

The fully assembled prototype can be seen in figure \ref{fig:ballpit_assembled}. The good thing about this prototype is that the camera connector sockets do not need to be twisted in any way, thus the connections are very reliable, and no mechanical tension is present. Unfortunately, this also has one disadvantage: In order to fit those cameras in the tight space, the boards had to be rotated around the camera center axis. Each of the $(40, 30)$ pixel frames is thus rotated at a different angle, causing us to have to perform lots of rotations in software to get a continuous image.

A more severe disadvantage of this prototype is space usage: While the eye itself is very small, and cameras are close to each other (thus giving us smaller gaps between all ommatidia), the camera boards themselves take up a lot of space. While this works for a single camera board, it prevents us from putting two eyes close to each other without having overlapping/colliding boards. Even when rotated slightly to create an overlapping front vision and a blind spot in the back, the distance between the eyes is still roughly two times the size of a single eye. For a compound image the unevenly large offset between the two eyes is unacceptable. 

\subsection{Spherical Prototype}
A different solution was devised: Instead of modeling two single eyes, we decided to go with a model that combines both eyes into one single spherical prototype. While this is not as close to the real ant as the other prototype, it does allow us to not worry about the camera board position as much. Additionally we were also able to fix all cameras in a 0\textdegree~ rotation fashion (i.\,e. all cameras are rotated with the same angle), which made post-processing the images  easier.

The prototype was created from scratch using spare wire: It is made up out of 3 circles along the X, Y, and Z axis, and two more circles along the vertical rotation axis that were used for attaching cameras. The resulting prototype can be seen in figures \ref{fig:camera_sphere} and \ref{fig:camera_sphere2}.

\IMAGE{camera_sphere}{1.0}{The spherical prototype: Cameras were attached in 45\textdegree~ angles apart using hot glue.}
\IMAGE{camera_sphere2}{0.75}{Camera boards where "hidden" in the inside of the prototype. The connectors had to be twisted slightly in some instances.}
\IMAGE{camera_sphere_layout}{0.7}{Alignment of the 16 cameras on the camera sphere; colors represent differently oriented circles. The horizontal plane (red) was favored with more cameras.}

In total 16 cameras were attached to the board; all of them in 45\textdegree\, angles distance on the circle they were attached to. 7 were put on the middle row (thus only leaving a gap in the back/the blind spot), and three each on the vertical circles at $-45$\textdegree, $45$\textdegree\, and $90$\textdegree. A few blind-spots in the back remained since we did not have enough camera modules remaining, but they were placed in areas consistent with the ant eye. The resulting alignment can be seen in figure \ref{fig:camera_sphere_layout}.

The $45$\textdegree\, angle was chosen since each of the camera modules has a horizontal field of view of that value\footnote{Which also supports our approach of aligning all cameras at the same rotation angle}. The suppliers page of the module stated that the modules have a $67$\textdegree,FOV, but they failed to mention that this was measured along the diagonal of the module instead of the horizontal. This meant that the gaps between the ommatidia ended up larger than expected, and no overlap is happening at all -- except for those cameras on the far left/far right.

\section{3D Visualization}
A simple 3D panorama application was developed using OpenGL\footnote{The OpenTK .NET implementation of the OpenGL wrapper was used for this}. This application takes the incoming camera images, transforms them into OpenGL textures, and draws them at their corresponding "should-be" location on the  camera sphere.

For each camera we take a custom $(\text{Yaw}, ~\text{Pitch}, ~\text{Roll})$-tuple as parameter, and draw a camera plane at a fixed distance that has been rotated by the rotation matrix produced by the given tuple. The camera plane, which used to be a flat quad initially, was later replaced by a more detailed rounded/bent mesh that bends in the spherical shape of the camera sphere. This has been done to correct for the effect of the lenses of the camera, and to get a more continuous image.

An example picture from this application can be seen in figure \ref{fig:panorama}. The view can be freely rotated around using the mouse. Here only a field of view of roughly $90$\textdegree\, angles is visible at any given time. Higher FOVs were tried, but were found to distort the image too much.

\IMAGE{panorama}{1.0}{3D Panorama View: All 16 cameras are mapped onto the camera sphere at their "should-be" position. A laptop (middle) and a plant (middle-right) can be seen in the displayed frames.}

\section{Calibration of the Camera Sphere}
As the prototype of the camera sphere was created by hand, inaccuracies are bound to happen: Some cameras are not exactly $45$\textdegree\, from each other, and others are rotated slightly wrong. This is especially visible when looking for straight lines that go across multiple cameras (see figure \ref{fig:panorama}): An extra offset calibration is necessary to ensure that these lines match, and that images are seamless.

\subsection{Manual Calibration}
The easiest and most effective way of calibrating that we could think of was by making use of students, and having them manually align the camera images by hand. This actually seemed like a viable option since there are only 16 cameras that can be calibrated in pair-wise fashion, and since there is only one prototype in existence. For this purpose special shortcuts ('1' \dots '6', 'A' \dots 'Z') were added to the panorama view application that allow the user to move single camera planes around all axis until they appear seamless. The calibration achieved by this was used as a ground truth for our further, automatic algorithms.

\subsection{Challenges}
There are a few kinds of common calibration errors that occur along cameras. The most common ones are simple offsets along the Yaw, Pitch or Roll values -- this happens if a camera is placed on the circle, but not exactly in the position where it was intended to land. A simple \textbf{rotational error} occurs.

There are, however, also more complex situations: In some circumstances the circle itself might be bent, and will cause the camera to be misplaced zoom-wise, causing a \textbf{distance error}. This can be detected only by looking at multiple camera images at once, and by comparing object scales between each. This, however, turns out to be pretty difficult in a small $(40,~30)$ pixel image.

The hardest kind of error to fix is a \textbf{skew error}. This happens if the circle itself is not actually a circle, but has a little bent/dent, and thus causes the camera to be locally rotated around its attachment point. Instead of facing in the direction of the vector $\overrightarrow{center_{camera}} - \overrightarrow{center_{sphere}}$, the camera direction is then rotated into any other arbitrary direction. Since this causes the camera to produce a similar view to that of the one that is \textit{actually correctly} facing in that direction, just slightly shifted by a few millimeters, there is no "nice" way of fixing this error visually. Any attempts to correct for this would cause us to loose our continuous camera sphere.~\\

\noindent Most of these errors are either unlikely or un-fixable, or their effect is so small that it can just be safely ignored. This is why we will only focus on the rotational errors that were mentioned initially, and that are most likely to occur.

\subsection{Automatic Calibration}
We were interested in whether the calibration could also be performed automatically. For this a simple calibration technique was designed. Several approaches were considered, a few of them were: \textbf{(a)} A fixed checkerboard pattern, \textbf{(b)} Standard image stitching solutions, \textbf{(c)} rotational calibration techniques that would rotate around the sphere in a fixed angle, and \textbf{(d)} simple pattern-based pair-wise camera pose-estimations.

A simple checkerboard pattern was found to be too generic/unsuitable since we would not know how many blocks were lost in the gaps between the cameras; similar goes for image stitching algorithms, which won't work if there isn't a significant overlapping image portion. Rotational/mechanic calibration methods were not further investigated since they would require significant stabilization effort and electronics to prevent noise. Thus the pattern-based solution remained as the way to go.

\IMAGE{calibration_testpattern}{0.8}{The calibration test pattern used for our pose estimation: Two parallel black lines with a width of $1$\,cm that are exactly $2$\,cm apart, and have a length of $15.3$\,cm. All on a white background.}

We devised a calibration algorithm that would try to detect a certain line pattern (see figure \ref{fig:calibration_testpattern}) in each of the camera images, and that would perform pair-wise matching and calibration based on this pattern. By averaging the detected calibration offsets, and calculating a transitive chain between all cameras, rotational offsets to a center/root camera (in this case the middle camera), which was left unchanged, were determined.

The algorithm makes some simplifying assumptions about the environment, which are generally true due to the selectiveness of our pattern detection. For one we assume that the pattern is held in a slightly bent way that directly faces the camera (this results in parallel lines), and at a semi-fixed distance (since we do not have many pixels to work with, and we can only recognize the lines if they have a certain minimum size, but are yet still completely visible). It does not assume any concrete spatial arrangement of the cameras. All pose estimations are done independently of any known previous spatial location, and the only constraint comes from the size of the calibration pattern, which - due to its size - can only be seen/detected in two cameras at any time.

As mentioned before, we also assume that there is only a rotational error in the calibration offsets. We can thus only detect and fix this error; other errors will cause the calibration to go wrong.

\subsubsection{Pattern detection}
The first step of the detection is the pattern recognition. For each camera we try to find our test pattern. We do so by making use of the OpenCV toolkit, and by employing a few well known filters and algorithms. The steps of the algorithm are listed below, and can also be seen in figure \ref{fig:calibration_pattern_detection}:

\begin{enumerate}
  \item Converting the image to grey scale, and blurring it to remove noise
  \item Running the Canny Edge Detector
  \item Running Line detection (line continuation) on the detected edges
  \item Filtering out lines that do not exit the the image, or exit the image on both sides
  \item Remove duplicates (lines that are too close together)
  \item Find pairs of lines that are parallel to each other, have similar length, and that have a black pixel in the middle
  \item Find pairs of pairs of lines whose distance is twice that of the inner-pair distance (relationship given by test pattern)
\end{enumerate}

\IMAGE{calibration_pattern_detection}{1.05}{Pattern detection; step description can be seen in the top of each frame. In the end only one (red text) potential match/detection remains} 

\noindent By performing these step, usually either none or just a single  detected test pattern remains. This detection can then be used for the pose estimation.

\subsubsection{Pair-wise pose estimation}
If a test pattern is detected in two images at a similar time ($|\Delta\, t| < 5 ~\text{frames}$), a match between the two images is made and recorded. After enough matches between two cameras exist, a pose estimation between the camera pair is attempted.

To estimate the pose of a camera $A$ in relation to a camera $B$, we make use of a few characteristics of our calibration pattern: It is our goal that the shown image is continuous, thus the line in one image should be matched straight on another (without rotation offsets and without any gaps). It is also our goal that we match the scale of the whole line: The detected line should always have an exact length of $15.3$\,cm.

\IMAGE{calibration_vectors}{0.8}{Pair-wise Pose Correction: We first correct the rotational error (top right), then fix both translation offsets along the orthogonal and the line axis itself (bottom).}

The basic principle of our pose estimation can be seen in figure \ref{fig:calibration_vectors}: We first fix our rotational offset by rotating the one camera plane so that both lines face in exactly the right direction. Then we estimate the offset along the orthogonal of the camera line by using a vector projection. To find the offset along the line itself, we look at the $(\text{length}:\text{width})$ ratio of each line, calculate an estimated total line length sum across both cameras, and compare this with our should-be value. This gives us three different values: $(\Delta\,\text{rotation}, ~\Delta\,\text{line}, ~\Delta\,\text{line}^{-1})$.

We do this calculation in the 3D world space, and then use the inverted world matrix to calculate an offset in Yaw, Pitch and Roll that can be directly applied to the camera location tuple, and that is saved for further processing. The result of this can be seen in figure \ref{fig:calibration_pose_diff}): One can see that there is still a minor offset along the orthogonal offset. This one to two pixel offset is a result of the flakiness of the OpenCV line detection when using low-resolution images.

\IMAGE{calibration_pose_diff}{1.0}{Effect of pose estimation: Original (left), Camera 'N' calibrated (right)}

\subsubsection{Graph-based recursive relocation}
Now we are able to figure out the transformation matrices between two camera pairs. This, however, does not give us a fully calibrated camera sphere yet. To correctly calibrate all cameras, we need to determine which camera remains a fixed point in each pair-wise calibration, and which one is moved around.

It makes senses to choose a center point (e.\,g. the middle camera), and to calibrate all cameras towards this point. If we use this pattern, the camera always closer to the center camera can be considered a fixed point. By turning the set of available pair-wise calibrations into a graph, we can use graph-fill algorithms for setting the proper fix-points for each calibration (figure \ref{fig:graph_fill}).

\IMAGE{graph_fill}{1.1}{Flood fill of the calibration graph. Pairs are first grouped into connected sub graphs, and then iteratively filled from/calibrated to the center node (here: 'A')}

\noindent After the calibration graph is known, we have the closest transitive chain from each node to the camera root node. By simple multiplying the transformation matrices in proper order we can then calculate the total calibration matrix for each camera.

\IMAGE{calibration_in_action}{1.05}{Gathering Calibration Data in practice: The number of detected patterns per camera can be seen in the middle, and the number of joint/correlated detections in the right image. The whole calibration is a lengthy process since a full transitive chain from the root node to any other node has to be established.}