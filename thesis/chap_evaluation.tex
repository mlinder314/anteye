\chapter{Evaluation}
\label{sec:eval}
In this chapter we will evaluate the performance of our built compound eye prototype, and look at how well we achieved the goals we initially aimed for. Where possible, we will do this for each of the separate sub-projects individually.

\section{Achieved Objectives}
First, we look at all objectives we planned on in our research proposal, and comment on each one of them:

\begin{description}
  \item["Build a hardware chain representing the eye":] We successfully created a hardware chain consisting of a camera sphere, camera boards, a merge board, and a computer that allows us to capture compound images and visualize them. The camera sphere itself is not as close to the real ant eye as one might like (especially size and form wise), but the general idea of using multiple ommatidia to create a full image has been captured. 
  
  \item["Capture Sparse Images at 30 Hz":] We are able to capture a combined image with gaps in between the cameras at a frequency of $30$\,Hz when using a $(20,15)$ pixel resolution per camera, thus still exceeding the set limit of $500$ pixels by a factor of $19.2$ at the proposed frame rate. The projection/alignment of the images has been done in software on the computer instead of running this natively on the hardware due to the lack of memory on the micro controllers. A simple pixel-select-and-assign algorithm\footnote{Specifying a list of pixels to select from each camera to build a combined image} was however implemented (although unused) in the camera boards and merge boards, thus still giving one the option to do this. We will talk about the achieved frame-rate in more detail in section \ref{sec:framerate}
  
  \item["Close to 360\textdegree\, Vision":] We manage to achieve a panorama view of roughly $270$\textdegree\, with our cameras with some blind spots in the back area. This is mostly due to the low number of cameras (16) instead of originally planned 20 or more cameras. The camera amount was decreased to be able to take advantage of a \textit{power-of-two} number of cameras, which simplified bus logic and memory mappings. Due to the limited FOV of the cameras the image is a bit more sparse than initially imagined, but we still manage to generate an image that gives a good sense of the environment.
  
  \item["Align Optical Elements to imitate an ant-eye":] Initially we planned on producing a 3D-printed mesh that would hold the camera cells. This idea was discarded due to a lack of time, and a hand-made wire prototype was used instead. While this prototype also provides the general notion of an eye, it is far from perfect and contains lots of mechanical/alignment inaccuracies. On the plus side, though, it allowed us to test an automatic calibration algorithm. This area could, however, definitely be improved further upon.
  
  \item["Calibrate the prototype":] We showed that it is possible to develop an automated calibration algorithm that performs reasonably well under certain assumptions, but that cannot fix all kinds of errors (especially not skew errors caused by a bent spherical base). It also became apparent that performing a manual calibration for a single prototype is much quicker/reliable than automating this process. Since our self-built pattern recognition was very selective about our test pattern, a lot of patience and fine hand-movement is required to calibrate the eye automatically.
\end{description}

\noindent We can see that we achieved most of our set goals to a satisfying level. There are always more improvements that could be made if more time was available, but since this was not the case, we instead wanted to deliver a solid prototype that would perform well, and that can run continuously without interruption.

\section{Frame-rate Statistics}
\label{sec:framerate}
As the achieved maximum frame-rate has been a constant thought during the development of this project, and since the final frame-rate was only possible through the many performance optimizations made in the firmwares of the controller, it makes sense to show how well each of the parts performs, and where the bottleneck lies. We created a small table for this purpose (table \ref{table:framerate}).

\begin{table}[h]
\centering
\begin{tabular}{r|c|c|c|c}
Resolution \textit{[px]}  & $40 \times 30$ & $20 \times 30$ & $20 \times 15$ & 500 pixels$^*$ \\ \hline \hline
OV7670       &  $30$\,Hz      &  $30$\,Hz      &  $30$\,Hz      & --   \\ \hline
Camera Board &  $20$\,Hz      &  $40$\,Hz      &  $40$\,Hz      & --   \\ \hline
Merge Board  &  $10.3$\,Hz    &  $20.5$\,Hz    &  $40.9$\,Hz    & $390$\,Hz \\ \hline
USB to PC    &  $22.17$\,Hz   &  $44.3$\,Hz    &  $88.4$\,Hz    & $848$\,Hz          
\end{tabular}
\caption{Maximum possible frame rates for each component and resolution. Values for "$500$ pixels" are theoretically calculated, not measured.}
\label{table:framerate}
\end{table}

Our implementation seems to be performing reasonably well, but not great at higher resolutions like $(40,~30)$, but gets really fast once we scale the image down further, or only process the $500$ elements the ant eye should have. Since no DMA controller was used in the camera board, the speed of the camera board is limited by the width of the OV7670 image output. The overall bottleneck in the infrastructure is primarily the camera itself (as it only can produce images at $30$\,Hz; higher frequencies result in an all-black image), and the merge board, as here 4 cameras are connected to a single bus, thus creating a congestion point. Increasing the bus-speed would be an option, but is not possible due to the limited CPU power of the camera boards. A different improvement would be to add another fifth or sixth UART bus, which would be possible since the controller used has 6 UART ports in total\footnote{One of which is used as a debug port at the moment}.

% USB bus performance
Another interesting statistic to look at is the efficiency of the USB implementation used. We looked at the total throughput achievable through different USB sticks, and found that a standard USB to serial converter was only able to provide a fraction of the speed that our controller could provide (table \ref{table:usb}). Performance was measured with a simple C\# application using a simple "echo" test (RX and TX lines were connected together).

\begin{table}
\centering
\begin{tabular}{c|c}
USB Device & Maximum Throughput \\ \hline \hline
CP2102 USB-to-Serial Bridge (echo) & $72.5$\,KiByte/s \\
FT232R USB-to-Serial Bridge (echo) & $113.5$\,KiByte/s \\
Merge Board with USB 2.0 (echo) & $249.7$\,KiByte/s \\
Merge Board with USB 2.0 (one direction) & $874.9$\,KiByte/s \\
\end{tabular}
\caption{Maximum throughput of several USB serial port devices.}
\label{table:usb}
\end{table}

It became obvious that the usual USB to serial bridges are limited by their fixed baud rate, which cannot go above $115200$ baud (and which were thus unusable for our purpose). Interestingly, the bandwidth of our board was also quite limited if a half-duplex bi-directional communication was used. We also presume that this was due to the internal buffer of the micro-controller, which had to be much smaller than the one on the PC side due to limited ram. When replaying a buffer as fast as possible (non-echo), we could achieve $0.9$ MiByte/s, thus getting close to the practical maximum\footnote{The maximum bandwidth that can be achieved if timings and required breaks are used} of $1.2$ MiByte/s for USB 2.0 Full-Speed devices.

\section{Calibration Results}
Putting quantitative data on the calibration results was hard since no ground truth is known, and since skew errors distorted the planes. What we could measure, however, was measure how far our calibrated lines were apart from each other, and how much of total line-length was missing in the captured image by using a ruler/marked sheet of paper as a basis. 

The results from this can be seen in figure \ref{fig:calibration_results} and \ref{fig:calibration_results2}, which were generated by running our calibration algorithm on the different possible (thus neighboring) camera pairs in the same environment at the same time of the day. Multiple samples were gathered for each camera pair, and the pixel-offset has been measured by screen-shots and by using GIMP. Data has been truncated at $20$ pixels offset to create two comparable graphs (no significant peaks or data was lost).

\IMAGE{calibration_results}{0.85}{Distribution of alignment error between camera pairs along the orthogonal axis. The mean of the Gaussian with $\mu = 3$ might be caused by a $\pm 1$ pixel error in the line detection.}
\IMAGE{calibration_results2}{0.85}{Distribution of alignment error between camera pairs along the line axis (error in total line length). The higher error rate relates to the higher number of dependencies (estimated line width and line length).}

One can see that usually a $1$ to $2$px alignment error occurs along the orthogonal axis (presumably due to an incorrect line detection), and a larger alignment error along the line axis happens, as here we rely on both detecting the line length and the line width correctly. Due to the low resolution of our camera image of $(40,~30)$ pixels, the line width in each image is usually between $4$ or $8$ pixels. Since sub-pixels are not measured, inaccuracies quickly accumulate. 

This alignment error could be fixed by e.\,g. using a higher-resolution image during the calibration phase. Since frame-rate is of no issue during calibration, providing a higher-res image should be doable. The remaining constraint, however, is the amount of memory available in each of the camera boards: Since each board needs to store a frame buffer copy of a frame, increasing the size by more than $400\%$ is probably not possible with the current board design.

While these values are still acceptable in a way that the human eye won't see the difference in lengths of the objects, the orthogonal shift is more obvious. Rotational errors were seldom present at all, and can be neglected.

The most flaky part of the calibration is actually the pattern detection itself: Since we rely on finding parallel lines with specific characteristics, we will only recognize the line pattern in less than $7\%$ of the frames where the pattern was actually present. Since we get about $10$ frames per second, this usually results in around one detected sample per second. In practice the hardest part was to get the line pattern to be recognized by two cameras at once at the same time, and not just one or another. The few high-error offsets were also caused by wrong pattern detections when the calibration pattern was not actually present.

\section{Cost of a single camera board}
In our initial motivation we talked about wanting to achieve a low-cost camera board solution using only off-the-shelf components. The total cost of the camera board, which is the most prominent part of the project (since it was duplicated 16 times), has been listed in table \ref{table:cost}.

As we can see the total cost for a single module is around roughly $7$\,\textsterling, resulting in a total of $112$\,\textsterling~ for the camera boards. The merge board and other mechanical components roughly add an additional $16$\,\textsterling, placing us at $128$\,\textsterling, which is still below our self-set limit of $200$\,\textsterling. One should note, however, that this calculation excludes \textbf{(a)} work hours, \textbf{(b)} money spent on experimental hardware that was not used in the final version, and \textbf{(c)} cost for the initial development tools.

\begin{table}
  \centering
  \begin{tabular}{r|c|c}
    Component & Quantity & Price \\ \hline \hline
    OV7670 Camera & 1 & $2.80$~\textsterling \\
    PIC24F32GA002 Controller & 1 & $1.59$~\textsterling \\
    Raw Board & 1 & $0.50$~\textsterling \\
    FPC Socket & 1 & $0.24$~\textsterling \\
    Dual Output Voltage Regulator & 1 & $0.23$~\textsterling \\
    Green LED & 1 & $0.08$~\textsterling \\
    0605 SMD Capacitors & 6 & $0.08$~\textsterling \\
    0605 SMD Resistors & 5 & $0.03$~\textsterling \\ \hline 
    \textbf{Total Cost} & & $6.99$~\textsterling
  \end{tabular}
  \caption{Cost calculation for a single camera board; work hours excluded}
  \label{table:cost}
\end{table}

\section{Testing a navigation algorithm: Visual Compass}
We also wanted to test the performance of an actual navigation algorithm on our compound eye. For this purpose we implemented a simple version of a Visual Compass algorithm (cmp. \cite{visualcompass}), whose purpose it is to estimate the rotation of the camera based on a known frame set over the environment.

The visual compass algorithm takes the pixel-by-pixel difference between two frames, and calculates the sum of differences, thus producing a value that tells us how different two images are:

\begin{equation}
  \delta_{VC} = \sum_{i = 1}^{w \cdot h}{\sum_{c = 1}^3{\left\lvert frameA_{i, c} - frameB_{i, c}\right\rvert}}
\end{equation}

\noindent The expected result of the Visual Compass algorithm is a minimal $\delta_{VC}$ for frames that have the same (or similar) rotation. This holds true even if the ambient lighting changes, or if the camera is held in a slightly different position. While the absolute offset of the entire graph might shifted in this case, $\text{min}(\delta_{VC}$ will still be located at the same frame.

To test the performance of this algorithm on our prototype we developed a simple frame recorder application, and recorded three frame-sets of around $180$ to $200$ images of the camera sphere rotating along its horizontal axis for $180$\textdegree. In those images we also purposely captured noise (people moving, and the rotation axis was not exactly in the middle of the camera).

We then developed a simple visual compass that would do frame-by-frame and pixel-by-pixel comparisons as stated above, and that would output the delta of a previously chosen frame to each of the frames in the reference frame-set. We visualized the results in form of a a 2D graph (figure \ref{fig:visualcompass}), and also compared the gathered results by hand: 
Along the $x$-axis of each diagram one can see the different reference frames (from $0$ to roughly $200$); along the $y$-axis the error in reference to the test-frame used is shown. The notion ``circleC[90] in circleB'' -- for example -- means that the 90th frame of the \textit{circleC} frame-set has been matched against the entire frame-set \textit{circleB} (frames along the $x$-axis), and that the error for each frame is shown in form of a bar. 

\begin{figure}[p]
  \centering
  \begin{minipage}[b]{0.47\linewidth}
    \IMAGEx{compass_circleBref}{1.0}{Test on the same set ($\delta = 0$)}
  \end{minipage}
  \quad
  \begin{minipage}[b]{0.47\linewidth}
    \IMAGEx{compass_circleA}{1.0}{Test on the 'circleA' set}
  \end{minipage}
   \quad
  \begin{minipage}[b]{0.47\linewidth}
    \IMAGEx{compass_circleC}{1.0}{Test on the 'circleC' set}
  \end{minipage}
   \quad
  \begin{minipage}[b]{0.47\linewidth}
    \IMAGEx{compass_circleBinC}{1.0}{Frame from B in set C}
  \end{minipage}
  \caption{Delta-measurements of our test-frame to the reference set. The lowest point marks the correctly estimated direction. (x: frame index, y: color delta, lower is better)}
  \label{fig:visualcompass}
\end{figure}

\IMAGE{compass_progress}{1.0}{Visual Compass delta over time (frames) for the circle C series when matched against circle B: We can see the peak continously moving from the left to right (as expected).}

~\\ \noindent As expected we find our test frame with a zero delta since $|frame_i - frame_i| = 0$, and we also see a continuous drop from and towards that point (figure \ref{fig:compass_circleBref}). 
More interestingly is, however, the performance on the other data sets, which were recorded at different rotation speeds and slightly different camera positions (as the model was moved by hand, and was not fixed to any particular point): In all those graphs we find a clear 'valley' in which the delta is lowest, and which matches the ground truth direction best. This is interesting as this test has been run on the raw camera data (without any scene calibration in use). Even when matching the entire set against another set (see figure \ref{fig:compass_progress}), we always only predict a single valley that progressively moves from the left to the right. The features detected in the low-resolution camera images are apparently still significant enough for a simple $\delta$-algorithm to work.

A concrete direction cannot be deduced from the frameset since the frames were recorded at an arbitrary, changing rotation speed and inaccuracies might have occurred, but we can deduce that the direction of rotation is accurate: If we check the valley location of the frame $i$, and the valley location of the frame $i + 20$, we can see that the valley has moved to the right for about $10\%$ along the x-axis.

~\\\noindent The recorded frame-sets for the visual compass runs, and our visual compass implementation has been included in the electronic submission of this dissertation so that reproducible results can be achieved.

\newpage
\subsection*{2D Panorama}
We also developed an additional simple 2D version of our 3D panorama visualization algorithm that is not completely accurate since it does not correctly distort the input images, but which instead produces approximate results; planes are only aligned in their spatial correct/expected location (figure \ref{fig:visualcompass_input}). 

This version, although first intended for use in a "sliding" visual compass that would also allow for matching frames with slight $(x,~y)$-offsets, was never used as the unaligned results are already very reliable. It can, however, potentially be used for more complex algorithms such as e.\,g. the Optical Flow algorithm, which requires some spatial alignment. 

~\\
\IMAGE{visualcompass_input}{0.8}{A 2D version of the panorama algorithm was created that tries to put the images in their expected spot. Spatial distortion along the Y-axis and overlap-calibration is missing though.}

