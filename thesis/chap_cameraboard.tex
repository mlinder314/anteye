\chapter{Camera Board}
\label{sec:cameraboard}
In this chapter we design a small camera board to which the OV7670 module can be attached. Since many of these boards will be required for the whole project (one per camera), we aim for providing a small, lightweight solution.

\IMAGE{camera_board}{0.95}{The finished camera board: The camera is attached on the left, the central processing board will be attached to the pins on the right using dupont cables.}

\noindent Since this module will be an interface board for our camera module, it will have a few special requirements, and will perform these actions:

\begin{description}
\item[Power the camera module:] The camera module needs at least two distinct voltages. This board should provide the voltages for the camera, and should make sure that no noise/spikes on the voltage lines occur. Special care has to be taken with analog components that do not respond well to noise.

 \item[Configure the Camera:] The module needs to configure the connected camera via the $I^2C$ protocol. We also want to have the possibility of dynamically re-configuring the module later on, so it needs to be able to accept dynamic input.
 
 \item[Asynchronously read out the OV7670 module:] The board needs to read out the frame data at a high frame rate (of up to $30$\,Hz), and to store it in an internal frame buffer. To do so, 8 data pixel lines need to be connected, and three additional input pins for pixel, row, and frame synchronization. The latter should be connected to a pin able to generate interrupts.
 
 \item[Serially stream frames as fast as possible:] Connecting 8 data pins to a controller is okay for one camera module, but not for 16 -- The lack of pins on most controllers would make this impossible. For this purpose we want the camera module to serially stream the data, thus reducing the required pin count from 8 to 1. The data should be streamed as fast as possible.
 
 \item[Scale down the image:] Since 16 full resolution cameras produce a very high amount of data (higher than what can be handled by the USB 2.0 interface), we need to reduce the frame size to a more compact form. This should happen as early as possible on this board to reduce the total bandwidth load on our communication channels.
 
  \item[Respond to simple commands:] Since we have many cameras, we might want to address/talk to them individually. The camera board should provide a command interface that can used for selective communication between the modules.
\end{description}

\noindent The previous requirements are the reason for why we need a single camera board per camera: Connecting a single OV7670 camera module requires about 20 pins to be connected to a micro-controller, and requires read-out speeds in the range of ~24\,MHz ($= 24$\,Mbps) when ignoring the uneven clock spacing. Aside from the required bandwidth, we would also require significant computing resources, and special high-power voltage regulators to serve all the modules. Additionally, such a solution would also be very unwise space-wise: Since the camera connector is only about $1$\,cm long, it would be impossible to align all cameras in a meaningful way if they were all connected to the same board. Also, to store sixteen $(640,~480)$-pixel images, we would need $9.8$ megabytes of memory in the intermittent controller -- which is beyond anything that can be found on off-the-shelf micro-controllers. For these reasons we decided to go with a separate camera board for each camera.

Since space usage is of essence, we designed a custom PCB using only small-sized SMD components so that a minimal board footprint could be achieved. The board was purposely designed to be taller and less wide so that multiple cameras could be aligned along side each other (see figure \ref{fig:camera_board}).

\section{AVR versus PIC24F Microcontrollers versus FPGAs}
When talking about cameras, an obvious thought is to use an FPGA (field programmable gate array) for processing the data produced by the camera. This is often a sensible choice since cameras produce a lot of data at a very high frequency, and FPGAs are exactly made for this purpose. 

FPGAs have the advantage that all the high frequency processing could happen in low-level logic, while also e.\,g. simulating a normal micro-controller for the parts that are less time-critical. Doing so would mean that the micro-controller logic can run untouched by the hardware load caused by the camera, resulting in an overall effective and balanced system.

Unfortunately, available FPGA chips are too expensive ($\geq 10$\,\textsterling) and only available in high-pin packages ($144+$ pins), making them unsuitable for a board that is produced many times, and that aims to be small. In other words: Using an FPGA is certainly possible, but most likely over-kill for our project. While using a single FPGA chip, and connecting all cameras to it might be possible, we would still face the issue of the spatial alignment, which is restricted by the $1$\,cm connector of the camera module. Another aspect of why an FPGA is unsuitable for our task is that open source USB stacks are available for FPGAs; only very expensive commercial solutions provide this functionality.

Can we achieve the same functionality with a standard micro-controller such as those produced by e.\,g. Microchip or Atmel? 

\subsection*{Dealing with the data}
The initial issue remains: We will be getting high amounts of data at a high frequency. If we had an even clock spacing on the PCLK line, reading in a normal frame would give us:

\begin{equation}
  t_{byte} = \frac{1\,\text{s}}{640\,\text{px} \times 480\,\text{px} \times 2~\text{bytes} \times 30\,\text{Hz}} \approx 54\,\text{ns/byte}
\end{equation}

~\\\noindent Or, to put it in other words, we would have to process the input data at $18.4 $\,MHz. Since normal micro-controllers run at frequencies like this (most even below), this would give us less than a single instruction just for capturing and storing the data -- And this assumes that the data is evenly spaced, which it unfortunately is not: In reality we have about $16$\,ns (or less) to process the data presented to us.

There are two possible solutions to deal with this kind of data load, and we will make use of both of them: For one we can downscale the image presented to us in the camera itself. By halving the image width, and halving the pixel clock speed, we get double the processing time. By down scaling the image to $(40,~30)$ pixels, which will still provide us with $40\,\text{px} \times 30\,\text{px} \times 16 ~\text{cameras} = 19200 ~\text{pixels}$ in total, we gain plenty of time to process our data, and still have a significant surplus of image data for our goal of 500 pixels.

The second solution to the data frequency issue is to find a way of offloading the processing of the incoming data to a peripheral device so that we are not bound by our CPU speed. This has the advantage that we can use the CPU for actually performing some other processing, and for handling communication between devices.

After comparing the different devices out on the market, we settled on the PIC24F micro controller series, which runs at a CPU frequency of $32$\,MHz, and provides hardware peripherals such as the \textit{Parallel Slave Port}. The parallel slave port is basically a serially clocked data input that can be toggled by the outside device (here: the camera), and that can read in 4 bytes worth of data into an internal buffer before any CPU intervention is necessary.

\section{Connecting the OV7670 module}
\label{sec:connecting}
\IMAGE{ov7679_to_pic}{1.0}{Connection scheme of the OV7670 module to a PIC24F microcontroller}
The camera module has been attached to the PIC24F controller as can be seen in figure \ref{fig:ov7679_to_pic}. The I$^2$C bus\footnote{A bus/protocol that is commonly used for communicating between several chips on the same board}, which is an addressable low-speed bus (up to $100$\,KHz under normal specifications) that supports read/write operations to arbitrary addresses, is used for configuring the camera: Using this protocol, we can set up the output image size, color format, clock input options and several other settings. 

The 8 pixel data lines are then used for retrieving the actual camera data. We use the Parallel Master/Slave port of the PIC24F by attaching the PCLK pixel clock output of the camera to the read-strobe of the parallel port. Thus every time the PCLK line is toggled by the camera, the parallel slave port peripheral will automatically transfer the 8 data bits present on the data lines into an internal buffer.

Additionally, in our design the HREF signal is also connected to the cable select line of the parallel slave port, thus disabling the parallel slave port when a row is being switched. This is not strictly required, though, since the camera can be set up to not toggle the PCLK line when no row is present.

As the camera requires two voltages, and an additional IO voltage, we use a dual-output voltage regulator that provides both $1.8$\,V and $2.8$\,V at the same time, and decided to also run the PIC24F controller at the $2.8$\,V level to save one additional regulator.

Furthermore, the camera itself does not have any integrated clock source: For producing a 30$\,$Hz image, the camera needs to be provided with a $24$\,MHz clock signal (see data sheet). We achieve this by using the clock output of the PIC24F controller that outputs the core clock of $\frac{32}{2}\text{MHz} = 16\,\text{MHz}$ on an external pin, and by using the internal PLL and divider of the camera:

\begin{equation}
	clk_{cam} = \frac{32\,\text{MHz}}{2} \times 6 \times \frac{1}{4} = 24\,\text{MHz}
\end{equation}

This controller was also chosen since it has the "Peripheral Pin Select" feature, which allows one to dynamically remap pins to any arbitrary feature at run time. By doing so, we can provide SPI and UART output at the same time without having to build different boards. The SPI or UART output is intended for the central board that performs the merging of the image data.

\section{Board schematics and design}
A board schematic following the description in section \ref{sec:connecting} has been designed using the Eagle CAD software. The files for this are included in the electronic version of this dissertation. 

For the board itself small SMD components and QFN chips were used to minimize the footprint. The board was also designed in a rectangular, tall fashion so that multiple cameras could be stacked together in close space. The complete design can be seen in figure \ref{fig:camera_board_design}; a larger version of this can also be found in the appendix.

\IMAGE{camera_board_design}{1.0}{The PCB that was designed for the camera board: Camera socket on the left, micro-controller in the middle, and voltage regulator on the right. A larger version can be found in the appendix (red: top layer, blue: bottom layer).}

\subsection*{Routing the signals}
Routing the signals on a small board like this is a non-trivial task. Especially since standard cheap PCB manufacturing services only allow for two layers, having crossing signals can be trouble some.

Learning by doing, it turns out that routing the shortest/closest signals that are nearby seems to be the most efficient strategy. This works well since nearby signals can be run in parallel bus-like fashion. By following this principle, and by working from the middle to the outside, nearly all of the signals can be routed easily.

The few remaining signals were a bit tricky since no more space was remaining, but by routing them completely around the outer side of the board (e.\,g. left to bottom left, to bottom right, to top) helped.

Usually, one would also make sure that data signals corresponding to the same clocking signal have the same length to avoid any issues occurring at high frequency readouts. But since our signal is "only" clocked at roughly $24$\,MHz instead of hundreds of MHz, we don't have to worry about this.

\subsection*{Ordering the bare PCBs}
After the design was done, the bare PCBs were ordered at Elecrow\footnote{http://elecrow.com}, a Chinese PCB manufacturing service. To be more price efficient, two boards were put on one board, and were later on separated again using a hand saw.

\subsection*{Soldering the boards}
\IMAGE{camera_board_soldering1}{0.6}{After an initial prototype was completed (left), all the remaining boards were soldered at the same time. Here: First soldering on the PIC24F micro-controller using a hot air gun}
\IMAGE{camera_board_soldering2}{0.7}{All camera modules are soldered up and tested for functionality; unique IDs have been assigned to each board.}
Soldering the boards by hand was fairly easy due to most of the components being 0605 SMD components. A bit of patience was required, but most of the work could be done in two days. There are, however, two difficult parts: The QFN-formed micro-controller, which does not have any pins on the side, but only on the bottom, and the 24 pin camera socket that had a 0.5mm pin spacing. The soldering process can be seen in figures \ref{fig:camera_board_soldering1} and \ref{fig:camera_board_soldering2}.

The camera socket can be soldered by using lots of flux, and first soldering all pins together (i.\,e. dipping the entire socket with solder), and then removing the excess solder using a wick. 

The QFN-sized controller needs to be soldered using a reflow oven, or by using a hot air gun. We went with the latter approach since we did not have access to a reflow oven: By first aligning the chip, and then evenly heating up the entire board to roughly 270\textdegree C using hot air, the chip can be soldered in place. For some of the parts a few iterations were necessary due to unconnected pins or solder bridges -- but after the correct temperature and air settings were found, the error chance drastically decreased.

\section{Micro-controller Program Flow and Design}
\IMAGE{camera_board_flow}{0.9}{Program flow of the controller on the camera board}
\noindent The program flow was designed with two special sections in mind: High frequency, important interrupt routines that need to complete as fast as possible, and the low-level main program execution that is less timing sensitive (see figure \ref{fig:camera_board_flow}).

Code for processing the incoming camera pixels, which needs to complete quickly so that the internal parallel slave port buffer space is free again, is running at the highest priority, and is thus being serviced by a hardware interrupt that can break the main program execution at any point. This interrupt -- in combination with the on-pin-change interrupt used for the VSYNC clock line -- deals with all the incoming camera data. The incoming pixel data is stored in memory in the controller in a frame buffer. As we are only dealing with a $(40,~30)$ pixel image at this point, $40\,\text{px} \cdot 30\,\text{px} \cdot 2\,\text{Bpp} = 2400 ~\text{bytes}$ (roughly $30\%$) of memory are required to store a single instance of the image.

The less critical sections have all been put into the main program in form of a main program loop. The flow of this program can be described in a few words: First, the chip itself is initialized by setting all the required settings (e.\,g. setting up pins, configuring the parallel slave port module, initializing the I$^2$C port\footnote{A bit-banging version of the protocol was implemented since the internal peripheral had known hardware hang issues.}).
After the chip is functional, the external camera is configured by writing data to its I$^2$C registers. This includes configuring the internal camera clock speed setting, setting image width and height, and adjusting several other parameters that influence the camera output such as color format and brightness control

On entering the main loop, the program is now in a slave-receiver-mode that awaits external commands from the central processing board (described later). We developed a simple (read-process-answer)-command protocol that is executed continuously. This protocol runs completely asynchronously from the incoming camera frame, thus allowing us to process frames at different speeds. Features such as a vertical synchronization were not implemented as memory in the controller was too limited for a secondary frame buffer.

\subsection{Hand-optimized interrupt routines}
The most critical part of the application is the parallel slave port interrupt, which is fired whenever the internal buffer of this peripheral has accumulated 4 bytes of data (thus two pixels). Since the incoming data is sent at a high frequency -- even though the image is already down-scaled -- we only have about $0.93\mu s$, or $14.88$ clock cycles to process the entire buffer. Using this many clock cycles, however, would mean we would have no remaining CPU time to run our main application loop, and we would only permanently be executing our interrupt. Thus no more than $70\%$ of the time should be spent in the interrupts.

Special care has to be taken to make sure that the interrupt routine is running as efficiently as possible. The code went through several iterations, the first one being a trivial C-code implementation that only copies the two words into the frame buffer, and advances the frame pixel counter:

\begin{verbatim}
void __attribute__ ((interrupt,no_auto_psv,shadow)) _PMPInterrupt(void) {
    unsigned int pixel1 = PMDIN1;
    unsigned int pixel2 = PMDIN2;
    mPMP_Clear_Intr_Status_Bit;

    frame[framePtr] = pixel1;
    framePtr += 1;
    frame[framePtr] = pixel2;
    framePtr += 1;
}
\end{verbatim}

\noindent This code, even though the only thing it does is reading two words and storing them in an array, uses \textbf{17 CPU instructions} in total. A simpler assembly solution was devised:

\begin{verbatim}
__PMPInterrupt: 
    push.s ; save w0-w3 registers to shadows (only one copy exists)
    mov _framePixelPtr, w0 ; load pointer - must be absolute
    mov PMDIN1, w1
    mov PMDIN2, w2
    mov w1,[w0++]
    mov w2,[w0++]
    mov w0, _framePixelPtr ; save new pointer
    pop.s ; restore registers
    bclr IFS2, #PMPIF ; clear interrupt
    retfie
\end{verbatim}

\noindent By storing the absolute frame pointer address instead of storing the base array address and the offset, the integrated store-and-increment functions of the PIC24F can be used. Thus lots of instructions are saved. Additionally, the \textit{push.s} and \textit{pop.s} instructions save the working registers in a single instruction without us having to manually use the stack. This new version of the code only requires \textbf{10 CPU cycles}.

Can we do even better? Yes. The CPU of the PIC24F has 16 registers in total. By reserving two of those 16 working registers\footnote{Additional initialization and reservation code is required to make this work}, and permanently storing the current frame buffer pointer/location in one of those registers, we do not have to load and save the data on every interrupt, but instead can immediately write and read to this address. Since 14 remaining registers is still plenty of space to work with, we can optimize our ISR (a method that is immediately called when a certain hardware event occurs) to work with even less cycles:

\begin{verbatim}
__PMPInterrupt: 
    mov PMDIN1, w12 ; pixel buffer
    mov w12,[w13++] ; pixel buffer -> frame pixel ptr
    mov PMDIN2, w12
    mov w12,[w13++]
    bclr IFS2, #PMPIF ; clear interrupt
    retfie
\end{verbatim}

\noindent By reserving the registers \textit{w12} and \textit{w13}, our interrupt now completes in \textbf{6 CPU cycles} instead of 10. Moving the data directly from the \textit{PMDIN} registers (which represent the internal input buffers of the Parallel Slave Port) directly to a memory location is not possible though, since no register-to-memory \textit{mov} operation exists. Otherwise we would not require the \textit{w12} register.

So, this is it then, right? No. Not yet. We can still do a slightly bit better -- although the code gets a bit more unreadable from this point on. But since this is a highly called interrupt routine, and since every CPU cycle counts for our use-case, we can live with added extra complexity:

\begin{verbatim}
__PMPInterrupt: 
    mov [w13++],[w12++] ; move PMDIN1 and re-point w13 to PMDIN2
    bclr IFS2, #PMPIF ; clear interrupt
    mov [w13--],[w12++] ; move PMDIN2 and re-point w13 to PMDIN1
    retfie
\end{verbatim}

\noindent This code also uses two reserved registers (\textit{w12} and \textit{w13}), where \textit{w13} is initialized to the address of \textit{PMDIN1}, and \textit{w12} is the current frame buffer pointer. What this code does is that it takes advantage of the PIC24Fs ability to arbitrarily move data between memory locations while simultaneously incrementing and decrementing address registers. By using clever increments, we can make \textit{w13} alternate between \textit{PMDIN1} and \textit{PMDIN2}. \textit{w12} is simply incremented to advance the frame buffer location. Using this clever piece of code, the ISR completes in just \textbf{4 CPU cycles} total:  This means we only need $23\%$ of the time the original C implementation of the same functionality took.

By performing this optimization, we are able to capture the camera video input at a frequency of up to $30$\,Hz at $(20,~ 30)$-pixels resolution, which is only capped by the camera output frequency itself. At a higher resolution of $(40,~30)$-pixels we can sample the input at a frame-rate of $20$\,Hz.

\subsubsection*{A quick lession learned -- DMA}
Having an ISR that performs quickly is of essential nature -- if even a single interrupt from the parallel slave port is missed, the entire frame will be shifted by an indefinite amount of pixels, and thus is worthless for us. Even if this is not the case, additional care has to be taken that the normal program operation is not significantly delayed -- if the ISR is taking up $99.9\%$ of the CPU time, there is not enough time to do any other work: A careful balance has to be chosen, especially since the interrupts may occur at non-deterministic intervals.

In retrospect it would have been easier if a controller with a DMA (direct memory access) peripheral was chosen. The PIC32  32-bit series e.\,g. commonly provides this feature. A DMA controller allows for memory copy operations without any CPU intervention. In our case, we could have simply started a DMA transfer from the parallel slave port data buffers that is triggered on each parallel slave port interrupt, and stored the data in our memory. Doing so would have allowed us to capture images at much higher resolutions (presumably even up to $640$ pixels in width) -- although the storage memory required for the frame-buffer still remains an issue.

Unfortunately though it was impossible to find a PIC24F device that had both the parallel slave port, DMA and would fit on the same board. For further versions of this board a different controller should be considered though.

\subsection{Communication protocol}
\label{sec:uart}
A simple UART\footnote{Protocol used e.\,g. by a standard serial port, consisting out of a "Transmit" and a "Receive" line, and data bytes framed with start and stop bits which are sent at a fixed baud rate}-based protocol was devised. The advantage of choosing UART over alternatives such as SPI (serial peripheral interface) is the existence of integrated framing mechanisms such as start and stop bits, making it easier to correctly reinterpret the data. Also, since a text-based protocol was chosen, it was much easier to visualize, test and interface the camera boards. Simple off-the-shelf USB-to-Serial converters could be used for debugging purposes. The command structure that was designed for this protocol can be seen in table \ref{table:uartcmd}.

\begin{table}[h]
\begin{tabular}{c|c|l}
 Byte\# & Value & Description  \\ \hline \hline
 0 & 'R' & Start Request \\
 1 & ID or '@' & Target of the request ('@' is a wild-card for all cameras) \\
 2 & Command & e.\,g. 'F' to request a full frame \\
 3 & \dots & Additional command arguments
\end{tabular}
\caption{UART, Character-based Command Structure}
\label{table:uartcmd}
\end{table}

Since multiple cameras will be shared on one bus, an unique camera ID was assigned to each camera. A sequence of simple letters ('A', 'B', ..) was chosen for this. After sending a command, the camera sends out the appropriate response over the UART \textit{TX} pin. Answers are suppressed if a generic target ('@') was specified to prevent bus collisions. 

A selected set of the operations implemented includes:

\begin{description}
  \item['R' \$id 'F':] Sends out a full $(40,30)$ pixel frame (byte by byte, 2400 bytes in total)
  \item['R' \$id 'H':] Sends out a half $(20,30)$ pixel frame
  \item['R' \$id 'S' 'I' \$register \$mask \$value:] Changes the I$^2$C camera register value
  \item['R' \$id 'S' 'R':] Resets the camera board and module
\end{description}

\noindent Many more operations have been implemented and can be looked up in the source code of the camera board.

\subsection{Readout of selected pixels}
Originally we were worried that our central camera bus would not be able to handle the whole bandwidth presented by the cameras, and that we would not be able to transmit a full, compound image to the connected computer. To help with those two concerns, we also designed and implemented a different way of reading out the frames, which was inspired by the sparseness of the ant compound eye: Instead of streaming a full frame, and having a central board combine the data, we are also able to selectively only stream a subset of pixels from each frame (see figure \ref{fig:pixelselect1}).

\IMAGE{pixelselect1}{0.75}{Single pixels of the whole frame are selected using  indices (red), thus creating a down-scaled image}

\noindent To implement this, a special array of $400$ elements is allocated on each board. This array holds the $16$-bit indices of the pixels to select from each frame. When a custom "pixel-select" frame is sent to the board, it iterates over this array instead of the full frame, and only sends the elements at the given indices. 

Since the $PIC24F$ we used does not contain any EEPROM memory, a different way of persistently storing the pixel-select indices during power-off phases had to be found: We now store the data at a temporary location in memory, but also provide a special "save" command that can be used to persist the array in the controllers flash memory by having an assembly routine reprogram part of the controllers program memory on the fly. Since program memory can only be written in pages of 512 bytes, special care of byte alignment had to be taken to ensure that that no code section was accidentally erased or overwritten. The persisted array indices are then loaded into memory again on each power on of the controller.

\IMAGE{pixelselect2}{0.75}{The "pixel-select" algorithm can also be used to realize approximated, pre-calculated versions of complex image transformations such as e.\,g. rotations.}

In addition to reducing the total data load and providing a simple way of creating a down-scaled, sparse image, there are more advantages of using this approach: By providing the means of selecting arbitrary pixels from each camera, we can pre-calculate image transform operations such as skew, rotation or mirroring and their resulting coordinates on a whole-pixel basis, and thus perform these operations on the camera controllers without having to perform any complex mathematical operations (see figure \ref{fig:pixelselect2}). Since the controller only has to perform memory fetches, the generated CPU load is minimal.

In the end, however, this feature -- while implemented in all boards -- was never used since we were able to handle the full data load of a frame. Complex image transformations were performed on the connected computer instead as this provided a quicker means of testing during development.

\section{Issues arising during development}
During the development of this board several issues arose which  will be briefly mentioned here for those who also want to interface the OV7670 camera module -- in the hope that it might save a few hours of work. Please note that the example images presented here were made using different versions of the .NET API, which will be presented later in chapter \ref{sec:api}.

\begin{figure}[htbp]
\centering
\begin{minipage}[b]{0.45\linewidth}
\IMAGEx{camera_board_testimage_wrong_edge}{0.62}{Camera image sampled on the wrong edge results in unstable colors}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
\IMAGEx{camera_board_testimage}{1.0}{The camera image correctly visualized (here: an internal test pattern)}
\end{minipage}
\end{figure}

\begin{figure}[htbp]
\centering
\begin{minipage}[b]{0.45\linewidth}
\IMAGEx{camera_board_testimage_wrong_pclk}{0.95}{Test image if a wrong internal Pixel Delay is selected}
\end{minipage}
\quad
\begin{minipage}[b]{0.45\linewidth}
\IMAGEx{camera_board_testimage_wrong_framing}{1.0}{Real camera image, but with lost bits (middle) and wrong framing}
\end{minipage}
\end{figure}

\begin{description}
  \item[Camera not responding:] This is most likely due to an inverted \textit{RESET} signal. The data sheet states that this signal has to be \textit{LOW} for the module to work, but in reality the signal has to be \textit{HIGH}.
  
  \item[Camera not responding via I$^2$C:] The module won't respond if the \textit{XCLK} pin is not actively driven. One can check this by looking at the PCLK output pin, which -- by default -- as the same output as the XCLK input.
  
  \item[Camera image noisy/every second pixel wrong:] Data most likely sampled on the wrong edge (data has to be sampled on the rising edge).
  
  \item[Camera test image not rectangular, but shifted:] Wrong image size in the target frame buffer. By default the camera only outputs $638$ instead of $640$ pixels (can be changed via $I^2C$).
\end{description}

\section{Color Calibration}
Since the camera module uses \textit{YCrCb} as an output format by default, the standard color settings of the module are not suitable for \textit{RGB} color output: The green and red channel are swapped, and colors are generally mismatched. To fix this, the module has to be configured with a custom color conversion matrix. The documentation on the required output was not very detailed, so some experimentation was required to produce a good output picture: 

A $3 \times 3$-matrix is used to convert the input raw $(R, G, B)$-tupels to the \textit{YCrCb} format (which is then internally transformed back to \textit{RGB} when \textit{RGB} is selected as an output format). Since the internal \textit{DSP}\footnote{Digital Signal Processor} of the OV7670 only works with integer values, the floating point matrix has to be converted to a 8-bit non-fractional integer matrix. The used color matrix itself is a combination of several different conversion matrices:

\begin{equation}
  M_{color} = M_{hue} \times M_{YCrCb} \times M_{correction}
\end{equation}

~\\ \noindent The resulting matrix can be used to calculate the output image by simply multiplication with the input column-order pixel array.

\begin{equation}
  \left( \begin{array}{c} Y_i \\ Cr_i \\ Cb_i \end{array} \right) = 
  M_{color} \times
    \left( \begin{array}{c} R_i \\ G_i \\ B_i \end{array} \right)
\end{equation}

~\\ \noindent The first matrix in this step is the correction matrix $M_{correction}$, which is used to reduce the cross-talk across the different colors (R shining through into the B sensor, \dots). The values for this matrix have been taken from the camera documentation itself:

\begin{equation}
  M_{correction} = \left( \begin{array}{ccc}
1.36 & -0.30  & -0.06 \\
-0.20 & 1.32 & -0.12 \\
-0.04 & -0.55 & 1.59 \end{array} \right) 
\end{equation}

~\\ \noindent After the true colors have been extracted, the output needs to be converted into the \textit{YCrCb} format, which is the expected output format of the camera matrix. It also makes other post-processing steps such as color rotation easier. The conversion matrix follows the standard formula for converting \textit{RGB} to \textit{YCrCb} (see \cite{YCrCb} and equation \ref{eq:ycrcb}). The \textit{YCrCb} output of this matrix is made up of three channels: The first one represents the Luma/Grayscale channel, remaining two the red and blue color component.

\begin{equation}
  M_{YCrCb} = \left( \begin{array}{ccc}
0.299 & 0.587  & 0.114 \\
0.5 & -0.418688 & -0.081312 \\
-0.168736 & -0.331264 & 0.5 \end{array} \right) 
\label{eq:ycrcb}
\end{equation}

~\\ \noindent For some unknown reason the colors extracted from the cameras are shifted by $120$\textdegree\, in the analog portion of the camera (the digital test image is perfectly accurate). Since no documentation was available on why this might happen, any argumentation on this would be pure speculation. However, to get the 
correct color output from the analog core, the entire color spectrum has to be rotated (the luma/Y component was left untouched):

\begin{equation}
  \alpha = 120\text{\textdegree}
\end{equation}

\begin{equation}
  M_{hue} = \left( \begin{array}{ccc}
1 & 0 & 0 \\
0 & \text{cos}(\alpha) & -\text{sin}(\alpha) \\
0 & \text{sin}(\alpha) & \text{cos}(\alpha) \end{array} \right) 
\end{equation}

~\\ \noindent Remaining values such as the saturation were left unchanged, but can be changed by simply multiplying the \textit{Cr} and \textit{Cb} channel with a gain value. By using the so created color matrix, and transforming it into an 8-bit integer matrix, we get the RGB output we expect. The matrix can be set directly on the camera module using $I^2C$ and 7 registers (the luma channel is dropped, since this calculation is hard-wired into the chip, and a sign register is added so that positive and negative values can be computed):

\begin{verbatim}
public void SetColorMatrix(Matrix3x3 mtx) {
   mtx *= 128; // Convert to integer matrix
   ...
   for (int i = 0; i < 6; i++) {
     // Set matrix value
     SendI2CConfig(0x4F + i, 0xFF, (byte) Math.Abs(Math.Round(mtx[3 + i])));
     if (mtx[3 + i] < 0)
        minusBits |= (1 << i);
   }
   // Set matrix sign bits
   SendI2CConfig(0x58, 0x3F, minusBits);
}
\end{verbatim}
