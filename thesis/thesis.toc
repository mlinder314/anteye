\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Motivation}{1}
\contentsline {section}{\numberline {1.2}Previous Works}{3}
\contentsline {section}{\numberline {1.3}Structure of the Ant Eye}{4}
\contentsline {section}{\numberline {1.4}Objectives}{5}
\contentsline {section}{\numberline {1.5}Basic Conceptual Design}{6}
\contentsline {section}{\numberline {1.6}Structure of this thesis}{7}
\contentsline {chapter}{\numberline {2}Optical Elements}{11}
\contentsline {section}{\numberline {2.1}Optical Receptor Arrays}{11}
\contentsline {section}{\numberline {2.2}Individual Optical Receptors}{12}
\contentsline {section}{\numberline {2.3}OV7670 Camera Module}{13}
\contentsline {chapter}{\numberline {3}Camera Board}{19}
\contentsline {section}{\numberline {3.1}AVR versus PIC24F Microcontrollers versus FPGAs}{20}
\contentsline {section}{\numberline {3.2}Connecting the OV7670 module}{21}
\contentsline {section}{\numberline {3.3}Board schematics and design}{22}
\contentsline {section}{\numberline {3.4}Micro-controller Program Flow and Design}{25}
\contentsline {subsection}{\numberline {3.4.1}Hand-optimized interrupt routines}{26}
\contentsline {subsection}{\numberline {3.4.2}Communication protocol}{28}
\contentsline {subsection}{\numberline {3.4.3}Readout of selected pixels}{29}
\contentsline {section}{\numberline {3.5}Issues arising during development}{31}
\contentsline {section}{\numberline {3.6}Color Calibration}{32}
\contentsline {chapter}{\numberline {4}Merge Board}{35}
\contentsline {section}{\numberline {4.1}Hardware Choice}{36}
\contentsline {subsection}{\numberline {4.1.1}Shared UART Bus}{36}
\contentsline {section}{\numberline {4.2}Board schematics and considerations}{37}
\contentsline {subsection}{\numberline {4.2.1}Breakout Board}{38}
\contentsline {subsection}{\numberline {4.2.2}Voltage Regulators}{38}
\contentsline {section}{\numberline {4.3}Merging 16 cameras into one image}{39}
\contentsline {subsection}{\numberline {4.3.1}Sequential Camera Readout}{39}
\contentsline {subsection}{\numberline {4.3.2}Bus Idle Detection}{39}
\contentsline {subsection}{\numberline {4.3.3}Probing the Camera Bus}{40}
\contentsline {subsection}{\numberline {4.3.4}Maximum Bus Speed}{40}
\contentsline {section}{\numberline {4.4}USB computer connection}{41}
\contentsline {section}{\numberline {4.5}Program Flow}{42}
\contentsline {chapter}{\numberline {5}AntEye .NET API}{45}
\contentsline {section}{\numberline {5.1}Conceptual Design}{45}
\contentsline {section}{\numberline {5.2}API Reference}{46}
\contentsline {section}{\numberline {5.3}Frame Reader Implementation}{46}
\contentsline {section}{\numberline {5.4}Implemented Programs}{47}
\contentsline {chapter}{\numberline {6}Camera Sphere}{49}
\contentsline {section}{\numberline {6.1}Modelling the Ant Eye}{49}
\contentsline {subsection}{\numberline {6.1.1}Two eye segments}{49}
\contentsline {subsection}{\numberline {6.1.2}Spherical Prototype}{50}
\contentsline {section}{\numberline {6.2}3D Visualization}{52}
\contentsline {section}{\numberline {6.3}Calibration of the Camera Sphere}{53}
\contentsline {subsection}{\numberline {6.3.1}Manual Calibration}{53}
\contentsline {subsection}{\numberline {6.3.2}Challenges}{53}
\contentsline {subsection}{\numberline {6.3.3}Automatic Calibration}{54}
\contentsline {subsubsection}{\numberline {6.3.3.1}Pattern detection}{55}
\contentsline {subsubsection}{\numberline {6.3.3.2}Pair-wise pose estimation}{55}
\contentsline {subsubsection}{\numberline {6.3.3.3}Graph-based recursive relocation}{57}
\contentsline {chapter}{\numberline {7}Evaluation}{59}
\contentsline {section}{\numberline {7.1}Achieved Objectives}{59}
\contentsline {section}{\numberline {7.2}Frame-rate Statistics}{60}
\contentsline {section}{\numberline {7.3}Calibration Results}{61}
\contentsline {section}{\numberline {7.4}Cost of a single camera board}{63}
\contentsline {section}{\numberline {7.5}Testing a navigation algorithm: Visual Compass}{63}
\contentsline {chapter}{\numberline {8}Summary and Conclusion}{67}
\contentsline {section}{\numberline {8.1}A thought on Image Quality}{68}
\contentsline {section}{\numberline {8.2}Reflection about hardware choices}{69}
\contentsline {section}{\numberline {8.3}Possible Improvements}{69}
\contentsline {chapter}{\numberline {A}Camera Board Layout}{71}
\contentsline {chapter}{\numberline {B}Merge Board Layout}{73}
\contentsline {chapter}{\numberline {C}Source Code and Software}{75}
\contentsline {chapter}{Bibliography}{77}
