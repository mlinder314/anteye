\chapter{AntEye .NET API}
\label{sec:api}
A small API was designed to receive the data on the connected computer via USB. The API was written in C\# using the .NET Framework to make use of existing interfaces for the Serial Port, and for its Imaging capabilities, but the software can also be easily ported to other frameworks and languages. The API has a three main tasks:

\begin{itemize}
  \item Reading data from the serial port, and turning it into images
  \item Adding some simple filters that help with splitting and reordering the data
  \item Providing extra utilities for visualization
\end{itemize}

\section{Conceptual Design}
\IMAGE{api_flow}{1.0}{Event-based API Data Flow (left to right)}
\noindent The data flow behind the API is rather simple (figure \ref{fig:api_flow}): We read our data from the serial port into a temporary buffer, transform that buffer into an RGB888 image, and notify all attached event handlers that a new frame has arrived.

The connected event handlers can e.\,g. be special filters that split up the image, or align it in a different way. Filters take an image as an input, and give one (or more) images as an output, and can be arbitrarily cascaded.

The last bit of the data flow is the visualization portion: Some simple means of showing an image window containing the frame have been implemented, and a more complex 3D projected panorama view was designed, which will be described in more detail in chapter \ref{sec:panorama}.

\section{API Reference}
\IMAGE{api_class_diagram}{1.1}{API Class Diagram; grouped by area.}
\noindent The classes implemented in the API can be seen in figure \ref{fig:api_class_diagram}. A generic interface \textit{ICameraBoard} has been declared which represents an frame fetch device for multiple cameras, and will produce output frames and publish them to an event handler. The actual implementation for this project can be found in \textit{CameraMasterBoard}, which uses an underlying SerialPort for receiving the data. For this thesis three simple filters have been implemented:

\begin{description}
  \item[FrameLabel:] Takes the multi-camera input frame, and adds a textual camera identifier to each of the sub frames, thus making it possible to identify from which camera a frame originates.
  \item[FrameGridAlign:] Takes a multi-camera input frame in its standard vertical layout, and realigns it in a grid. This is useful for visualizing the image in a space-saving fashion. The aspect-ratio of the generated grid can be adjusted to create e.\,g. wide-screen, quadratic or rectangular grids.
  \item[FrameSplitter:] Takes the multi-camera input frame, and turns it into $N$ single sub-images, each for one of the cameras in the board. Useful when more complex aligning techniques that operate on single cameras are required.
\end{description}

\section{Frame Reader Implementation}
The internal implementation of the API was written with efficiency in mind: Two reusable frame-buffers are allocated and swapped after a frame have been read. Internal bit-blit operations are used to transfer data between buffers. The camera output has been adjusted so that the RGB565 to RGB888 conversion can be done natively in the .NET framework.

The simplified algorithm of the frame reader that is being used by this flow has been described below:

\begin{enumerate}
	\item Find the Camera Board by looking for connected USB Devices with the proper ID

	\item Search for a Serial Port that has the same Device-ID as our connected USB Device

	\item Open the Serial Port, and send I2C Configuration Data to the Camera Board
	\begin{enumerate}
	  \item Setup RGB565 output format with swapped MSB/LSB bytes
	  \item Disable automatic brightness control (since each camera has an individual brightness setting, the compound image would get unevenly lit with the automatic gain/brightness control active)
	  \item Set manual brightness and gain levels
	  \item Setup color matrix (by default the R and G channels are swapped in this module)
	\end{enumerate}
	
	\item Find first start-of-frame marker using a byte-by-byte scan

	\item Repeat:
	\begin{enumerate}
		\item Read $2400 \times 16$ bytes into an internal buffer
		\item Check if frame valid (EOF marker present?)
		\item Convert to native .NET RGB888 image
		\item Notify attached frame handlers of arrived frame, and swap buffers
	\end{enumerate}		
\end{enumerate}

\noindent The methods for finding the multi-character markers have been realized by a simple per-byte scan of the data input, and an integrated state machine. This implementation is rather ineffective, but only has to be executed once until the first correct frame is found.

\section{Implemented Programs}
In the process of creating this thesis, a set of test and helper programs were implemented along the way that are not separately mentioned in this thesis. For the sake of documentation they are shortly listed hereafter by their project/folder name with a short description:

\begin{figure}[hbtp]
\centering
\begin{minipage}[b]{0.46\linewidth}
\IMAGEx{i2cconsole}{1.0}{I$^2$C Console}
\end{minipage}
\quad
\begin{minipage}[b]{0.432\linewidth}
\IMAGEx{framerecorder}{1.0}{Frame Recorder}
\end{minipage}
\end{figure}

\begin{description}
  \item[InitialCamTest:] The original camera test application that was used to first read the camera input. Not very cleanly written, but can deal with different kinds of data input and automatically detects image sizes.
  
  \item[AntEye:] The cleaner version of the API -- as presented in this chapter. It allows for reading, filtering, and visualizing frames.
  
  \item[AntEyeTest:] Simple test application making use of the AntEye-library. Displays several image windows with different filters applied.
  
  \item[I2CConsole:] Utility which allows for changing the camera settings for all attached cameras on the fly without re-flashing any of the controllers. Useful for playing around with imaging parameters such as color matrices, brightness, \dots  ~(see figure \ref{fig:i2cconsole}).
  
  \item[FrameRecorder:] Recording utility that allows for saving the captured, unprocessed camera output in a sequence of \textit{.png} images. Also allows for readjusting the camera brightness/gain settings on the fly. Used in the evaluation for having reproducible results (figure \ref{fig:framerecorder}).

  \item[EyeCalibration, PanoramaView:] 3D Visualization of the captured image (see chapter \ref{sec:panorama}).
  
  \item[SerialPortSpeedTest:] Used to measure the maximum achievable UART/USB bandwidth of a connected USB device (see evaluation).
  
  \item[VisualCompass:] Implementation of a simple navigation algorithm that uses pixel-by-pixel deltas (see evaluation).
\end{description}