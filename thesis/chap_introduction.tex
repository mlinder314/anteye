\chapter{Introduction}
In the current state of the art, there are many different kinds of sensory input available for use in the area of robotics: E.\,g. laser rangefinders, ultra sonic systems, or infrared-based solutions. However, the most commonly used sensory input is vision. We focus on a special subset of this area -- namely compound vision inspired by the insect eye, which nearly has a  360\textdegree~ field of view.

We investigate whether we can find a advantageous trade-off between using a single-camera eye module that has been fitted with a $360$\textdegree~ lens, and modules that directly imitate a compound insect eye by combining many hundreds of single optical elements. To do this, we will create our own compound eye built from sixteen single CMOS camera modules. This compound eye, which has been inspired by the ant eye, makes use of off-the-shelf hardware and micro-controllers to create one compound image resembling a view similar to that of insects. The resulting image will be transferred to a computer for further post-processing. We will test navigation algorithms  on this prototype to evaluate its practical performance.

Since our research group is especially working with desert ants (Cataglyphis velox), we will focus on the mechanics and characteristics of the ant eye. However, much of the work presented here can also be applied to the general characteristics of the insect eye.

In the following sections we will first describe our exact motivation for trying to find such a middle ground, and we will then look at existing solutions to our problem. We will also shortly outline the structure of the ant eye, and then describe the objectives we have set ourselves in more detail. After the initial ground has been set, the remaining parts of this thesis will deal with the construction and evaluation of our prototype model.

\section{Motivation}
Why do we want to create another detailed electronic model inspired by the insect eye? Why do we not just use one single camera with a 360\textdegree~ degree lens (see figure \ref{fig:view_of_360_degree_lens}) that provides a similar -- or maybe even better vision -- while also being very cost-effective? Or why not use an exact replica of a compound eye as found in an insect? Why not use hundreds of optical elements, and create one combined image from them?

\IMAGE{view_of_360_degree_lens}{0.5}{View of a 360 degree lens mounted on a normal camera: Except for a blind spot along the axis the camera is mounted on, we achieve a continuous image.}

While the previous two approaches have been widely used, and prototypes exist for both (see section \ref{sec:previous}), they do not meet our demands: Using many hundreds of optical elements is not feasible when building a prototype by ourselves, since this requires extensive hand labor and expensive equipment. The work required to build, align and test such a prototype would be too immense. Also, since this would require parts on a miniature scale, a model of this kind cannot be built unless specialized equipment is available. This problem gets even more difficult if single lens apparatuses have to be fitted onto every optical element.

So why not just use a $360$\textdegree\, lens and a single camera? While this has been done in our labs for simple test robots, our research group is primarily interested in figuring out the inner workings of the desert ant brain, and how sensory input is used by the ants for navigation within the ant habitat. Of course this includes low-resolution omni-directional vision that can be imitated with a $360$\textdegree lens. But there are also features that are more specialized than can be seen by the human eye. Instead of simply seeing color vision, ants have certain eye elements that e.\,g. can detect UV light, or that detect polarized light waves in specific orientations (cmp. \cite{VisualAcuity}). The ant makes use of those to detect its orientation in accordance to the sun, and thus can create a compass reference directly from its visual input. While a single camera with a $360$\textdegree\, lens would give us a very large field of view, it would make it difficult to put specialized filters on only partial areas of this field of view. 

For this reason we want to create a trade-off model between the single-camera and the multiple-element approach by making use of many cameras, which can potentially be fitted with different lenses/filters. We hope that such a prototype is simpler to build than a fully fledged compound eye, but that it will still allow us to take the advantages of having multiple optical elements instead of just one camera. Due to the wide availability of cheap miniature cameras creating a model made up out of separate camera modules instead of single optical elements also seems to be easier and cheaper to produce.

We hope to test the feasibility of such a prototype, while simultaneously trying to figure out which implementation issues will arise when combining multiple optical elements into one image. Our final goal is to see how well this prototype works in a practical environment. At some point in the future we also plan on putting this model on an actual lab robot, and to test some of the ant navigation algorithms on it.

By sticking closely to the inner workings of the ant's visual system, we also have the hope that some these problems we face might have also occurred similarly in nature, and thus that our solutions to these problems might shed some closer light on the biological ant-eye and visual circuit.

\section{Previous Works}
\label{sec:previous}
As we are building a compound eye which is biologically inspired, we are taking into account the research that has been performed on the ant- and insect-eye. The general structure of ant-eye, and its vision system have e.\,g. been researched by \cite{AustralianDesertAnt}, \cite{VisualAcuity} and \cite{flickerfrequency}. We will use those papers as a basis for modeling our prototype.

But even the idea of an electronic 360\textdegree\, eye, or an compound eye made out of separate optical segments is nothing completely new, and has been implemented before. A few distinct models (as already researched by \cite{irp}), are:

\begin{itemize}
\item The "Ladybug" camera (\cite{ladybug}), which is a compound camera model that provides a completely overlapping 360\textdegree\, field of view, and that can e.\,g. be mounted on top of a car. It is a larger prototype made up out of 6 separate HD cameras that are aligned on a cylindrical base.

\item Another model was developed by Curvace (cmp. \cite{curvace}), which tries to imitate the fly eye using single line-arrays of optical elements that are aligned in a half circle fashion. Their approach makes use of actual single pixel ommatidia elements instead of cameras, and also requires special electronic fabrication equipment that is capable of splicing optical arrays into stripes, and aligning them on a cylindrical base.

\item Another kind of professionally made model is the elastic micro-lens model by \cite{Compoundeye2}, which employs a special technique that can be used to put micro-lens arrays (such as those used by CMOS cameras) on a spherical/flexible surface. Their paper describes the manufacturing process required to achieve such a flexible imaging array, and the required wire bonding techniques required for steady connections in such flexible environments. Using this technique they can put up to $250$ optical elements on a half-spherical base.
\end{itemize}

\noindent In general, the existing projects we found in this field of research fall into one of the two mentioned extremes: Either they only use a single camera apparatus and some specialized lens, or they are made up out of hundreds of single optical elements. The latter requires expensive manufacturing; the former do not suit our needs for using customized light wave filters. The project that comes closest to ours is the "Ladybug" camera, which -- as already mentioned -- is also a compound camera model made up out of six separate cameras.

So why is our project something new and different? Instead of focusing on large, expensive solutions that requires high-tech cameras or expensive equipment, we will instead focus on a solution that only uses off-the-shelf components, and which thus can be produced at a cheap cost without much specialized equipment. We aim to answer the question of "what can we do with as little as possible" while simultaneously providing all the features required for ant vision. Additionally we also want to investigate at which point using more cameras (or less cameras) becomes less advantageous, and what the optimal number of cameras for such a prototype model is.

\section{Structure of the Ant Eye}
\IMAGE{structure_of_the_ant_eye}{0.66}{The compound structure of the ant eye consists out of many tiny ommatidia that -- when combined -- create a coherent image. Each ommatidium has a separate lens apparatus, and captures a distinct region within the field of view (diagram taken from \cite{compoundeye.wiki}).}

\noindent In contrast to the human eye, which works by projecting focused rays of light on to the back of the retina to create one continuous image (cmp. \cite{humaneye.wiki}), the ant eye is made up of many single eye elements: The so called "ommatidium" (pl: ommatidia, cmp. \cite{AustralianDesertAnt}). These ommatidia are aligned in a hexagonal fashion (see figure \ref{fig:structure_of_the_ant_eye} and \ref{fig:gaps_between_ommatidia}) on a spherical plane to provide a compound view of slightly less than 360 degree views. (\cite{compoundeye.wiki})

While each of these ommatidia has about six to nine photo receptors that are focused on the area in which the receptor is facing (cmp. \cite{OpticalScaling}, \cite{VisualAcuity}), the output of one ommatidia is just a single color signal. An ant eye thus provides a complete view of around 400 to 500 pixels, which is a much lower resolution than human eye.

Each of these ommatidia is also built and shielded from other ommatidia in such a way that the light entering the receptor only corresponds to this single ommatidium; the total view of the ant is thus a coarse image with gaps in between (see figure \ref{fig:gaps_between_ommatidia}).

\IMAGE{gaps_between_ommatidia}{0.8}{The image generated by the ant eye is not continuous, but instead has many gaps between different ommatidia that are caused by the internal design and arrangement of each ommatidium.}

~\\\noindent There are, however, more characteristics that are specific to insect eyes (cmp. \cite{irp}). A small list of these features folllows:

\begin{itemize}
  \item Ommatidia are arranged around 3.7\textdegree\, apart (desert ant, \cite{AustralianDesertAnt})
  \item Ants have certain ommatidia that can detect UV and polarized light, and they use them as a means of deriving a compass function from the detected sun light.
  \item While the eye itself is continuously generating color signals, the internal response delay of the ommatidia (cmp. "time constant" in \cite{flickerfrequency}) results in an equivalent of digital frequencies in the range of 50 Hz and higher. This is especially known to be true for flying insects that move at much faster speeds (see \cite{flickerfrequency}).
  \item The two eyes of the ant provide a panorama view that has an overlapping view area in the front, but a small blind spot in the back (see \ref{fig:overlapping_eyes}).
\end{itemize}

\IMAGE{overlapping_eyes}{0.5}{The two eyes have an overlapping area in the front, but also a blindspot in the back area. (\cite{OpticalScaling})}

\section{Objectives}
A research proposal was written earlier in the year before the actual project was started (see \cite{irp}). In this proposal, we also already outlined the objectives of this project. We will shortly recapture, and refine the most important aspects of this proposal:

\begin{description}
  \item[Building a hardware chain representing a compound eye.] It is our primary goal to build a set of hardware boards that can be linked together to capture visual data from each optical elements and to transfer the resulting, combined image to a connected computer.
  
  \item[Capturing Sparse Images.] It is essential that we are able to display some sort of captured (sparse) image on the endpoint (computer). We hope to be able to provide this image at a high frame rate of up to $30$\,Hz, and for an image resolution close to that of the ant eye (thus around 400 to 500 pixels).
  The image should -- if possible -- be of sparse nature to imitate the real behavior of the ant-eye. As an output format we can imagine a 3D visualization, or an array-based output where each pixel could be represented by its vector direction from the camera center, and its color data.
  
  \item[Combine the optical elements.] Single optical elements do not yet make up a whole eye: Another aspect to this is to combine all single image elements in some sort of single frame that can be used for further processing.
  
  \item[Align elements and calibrate data.] Since our model base will most likely be a handmade prototype, it will inevitably contain inaccuracies. If this is the case, we will aim to provide a calibration mechanism that can be used to get a correctly aligned image on the endpoint. However, since for some navigation algorithms it does not matter whether the single pixels within the image are correctly aligned within world-space or not, we will not focus on providing an exact representation, but instead are content with providing an approximation of the ant eye view.
  
  \item[Test the eye in simulations.] For our evaluation we hope to run simple navigation algorithms on the eye in order to test how well our prototype will work in comparison to a single-camera approach, which is usually employed as an easier alternative.
\end{description}

~\\\noindent We aim to provide all those objectives mentioned above while only using off-the-shelf components (that is: parts that can be easily acquired) and cheap parts to keep the cost of the whole product reasonably low. We also do this to provide  a contrast to the expensive models already available on the market. The countless hours of soldering components by hand, and all the remaining handwork for a prototype have been excluded from this cost calculation -- since we also learned a lot during this project.

\section{Basic Conceptual Design}
For our hardware design we want to closely follow the basics of the signal pattern of the ant brain (see figure \ref{fig:ant_brain}): The single imaging elements capture the selected portion of their environment, and forward their pixel data to a central component that does some kind of merging and aligning.

Our design, which can be seen in figure \ref{fig:conceptual_layout}, will do the same, but will instead use micro-controllers and a computer instead of using brain tissue.
The image cells will capture a set of pixels, which are then forwarded to a central "merging" board made up out of one high-speed micro-controller. The board merges all image cells into one coherent image, which is then presented to the computer endpoint (our "brain") for further processing. Since soldering 500 elements or boards by hand was also out of the question, we  we settled on a more reasonable amount: 16 imaging elements.  This number still allows us to place special filters on a few of the imaging elements, while also providing standard color vision on the remaining nodes.

\IMAGE{ant_brain}{0.8}{Presumed "hardware layout" of the ant-brain: Image data is merged within the visual circuit, and then forwarded to the deeper parts of the brain for further processing (e.\,g. to the navigational systems)}

\IMAGE{conceptual_layout}{1.0}{Conceptual hardware layout of our image capturing chain: Camera images get merged in a central micro-controller, and then forwarded to the connected computer.}

\section{Structure of this thesis}
The structure of this dissertation closely follows the \textit{bottom-to-top} approach that was used to implement this project: 

First we start with discussing several possible hardware components that could potentially be used for sensing light (chapter \ref{sec:hardware}). We look at a few of the options out there, and settle on the OV7670 camera module which we will describe in more detail.

In chapter \ref{sec:cameraboard} we then implement the lowest level of the hardware -- the equivalent of a single ommatidium -- in the form of a small camera board, one of which is attached to each camera. The board will be designed, soldered and programmed from scratch, and then tested with a simple computer connection.

Chapter \ref{sec:mergeboard} then deals with combining the data produced by each ommatidium (camera board) into one coherent image. This is done in on another hardware board by using 4 shared buses, and a specialized round-robin update loop setup.  

As a full image is produced, we worry about how to interface our boards with a normal computer. We talk about USB connections and protocols, and we show how our API for the computer endpoint is designed, and how we can efficiently deal with a high load of data (chapter \ref{sec:api}).

The last part of the implementation -- chapter \ref{sec:sphere} -- deals with putting all the ommatidia into a spherical, eye-like physical model. Here we also discuss how this hand-made prototype can be calibrated manually and automatically so that correctly overlapped images are produced.

After the implementation of the compound ant-eye has been explained, in chapter \ref{sec:eval} we will try evaluate the performance of this prototype by looking at the resulting images, and by using some simple navigation algorithms.

We finish with a conclusion and summary on what we have learned, and also provide a list of potential improvements that can be done to this and future prototypes using the same camera module.

