\chapter{Merge Board}
\label{sec:mergeboard}
At this point we can capture full images from each of the cameras, and present them via the UART protocol described in section \ref{sec:uart}. However, we still do not have any means of creating one compound camera image from all 16 cameras, no means of transferring it to a computer. For this purpose another board has been designed (figure \ref{fig:mergeboard}).

\IMAGE{mergeboard}{0.9}{The fully assembled Merge Board: No special PCB was required due to less dense space constraints; the cameras are connected at the pin array in the bottom via dupont cables.}

\section{Hardware Choice}
For this board we require another micro-controller that is capable of merging the image data. There are a few critical points here to consider:

\begin{description}
 \item[Need to read in data in parallel:] To be able to continuously merge images at a frequency of $30$\,Hz from $16$ cameras, we need to be able to read in data in parallel from multiple cameras at once. To this end we need a controller capable of performing DMA based operations.
 
 \item[Native USB:] We also need to worry about how we are going to transfer data from our controller to the connected computer. An USB to Serial Port bridge is an option, but is limited in speed (usually up to a maximum of $115200$ Baud). A better option would be a native USB 2.0 device, which would theoretically give us a bandwidth of up to $12$\,MBit, or $1.5$\,MB/s.
 
 \item[CPU frequency:] As we also might be required to do some small image processing on this device, a controller with some spare CPU power might be a nice-to-have. The controller should be running at a faster frequency than the slave boards since it represents a congestion point.
\end{description}

\noindent We decided to go with a PIC32 micro-controller, which is a 32-bit micro controller running at $80$\,MHz: The \textit{PIC32MX675F256H} also has 6 integrated UARTs, and similar amounts of I$^2$C and SPI channels, thus giving us plenty of resources to play around with. 
It also has a fairly large amount of RAM: $64$\,KB  -- of which we will occupy $2300~\text{bytes} \times 16~\text{cameras} = 36.8$\,KB alone with our merged frame buffer.

\subsection{Shared UART Bus}
We have at least 16 cameras to interface, and we would like to use a serial protocol such as SPI or UART so that we can make use of a DMA transfers without having to reorder bits after the transfer is done\footnote{This would e.\,g. be required if we would connect all 16 cameras to the Parallel Master Port, since we would read in camera data in an interleaved pattern this way}. Unfortunately most state-of-the-art micro-controllers only have up to 4 or 6 UART/SPI interfaces available, meaning we have to share these buses. Both of those protocols, however, are not made for multiple devices on the same bus. 

The I$^2C$ protocol, as mentioned previously, is an addressable bus and would be suitable for this task -- its clock frequency, however, is limited to a maximum of $400$\,KHz ($100$\,KHz for normal operation). Since I$^2C$ is a passive bus that makes use of resistors instead of actively pulling voltages up to a certain level, it won't work well over longer distances and at higher clock speeds. This makes this protocol unsuitable for us.

Instead, we made use of the UART protocol, but slightly adjusted it for our own purpose: We use 4 UART buses in parallel. For each UART bus, we have a shared transmit line (TX) and receive line to which 4 cameras are connected. Whenever we send out a byte/char of data, every camera node on the bus will receive this command. For this purpose the previously mentioned unique camera identifier (e.\,g. 'A') was introduced. Since communication from the master board to each camera is rather sparse (we only request a frame, which results in a 3 byte command sequence), but the opposite direction is very dense, we optimized for this use case.

The problem (and benefit) of this layout and the UART protocol, however, is that all output lines are driven actively. That means that each of the 16 cameras is trying to actively drive its transmit line (or our shared RX/receive line) to a certain level. Since multiple cameras are on a single bus, this will result in bus collisions and thus short circuits. To circumvent this, two constraints have been put up: 

\begin{enumerate}
 \item Cameras who are not actively being addressed must have their their TX line disabled (in tri-state), thus freeing the bus.
 \item Cameras should not respond with any data to a command that is addressed to the '@' wild card address that affects all cameras on the bus. This way no collisions can occur.
\end{enumerate}


\section{Board schematics and considerations}
Since this board is a one-off prototype, no special schematic or PCB was designed as this would have interfered with our time constraints. Instead a simple standard prototyping board was used, and components were soldered directly by hand. A pseudo schematic for this board can be seen in figure \ref{fig:merge_board_schematic}.

\IMAGE{merge_board_schematic}{1.0}{Schematic for the Merge Board: A PIC32 micro-controller connects the cameras to the computer using 4 shared UART buses, and USB 2.0.}

\noindent As already mentioned, the camera boards are connected in clusters of 4 cameras to 4 different UART buses (16 cameras in total), and a USB connector is directly attached to the controller using its hardware USB peripheral. Two $3.3$\,V voltage regulators are used for the 16 camera boards, and the controller itself is powered by a voltage of $2.8$\,V to ensure that the camera boards and the merge board run at the same voltage. A simple pin-header aligned in a $4 \times 16$ grid was use as a connector for the camera boards. Additionally, a few status LEDs and DIP switches have been added to provide run-time feedback/configuration methods.

\subsection{Breakout Board}
Since the PIC32 controller is only available in a TQFP package which cannot be soldered on a standard $2.54$\,mm prototyping board, an extra breakout board was required. The board (see figure \ref{fig:tqfp_breakout}) was also quickly designed, and then manufactured in the workshop of the University of Edinburgh.

\IMAGE{tqfp_breakout}{0.45}{The TQFP breakout board allows us to put the controller on a normal $2.54$\,mm prototyping board.}

\subsection{Voltage Regulators}
Initially, only one voltage regulator was present on the whole board. But since the 16 camera boards draw quite a bit of current, and since all regulators used are LDOs (linear low drop voltage regulators) instead of switching regulators, they were less efficient than expected. To prevent overheating of the one regulator, a secondary regulator was added, and half of the cameras were switched to use the second regulator.

We also noticed the importance of filter capacitors here: Due to a missing filter capacitor half of the 16 cameras would produce a noisy image that could not be explained. Adding the missing $50 \mu F$ capacitor on the output side of the second regulator helped to solve this issue (see figure \ref{fig:camera_board_noisy_voltage}).

\IMAGE{camera_board_noisy_voltage}{0.6}{A missing capacitor on a voltage regulator caused noisy images (right) on 8 of the 16 cameras.}

\subsection*{USB Noise}
The USB data lines of the controller had to be extra shielded/routed away from the camera bus since the USB peripheral is running at a frequency of $48\,MHz$, and thus extremely sensitive to noise. This can still be noticed when -- for example -- putting a finger onto the USB lines on the TQFP breakout board, as this will cause the USB device to detach/freeze immediately.

\section{Merging 16 cameras into one image}
At this point we now have a master board with 16 attached cameras, all on 4 shared UART buses. But we are still missing the firmware for this master board. How can we efficiently get images from our camera boards into the memory of our controller?

\subsection{Sequential Camera Readout}
\noindent As multiple cameras are on a single bus, we have to read out the data in a sequential format: We ask the first camera ('A') to send us a frame, then record the data response into our frame buffer using a DMA transfer. When the bus becomes idle again (no communication for at least 4 ticks), we ask the next camera ('B') for a frame. This is repeated in a round-robin fashion to update our frame buffer, and runs as fast as possible in an open-loop.

\IMAGE{merge_board_command_framing}{1.0}{Logic Analyzer Dump of the data transmitted to/from the merge board while reading out a frame:  'R' 'A' 'F' is sent after the bus has been idle, and thus a new frame is requested from camera A. The camera responds immediately with the frame data (here, test data: 'S, 'A', 'B', 'A', \dots). }

\noindent No special synchronization between the different camera buses is happening, thus the readout might slightly diverge across buses after a while. VSync/waiting for the slowest bus is certainly an option, but since there is no concrete advantage to this in our use case we instead went with the "as-fast-as-possible" approach. The data is saved in our frame-buffer by using a simple DMA transfer to a pre-calculated offset in the memory:

\begin{equation}
\text{dmaDestination} = \&\text{framebuffer}[0] + \text{cameraFrameSize} \cdot i_\text{camera}
\end{equation}

\begin{figure}[htb]
\centering
\begin{minipage}[b]{0.43\linewidth}
\IMAGEx{merge_board_connected}{1.0}{The camera board with all 16 cameras attached.}
\end{minipage}
\quad
\begin{minipage}[b]{0.53\linewidth}
\IMAGEx{merge_board_interleaving}{1.0}{Cameras are queried in a cyclic, sequential fashion on the same bus, but in parallel along all 4 UART buses.}
\end{minipage}
\end{figure}


\subsection{Bus Idle Detection}
The bus idle detection has to be carefully chosen: If a wait time too small is chosen, we assume the camera is done responding when it is still in the process of processing a command; if the value is too large, we waste valuable time and decrease the maximum frequency we can achieve. This calculation has to include potential interrupts happening in the camera board that might cause the main loop execution to be delayed, thus causing longer bus idle period than usual.

Instead of relying on the bus being idle, we tried reading fixed-size frames at first. This, however, turned out to be unreliable (especially during the initialization phase, or while changing frame sizes): The bus would randomly hang, or frames would be offset by a random amount of pixels. Switching to the bus idle detection helped with this problem, and allows us to continue reading out cameras even if a single module should fail/hang.

\subsection{Probing the Camera Bus}
In order to be able to use this readout approach, we need to know what cameras are located on which bus. We figure this out by initially probing each bus by sending a command to each of the possible cameras from 'A' \dots 'Z', and by checking if we get a response or not. When we get a response, we record the camera ID so that we can later query it in our sequential readout loop. A small timeout is used in case the camera is not present.

\subsection{Maximum Bus Speed}
The total bus speed is dependent on the number of cameras on a single bus. With 4 cameras connected, we can achieve frequencies of roughly $10$\,Hz at a $(40,~30)$ pixel resolution. If only two cameras are on a single bus, we can read the same resolution at $20$\,Hz. The same happens if we half the image width. The underlying UART bus is running at a speed of $1$\,MHz; higher frequencies will not work due to the CPU in the slave board being too busy, and thus failing to respond/recognize a command in time.

\begin{equation}
  d_{img} = 40~\text{px} \times 30~\text{px} \times 2~\text{bpp} = 2400~\text{bytes (image size)}
\end{equation}

\begin{equation}
   t_{req} = d_{img} + 3~\text{bytes/request} + 2~\text{bytes idle delay} = 2405~\text{bytes/frame}
\end{equation}

\begin{equation}
	f_{bus} = \frac{1~\text{MHz}}{t_{req} \times 4~\text{cameras/bus} \times 10~\text{bits per byte}} \approx 10.39~\text{Hz}
\end{equation}

\noindent The calculation here includes the UART framing, which requires on additional start and stop bit for each byte, thus giving us $10$ bits/byte.

Since we aim for a high frame-rate $\geq 30$\,Hz, we provide two different modes of operation: A low frequency $10$\,Hz mode providing a $(40,~30)$-pixel image per camera, and a high frequency $30$\,Hz mode providing a $(20,~15)$-pixel image.

\section{USB computer connection}
The full speed USB 2.0 specification allows a bandwidth of up to $12$\,Mbit/s, and a theoretical limit which is slightly lower than this (cmp. \cite{usb}). To achieve this bandwidth, an USB bulk endpoint has to be used. Simple interrupt endpoints such as used by standard HID\footnote{Human Interface Device} devices (e.\,g. mice and keyboard) will not work for this, since they are limited to simple 8 byte transfers per request, and a polling interval of roughly $10$\,ms.

A bulk endpoint is an endpoint that can use any spare bandwidth on an USB port, and transmit data as fast as it likes. There are no guarantees on when the data arrives, but it guaranteed to arrive in order and without any packet loss.

Using a bulk endpoint in practice is rather hard though -- especially under operating systems like Windows: In contrast to HID devices, which are installed automatically and do not require any custom system drivers, devices using bulk endpoints need a custom written driver to be recognized correctly. Windows, however, requires custom drivers to be signed using an expensive signing process, which is not an option for us. An alternative thus had to be found.

\subsection*{CDC ACM Device}
The CDC ACM device category is typically used for devices like serial port emulators, or serial port to USB converters, and exactly provides what we are looking for: Devices here have one control end point on which device configuration data is transmitted, and two bulk endpoints used for actually transmitting and receiving data.

We have thus decided to simply emulate a serial port, and ignore the baud/speed settings the computer is sending us, but instead send data as fast as possible. This allows us to send arbitrary amounts of data as fast as possible from the controller to the computer. It also allows us to reuse existing drivers, and to use the serial port interfaces integrated in most programming languages.

Using this technique, we can write data up to $874.9$\,KB/s, which is close the theoretical limit of around $1.05$\,MB/s. This speed also heavily depends on the USB host/computer, and system drivers used; A simple Raspberry Pi -- for example -- is much slower at processing the incoming data.

\subsection*{Image Framing}
Since the serial port is a continuous stream of data, a framing marker for the beginning of the frame ('B', 'O', 'F'), and for the end of a frame ('E', 'O', 'F') was inserted into the data stream. By first scanning the input for the first instance of the BOF marker, and then reading the presumed image size (2400 bytes) and checking for the presence of the EOF marker, we can always effectively read and find frames, even if we start reading in the middle of the stream. The computer application receiving the data only has to find the BOF marker once, and can then continue to read frames of $2400+6$ bytes of size continuously. Thus even if a BOF marker would occur randomly in the data in the middle of the frame stream (highly unlikely), this would not confuse the reading application, as we would eventually align on the correct (BOF, EOF)-marker pair.

\section{Program Flow}
The design of the program for this controller was slightly different to that of the PIC24F series due to the added complexity of the USB Stack. We followed the patterns presented by the \textit{MPHarmony Framework} from Microchip, which is also used to provide the basic USB Stack: In this model, the low-level logic is encapsulated in drivers and system modules that are periodically called in the main loop (and also by interrupts); and the application logic is called in the same loop, and makes use of many state machines and sub-state machines to transition between different program states. Our design for the software can be seen in figure \ref{fig:merge_board_flow}.

\IMAGE{merge_board_flow}{1.0}{Application flow diagram for the Merge Board Controller: Multiple State machines (right) are run in parallel and independently of each other, and are called by the main loop (left).}

\subsection*{Merged Image}
The merged image produced by this board can be seen in figures \ref{fig:camera_board_final_image} (raw form), and \ref{fig:camera_board_output} (processed form). No alignment or calibration has happened yet; this is reserved for the client application which will be described in chapter \ref{sec:panorama}. 

A fixed frame-buffer position was calculated for each camera ID so that changing the order of the cables on the UART buses and detaching single cameras would not confuse our visualization software. We first also implemented a dynamic frame-buffer that would be re-sized depending on the number of cameras, and would thus allow for higher transmission frame-rates. This functionality was, however, removed for the final version with 16 cameras.

\IMAGE{camera_board_final_image}{0.55}{The raw merged image as visible on the PC: Here a test image/empty frame-buffer is shown. Each camera is placed on a different row-multiple of the frame-buffer.}

\IMAGE{camera_board_output}{0.6}{Output of the camera board, scaled up and aligned in a $4 \times 4$ grid. The camera ID (yellow) can be seen on the top left of each image.}
