1 NC
2 AGND Power Analog ground
3 SIO_D I/O SCCB serial interface data I/O
4 AVDD Power Analog power supply (VDD-A= 2.45 to 3.0 VDC)
5 SIO_C Input SCCB serial interface clock input
6 RESET Function (default = 0) Clears all registers and resets them to their default values. Active high, internal pull-down resistor.
7 VSYNC Output Vertical sync output
8 PWDN Function (default = 0) Power Down Mode Selection - active high, internal pull-down resistor. 0: Normal mode 1: Power down mode
9 HREF Output HREF output
10 DVDD Power Power supply (VDD-C= 1.8 VDC + 10%) for digital output drive
11 DOVDD Power Digital power supply (VDD-IO= 2.5 to 3.3 VDC)
12 Y7 Output Output bit[7] - MSB for 10-bit RGB and 8-bit YUV
13 XCLK1 Input Crystal clock input
14 Y6 Output Output bit[6 ]
15 DGND Power Digital ground
16 Y5 Output Output bit[5 ]
17 PCLK Output Pixel clock output
18 Y4 Output Output bit[4 ]
19 Y0 Output Output bit[0 ] - LSB for 8-bit YUV
20 Y3 Output Output bit[3 ]
21 Y1 Output Output bit[1 ]
22 Y2 Output Output bit[2 ]
23 NC
24 NC