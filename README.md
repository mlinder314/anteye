MSc Dissertation: "A compound eye built from miniature cameras"
====================================================================
This repository contains the digital submission of the MSc thesis "A compound eye built from miniature cameras" 
by Matthias Linder at the University of Edinburgh (2014). The repository includes the schematics and designs
for the hardware created, the software for the micro-controllers firmware, and the visualization applications
created. The original LaTeX documents are also included.

Directory Structure
------------------------
The repository contains the following folders:

- *./AntEyeSharp/* contains the developed .NET API and visualization projects, i.e. everything that runs on the computer
- *./arduino/* contains simple Arduino test sketches used during development
- *./boards/* contains the designed PCBs, schematics, and also datasheets for all the components used
- *./images/* contains screen-shots and images taken during the development of this thesis
- *./pic/* contains the firmware written for the two hardware boards (camera board and merge board)
- *./proposal/* contains the original research proposal for this projects
- *./thesis/* contains the LaTeX files of the dissertation

To open the files presented here, the following software might be required:

- Visual Studio 2013
- MPLAB
- Eagle CAD Software
- Texmaker

Questions?
------------------------
If you have any questions, feel free to contact me at *matthias@matthiaslinder.com* 

