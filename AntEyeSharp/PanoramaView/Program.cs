﻿using AntEye;
using AntEye.Hardware;
using AntEye.Processing;
using OpenTK.Input;

namespace PanoramaView
{
  /// <summary>
  /// Provides a simple panorama view of ant eye
  /// </summary>
  class Program
  {
    static void Main(string[] args)
    {
      // Load the config
      Config.Instance = Config.LoadConfig("anteye.json");

      // Initialize the board
      var board = CameraMasterBoard.Attach();
      board.AutomaticBrightnessControl = false;
      board.HighSpeedMode = false;
      board.ResetCameras();
      board.StartProcessingData();

      // Initialize the panorama view
      var panorama = new AntEye.Visualization.PanoramaView();
      panorama.KeyDown += (obj, eargs) =>
      {
        if (eargs.Key == Key.BackSpace)
          board.ResetCameras();
        if (eargs.Key == Key.B)
        {
          board.AutomaticBrightnessControl = !board.AutomaticBrightnessControl;
          board.ResetCameras();
        }
      };
      board.FrameReceived += frame => panorama.UpdateFrame(FrameLabel.Label(board, frame));

      // Show the form
      panorama.Run();
    }
  }
}
