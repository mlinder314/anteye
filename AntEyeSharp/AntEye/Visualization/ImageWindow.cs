﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AntEye.Visualization
{
  /// <summary>
  /// Shows a quick image window that is continously updated. Very useful for debugging purposes
  /// </summary>
  public sealed class ImageWindow
  {
    // Auto alignment ...
    private static Point currentStartPosition = new Point(0, 0);
    private static int maxY = 0;

    private readonly Form form;
    private readonly PictureBox pictureBox;
    private bool firstFrame = true;
    private bool viewFrozen = false;

    public ImageWindow(string title = "Image", bool zoom = true, Size? size = null)
    {
      // Create a simple render window
      pictureBox = new PictureBox();
      pictureBox.Dock = DockStyle.Fill;
      pictureBox.SizeMode = zoom ? PictureBoxSizeMode.Zoom : PictureBoxSizeMode.CenterImage;

      form = new Form();
      form.Text = title;
      form.ClientSize = new Size(40, 30);
      form.Controls.Add(pictureBox);
      form.FormClosed += (o, e) => Application.Exit(); // close application on form close
      form.KeyDown += OnKeyDown;

      if (size != null)
      {
        firstFrame = false;
        form.ClientSize = size.Value;
        form.StartPosition = FormStartPosition.Manual;

        // Try to automatically align windows neatly
        int startX = currentStartPosition.X;
        int startY = currentStartPosition.Y;
        if (startX + form.Width >= Screen.PrimaryScreen.WorkingArea.Width)
        {
          startX = 0;
          startY = maxY;
        }
        form.Location = new Point(startX, startY);
        maxY = Math.Max(maxY, form.Location.Y + form.Height);
        currentStartPosition = new Point(startX+form.Width, startY);
      }
    }

    private void OnKeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Space) viewFrozen = !viewFrozen;
      if (e.KeyCode == Keys.S)
      {
        var file = form.Text + ".png";
        pictureBox.Image.Save(file, ImageFormat.Png);
        MessageBox.Show("Image saved to '" + file + "'!");
      }
    }

    /// <summary>
    /// Updates the displayed frame
    /// </summary>
    /// <param name="frame"></param>
    public void ProcessFrame(Bitmap frame)
    {
      // Only do this is the form is already active
      if (!form.Visible || form.IsDisposed || form.Disposing || viewFrozen)
        return;

      // Create a copy of the frame (PictureBox doesn't like multithreading...)
      Bitmap copy;
      lock (frame)
        copy = new Bitmap(frame);

      // Update the displayed image
      pictureBox.Invoke(new Action(() => {
        if (firstFrame) {
          form.ClientSize = copy.Size;
          firstFrame = false;
        }
        pictureBox.Image = copy;
      }));
    }

    /// <summary>
    /// Shows this form
    /// </summary>
    public void Show()
    {
      form.Show();
    }
  }
}
