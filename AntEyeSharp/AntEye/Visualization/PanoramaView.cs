﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using AntEye.Processing;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace AntEye.Visualization
{
  /// <summary>
  /// A 3D first-person view of the ant-eye's sight
  /// @author mlinder
  /// </summary>
  public class PanoramaView : GameWindow
  {
    /// <summary>
    /// Distance of the panels to the center (determines image scale)
    /// </summary>
    private const float CAM_DIST = CameraSphere.SPHERICAL_DISTANCE_FROM_CENTER;

    /// <summary>
    /// The last captured frame
    /// </summary>
    private Bitmap lastFrame;

    /// <summary>
    /// True if curved camera planes are used
    /// </summary>
    public bool UseCurvedPlanes { get; set; }

    /// <summary>
    /// True if the camera perspective is frozen
    /// </summary>
    public bool FreezeCamera { get; set; }

    /// <summary>
    /// True if calibration values are used
    /// </summary>
    public bool UseCalibrationValues { get; set; }

    /// <summary>
    /// Selected Camera letter
    /// </summary>
    public char SelectedCamera { get; set; }

    /// <summary>
    /// Test points to draw (if any)
    /// </summary>
    public static Vector3[] TestPoints { get; set; }

    /// <summary>
    /// Test lines to draw (if any)
    /// </summary>
    public static Vector3[] TestLines { get; set; }

    /// <summary>
    /// Updates the displayed frame
    /// </summary>
    public void UpdateFrame(Bitmap frame)
    {
      lastFrame = new Bitmap(frame);
    }

    public PanoramaView(int width = 1200, int height = 600)
      : base(width, height, GraphicsMode.Default, "AntEye Panorama View", GameWindowFlags.Default)
    {
      UseCurvedPlanes = true;
      UseCalibrationValues = true;
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      GL.Viewport(0, 0, Width, Height);
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      VSync = VSyncMode.Off;

      // Set up GL parameters
      GL.ClearColor(Color.FromArgb(32, 32, 32));
      GL.CullFace(CullFaceMode.FrontAndBack);
      GL.Enable(EnableCap.Texture2D);
      GL.Enable(EnableCap.DepthTest);

      // Setup projection
      const float near = 1f;
      const float far = CAM_DIST * 2;
      GL.MatrixMode(MatrixMode.Projection);
      float aspect = Width / (float)(Height);
      const float fov = 90 * (float)Math.PI / 180f; /* 180 degrees */
      Matrix4 projection;
      Matrix4.CreatePerspectiveFieldOfView(fov, aspect, near, far, out projection);
      GL.LoadMatrix(ref projection);

      // Bind frame texture
      int tex = GL.GenTexture();
      GL.BindTexture(TextureTarget.Texture2D, tex);
    }

    protected override void OnRenderFrame(FrameEventArgs e)
    {
      base.OnRenderFrame(e);

      // Clear all
      GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

      // Setup camera
      var center = Vector3.Zero;
      var xrot = (float)((Mouse.Y - Height / 2) * 1.75f * Math.PI / Height);
      var yrot = -(float)((Mouse.X - Width / 2) * 1.75f * Math.PI / Width);
      var zrot = 0.0f;

      GL.MatrixMode(MatrixMode.Modelview);
      if (!FreezeCamera)
      {
        var rotationMatrix = Matrix4.CreateRotationX(xrot)*Matrix4.CreateRotationY(yrot)*Matrix4.CreateRotationZ(zrot);
        var eyeTarget = (Vector3.TransformVector(new Vector3(0, 0, 1f), rotationMatrix)) + center;
        var camera = Matrix4.LookAt(center, eyeTarget, Vector3.UnitY);
        GL.LoadMatrix(ref camera);
      }

      // Blit frame into texture
      var frame = lastFrame;
      int cams = 1;
      if (frame != null)
      {
        cams = frame.Height / 30;
        BitmapData data = frame.LockBits(new Rectangle(0, 0, frame.Width, frame.Height),
          ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

        GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
          OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

        frame.UnlockBits(data);
      }
      GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
      GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

      // Draw all panels
      foreach (var camID in Config.Instance.CameraIDs)
      {
        GL.PushMatrix();
        var panelRot = CameraSphere.GetTransformation(camID, UseCalibrationValues);
        var panelRotGL = new Matrix4(
          panelRot.V00, panelRot.V01, panelRot.V02, panelRot.V03,
          panelRot.V10, panelRot.V11, panelRot.V12, panelRot.V13,
          panelRot.V20, panelRot.V21, panelRot.V22, panelRot.V23,
          panelRot.V30, panelRot.V31, panelRot.V32, panelRot.V33);

        // OpenTK uses transposed matrices (as apposed to AForge)
        panelRotGL.Transpose();

        //camrot2.Row3 = lastCol;
        //camrot2.Column3 = lastRow;
        GL.MultMatrix(ref panelRotGL);
        
        float spanY = 1.0f / cams;
        float texY = (camID - 'A') * spanY;

        GL.Color3(Color.White);
        DrawCameraPlane(camID, texY, spanY);

        GL.PopMatrix();
      }

      // Draw testlines
      if (TestLines != null)
      {
        GL.PushAttrib(AttribMask.AllAttribBits);
        GL.LineWidth(2f);
        GL.Color3(Color.Lime);
        GL.Disable(EnableCap.Texture2D);
        GL.Disable(EnableCap.DepthTest);

        GL.Begin(PrimitiveType.Lines);
        foreach (var pt in TestLines)
          GL.Vertex3(pt);
        GL.End();

        GL.PopAttrib();
      }
      
      // Draw testpoints
      if (TestPoints != null)
      {
        GL.PushAttrib(AttribMask.AllAttribBits);
        GL.PointSize(4f);
        GL.Color3(Color.Red);
        GL.Disable(EnableCap.Texture2D);
        GL.Disable(EnableCap.DepthTest);

        GL.Begin(PrimitiveType.Points);
        foreach (var pt in TestPoints)
          GL.Vertex3(pt);
        GL.End();

        GL.PopAttrib();
      }

      SwapBuffers();
    }

    /// <summary>
    /// Draws a centered camera plane at z=CAM_DIST with the given texture coordinates
    /// </summary>
    /// <param name="texY">TexCoord Y start</param>
    /// <param name="spanY">TexCoord Y length</param>
    private void DrawCameraPlane(char cam, float texY, float spanY)
    {
      if (!UseCurvedPlanes)
      {
        // Simple planar mesh (40x30) ..
        GL.Begin(PrimitiveType.Quads);
        GL.TexCoord2(0, texY);
        GL.Vertex3(0, 0, 0); // bottom left

        GL.TexCoord2(0, texY + spanY);
        GL.Vertex3(0, 30, 0); // top left

        GL.TexCoord2(1, texY + spanY);
        GL.Vertex3(40, 30, 0); // top right

        GL.TexCoord2(1, texY);
        GL.Vertex3(40, 0, 0); // bottom right
        GL.End();
        return;
      }

      // Complex concave/spherical mesh
      const int densityX = 12;
      const int densityY = densityX * 3 / 4;

      GL.Begin(PrimitiveType.Quads);
      var zoom = Config.Instance.GetAlignment(cam, UseCalibrationValues).Zoom;
      for (int x = 0; x < densityX; x++)
      {
        for (int y = 0; y < densityY; y++)
        {
          // Draw a single quad
          for (int i = 0; i < 4; i++)
          {
            // Find corner positions
            int dx = x + ((i >= 1 && i <= 2) ? 1 : 0);
            int dy = y + ((i >= 2) ? 1 : 0);

            // Texture coords map over the entire X-domain, and one n-th of the y-domain
            GL.TexCoord2(dx / (float)densityX, texY + spanY * dy / densityY);

            // vertices are just centered 40x30
            float depth = zBend((float) dx/densityX, (float) dy/densityY, CAM_DIST / (float)(1+zoom));
            GL.Vertex3(40f * dx / densityX, 30f * dy / densityY, depth);
          }
        }
      }
      GL.End();
    }

    /// <summary>
    /// Calculates the depth of the bend of the plane at the given x,y percentage
    /// </summary>
    /// <param name="xPercent"></param>
    /// <param name="yPercent"></param>
    /// <param name="zDist"></param>
    /// <returns></returns>
    private float zBend(float xPercent, float yPercent, float zDist)
    {
      const float horizontalFieldOfView = 45;
      const float xBendFactor = horizontalFieldOfView; // percent to degree (also: field of view)
      const float aspectRatio = 30f / 40f;
      const float yBendFactor = xBendFactor * aspectRatio; // percent to degree
      return (float)(zDist * Math.Cos((xPercent - 0.5) * xBendFactor * Math.PI / 180f)
                           * Math.Cos((yPercent - 0.5) * yBendFactor * Math.PI / 180f)) - zDist;
    }

    protected override void OnKeyDown(KeyboardKeyEventArgs e)
    {
      base.OnKeyDown(e);
      if (e.Key == Key.Escape)
        Close();
      else if (e.Key == Key.Space)
        FreezeCamera = !FreezeCamera;
      else if (e.Key == Key.F1)
        UseCalibrationValues = !UseCalibrationValues;
      else if (e.Key == Key.F2)
        UseCurvedPlanes = !UseCurvedPlanes;
      // Manual Calibration
      else if (e.Key >= Key.A && e.Key <= Key.Z)
        SelectedCamera = (char) (e.Key - Key.A + 'A');
      else if (e.Key >= Key.Number1 && e.Key <= Key.Number8)
      {
        int rotOffset = (e.Key == Key.Number1) ? 1 : (e.Key == Key.Number2) ? -1 : 0;
        int xOffset = (e.Key == Key.Number3) ? -1 : (e.Key == Key.Number4) ? 1 : 0;
        int yOffset = (e.Key == Key.Number5) ? -1 : (e.Key == Key.Number6) ? 1 : 0;
        var zoomOffset = (e.Key == Key.Number7) ? -0.05f : (e.Key == Key.Number8) ? 0.05f : 0;
        var cfg = Config.Instance.CameraCalibrationOffsets.FirstOrDefault(c => c.ID == SelectedCamera);
        if (cfg == null)
        {
          cfg = new Config.CameraAlignment();
          cfg.ID = SelectedCamera;
          Config.Instance.CameraCalibrationOffsets = Config.Instance.CameraCalibrationOffsets.Concat(new[]{cfg}).ToArray();
        }
        cfg.Rotation += rotOffset;
        cfg.Horizontal += xOffset;
        cfg.Vertical += yOffset;
        cfg.Zoom += zoomOffset;
      }
    }
  }
}
