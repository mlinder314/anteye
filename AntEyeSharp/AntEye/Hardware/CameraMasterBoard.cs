﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Threading;
using AForge.Math;

namespace AntEye.Hardware
{
  /// <summary>
  /// Implements the interface for the AntEye Camera Master board.
  /// Connects to the board using a USB CDC serial port
  /// @author matthias@matthiaslinder.com
  /// </summary>
  public sealed class CameraMasterBoard : ICameraBoard, IDisposable
  {
    /// <summary>
    /// The beginning of the PNP device id string by which the camera board can be identified
    /// Made up out of the USB vendor and product id, and the device serial number.
    /// </summary>
    private const string PNPDeviceID = "\\VID_04D8&PID_000A\\MATTHIASLINDER.COM:ANT-EYE";

    /// <summary>
    /// Number of bytes per pixel
    /// </summary>
    private const int INPUT_BYTES_PER_PIXEL = 2;

    /// <summary>
    /// Input pixl format
    /// </summary>
    private const PixelFormat INPUT_PIXEL_FORMAT = PixelFormat.Format16bppRgb565;

    /// <summary>
    /// The markers used for signaling the start of a frame
    /// </summary>
    private static readonly char[] BeginningOfFrameMarker = {'B', 'O', 'F'};

    /// <summary>
    /// The markers used for signalling the end of a frame
    /// </summary>
    private static readonly char[] EndOfFrameMarker = { 'E', 'O', 'F' };

    /// <summary>
    /// Defines the size of a full frame (all cameras) combined)
    /// </summary>
    public static readonly Size FullCameraFrameSize = new Size(40, 30);

    /// <summary>
    /// Defines the size of a highspeed frame (all cameras combined)
    /// </summary>
    public static readonly Size HighspeedCameraFrameSize = new Size(20, 30);

    #region Static Methods
    /// <summary>
    /// Returns true if the camera board is presently connected to the computer
    /// </summary>
    /// <returns></returns>
    public static bool IsPresent()
    {
      return GetCameraSerialPort() != null;
    }

    /// <summary>
    /// Returns the serial port used by the camera (or null if not present)
    /// </summary>
    /// <returns></returns>
    public static string GetCameraSerialPort()
    {
      var searcher = new ManagementObjectSearcher(@"Select * From Win32_SerialPort");
      return (from ManagementBaseObject device in searcher.Get()
        where ((string) device.GetPropertyValue("PNPDeviceID")).Contains(PNPDeviceID)
        select (string) device.GetPropertyValue("DeviceID")).FirstOrDefault();
    }

    /// <summary>
    /// Tries attaching to the camera master board
    /// </summary>
    /// <returns></returns>
    public static CameraMasterBoard Attach()
    {
      if (!IsPresent())
        throw new IOException("Camera Master Board is currently not attached to the computer");

      var port = new SerialPort(GetCameraSerialPort());
      if (port.IsOpen)
        throw new IOException("Camera Master Board is already in use");

      // Initialize the port
      port.ReadBufferSize = 256*1024;
      port.DtrEnable = false;
      port.RtsEnable = false;
      port.Handshake = Handshake.None;
      port.Open();

      var board = new CameraMasterBoard(port);
      return board;
    }
    #endregion

    #region ICameraBoard

    /// <summary>
    /// Describes how large a single frame is.
    /// </summary>
    public Size TotalFrameSize
    {
      get
      {
        return new Size(CameraFrameSize.Width, CameraFrameSize.Height * NumberOfCameras);
      }
    }

    /// <summary>
    /// Size of a single camera frame
    /// </summary>
    public Size CameraFrameSize { get; private set; }

    /// <summary>
    /// Total number of cameras present in an frame
    /// </summary>
    public int NumberOfCameras
    {
      get { return 16; }
    }

    /// <summary>
    /// True if the automatic brightness adjustment is enabled
    /// </summary>
    public bool AutomaticBrightnessControl { get; set; }

    /// <summary>
    /// True if the 30hz mode is enabled
    /// </summary>
    public bool HighSpeedMode { get; set; }

    /// <summary>
    /// Called whenever a frame has been received
    /// </summary>
    public event Action<Bitmap> FrameReceived;
    #endregion

    /// <summary>
    /// The serial port used for all communication with the board.
    /// </summary>
    private readonly SerialPort port;

    /// <summary>
    /// A worker that continously processes data frames.
    /// </summary>
    private readonly BackgroundWorker frameWorker = new BackgroundWorker();

    /// <summary>
    /// The framebuffer used for storing an entire frame. We use two alternating 
    /// buffers so that reading & processing data can happen in different stages.
    /// </summary>
    private readonly Bitmap[] frameBuffers = new Bitmap[2];

    /// <summary>
    /// Length of the raw input frame buffer
    /// </summary>
    private int rawFrameBufferLength;

    private CameraMasterBoard(SerialPort port)
    {
      this.port = port;
    }

    /// <summary>
    /// Configures the firmware of the board
    /// </summary>
    private void SetupBoard()
    {
      CameraFrameSize = FullCameraFrameSize;

      // Fix output sequence (swap LOW and HIGH byte) for c#'s RGB565 format
      SendI2CConfig(0x3A /* TSLB */, 1 << 3, 1 << 3);

      // Disable automatic brightness control
      SendI2CConfig(0x13, 1 | (1 << 2), (byte)(AutomaticBrightnessControl ? 0xFF : 0x00)); // Dis/En-able AEC and AGC
      SendI2CConfig(0x07, 0xFF, 0x00); // brightness[10:15]
      SetExposure(80); // brightness[2:10]
      SendI2CConfig(0x04, 3, 0x00); // brightness[0:2]
      SetGain(20);

      // Enable true black level calibration
      SendI2CConfig(0xB1, 1 << 2, 0xFF);

      // Fix colors
      SetupColorMatrix();

      // High speed 30hz mode
      if (HighSpeedMode)
        EnableHighSpeedMode();
    }

    /// <summary>
    /// Sets up the initial color matrix
    /// </summary>
    private void SetupColorMatrix()
    {
      // Color correction matrix is given by datasheet
      var colorCorrectionMatrix = Matrix3x3.CreateFromRows(
        new Vector3(1.36f, -0.3f, -0.06f),
        new Vector3(-0.20f, 1.32f, -0.12f),
        new Vector3(-0.04f, -0.55f, 1.59f));

      // Convert RGB to YCbCr
      var crcbConversionMatrix = Matrix3x3.CreateFromRows(
        new Vector3(0.299f, 0.587f, 0.114f),
        new Vector3(-0.168736f, -0.331264f, 0.5f),
        new Vector3(0.5f, -0.418688f, -0.081312f)
        );
      
      // Rotate colors around (why? no idea. but need to do this to get correct colors)
      const float hue = 120f * (float)Math.PI / 180f;
      var hueControlMatrix = Matrix3x3.CreateFromRows(
        new Vector3(1, 0, 0),
        new Vector3(0, (float)Math.Cos(hue), (float)-Math.Sin(hue)),
        new Vector3(0, (float)Math.Sin(hue), (float)Math.Cos(hue))
        );
      // Increase saturation?
      const float gain = 1.0f;
      var colorSaturationMatrix = Matrix3x3.CreateDiagonal(new Vector3(1, gain, gain));

      // Build the combined matrix
      var combinedMatrix = colorSaturationMatrix * hueControlMatrix * crcbConversionMatrix * colorCorrectionMatrix;
      SetColorMatrix(combinedMatrix);
    }

    #region Public Methods

    /// <summary>
    /// Switches to high-speed mode.
    /// </summary>
    private void EnableHighSpeedMode()
    {
      // Send board into high-speed mode
      port.Write(new[] { 'C', 'H' }, 0, 2);

      // Update framebuffer
      CameraFrameSize = HighspeedCameraFrameSize;
    }

    /// <summary>
    /// Resets all connected cameras
    /// </summary>
    public void ResetCameras()
    {
      port.Write(new[] { 'C', 'R' }, 0, 2);
      SetupBoard();
    }

    /// <summary>
    /// Changes an I2C camera register
    /// </summary>
    public void SendI2CConfig(byte addr, byte mask, byte value)
    {
      port.Write(new[] { (byte)'C', (byte)'I', addr, mask, value }, 0, 5);
    }

    /// <summary>
    /// Changes the overall camera exposure settings. A value of 80
    /// is good for a semi-lit room during the day. Lower values = darker image
    /// </summary>
    public void SetExposure(byte brightness)
    {
      SendI2CConfig(0x10, 0xFF, brightness);
    }

    /// <summary>
    /// Changes the overall camera gain settings. Ranges from 0 to 31
    /// </summary>
    /// <param name="gain"></param>
    public void SetGain(int gain)
    {
      SendI2CConfig(0x00, 0xFF, (byte)gain); // AGC[0:7]
      SendI2CConfig(0x03, 3 << 6, (byte)((gain >> 8) << 6)); // AGC[8:9]
    }

    /// <summary>
    /// Changes the 6-component color matrix of the camera (see datasheet)
    /// </summary>
    /// <param name="matrix"></param>
    public void SetColorMatrix(Matrix3x3 matrix)
    {
      // Calculate integer matrix values
      matrix = matrix*128;
      int mtx1 = (int) Math.Round(matrix.V10);
      int mtx2 = (int) Math.Round(matrix.V11);
      int mtx3 = (int) Math.Round(matrix.V12);
      int mtx4 = (int) Math.Round(matrix.V20);
      int mtx5 = (int) Math.Round(matrix.V21);
      int mtx6 = (int) Math.Round(matrix.V22);

      // Set the absolute matrix registers
      SendI2CConfig(0x4F + 0, 0xFF, (byte) Math.Abs(mtx1));
      SendI2CConfig(0x4F + 1, 0xFF, (byte) Math.Abs(mtx2));
      SendI2CConfig(0x4F + 2, 0xFF, (byte) Math.Abs(mtx3));
      SendI2CConfig(0x4F + 3, 0xFF, (byte) Math.Abs(mtx4));
      SendI2CConfig(0x4F + 4, 0xFF, (byte) Math.Abs(mtx5));
      SendI2CConfig(0x4F + 5, 0xFF, (byte) Math.Abs(mtx6));

      // Set the sign bits
      byte minusBits = 0x00;
      if (mtx1 < 0) minusBits |= (1 << 0);
      if (mtx2 < 0) minusBits |= (1 << 1);
      if (mtx3 < 0) minusBits |= (1 << 2);
      if (mtx4 < 0) minusBits |= (1 << 3);
      if (mtx5 < 0) minusBits |= (1 << 4);
      if (mtx6 < 0) minusBits |= (1 << 5);
      SendI2CConfig(0x58, 0x3F, minusBits);
    }

    /// <summary>
    /// Launches the background frame processor
    /// </summary>
    public void StartProcessingData()
    {
      // Setup the board
      SetupBoard();

      // Update the framebuffer
      ChangeFrameBufferLayout();

      // Start the background worker
      frameWorker.WorkerSupportsCancellation = true;
      frameWorker.DoWork += RunFrameWorker;
      frameWorker.RunWorkerAsync();
    }

    #endregion

    /// <summary>
    /// Changes the layout of the frame buffers to the size specified in TotalFrameSize
    /// </summary>
    private void ChangeFrameBufferLayout()
    {
      rawFrameBufferLength = TotalFrameSize.Width*TotalFrameSize.Height*INPUT_BYTES_PER_PIXEL;
      for (int i = 0; i < frameBuffers.Length; i++)
        frameBuffers[i] = new Bitmap(TotalFrameSize.Width, TotalFrameSize.Height, INPUT_PIXEL_FORMAT);
      port.DiscardInBuffer();
    }

    /// <summary>
    /// Runs the background thread for processing the data
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void RunFrameWorker(object sender, DoWorkEventArgs e)
    {
      try
      {
        int bufferIndex = 0;
        var inputBuffer = new byte[0];
        while (!e.Cancel)
        {
          // Find the beginning of the frame
          ReadPastMarker(BeginningOfFrameMarker);

          // Re-init the framebuffer if necessary
          if (inputBuffer.Length != rawFrameBufferLength)
          {
            inputBuffer = new byte[rawFrameBufferLength];
          }

          // Read the frame content into a temporary buffer
          int read = 0;
          do
          {
            read += port.Read(inputBuffer, read, inputBuffer.Length - read);
          } while (read < inputBuffer.Length);

          // FIXME: Swap the R and G channels ...
          //for (int i = 0; i < read; i += 2)
          //{
          //  // from: GGGGGRRR RRRBBBBB 
          //  //   to: RRRRRGGG GGGBBBBB 
          //  int value = inputBuffer[i] | (inputBuffer[i+1] << 8);
          //  int b = value & 0x1F;
          //  int r = (value >> 6) & 0x1F;
          //  int g = (value >> 11) & 0x1F;
          //  value = b | (g << 6) | (r << 11);
          //  inputBuffer[i] = (byte) value;
          //  inputBuffer[i + 1] = (byte) (value >> 8);
          //}

          // Copy the frame into the next free buffer
          var buffer = frameBuffers[bufferIndex++];
          bufferIndex %= frameBuffers.Length;
          lock (buffer)
          {
            var rect = new Rectangle(Point.Empty, buffer.Size);
            var data = buffer.LockBits(rect, ImageLockMode.WriteOnly, INPUT_PIXEL_FORMAT);
            Marshal.Copy(inputBuffer, 0, data.Scan0, inputBuffer.Length);
            buffer.UnlockBits(data);
          }

          // Read the end of frame marker
          int skipped = ReadPastMarker(EndOfFrameMarker);
          Debug.WriteIf(skipped != 0, "Frame EOF not at expected position -- wrong frame size?", "WARN");

          // Process the frame
          OnFrame(buffer);
        }
      }
      catch (Exception ex)
      {
        // Exceptions get silently swallowed otherwise..meh
        Debug.Fail(ex.ToString());
      }
    }

    private void OnFrame(Bitmap frame)
    {
      if (FrameReceived != null)
        FrameReceived(frame);
    }

    /// <summary>
    /// Reads the input stream past the "Beginning-of-Frame" marker
    /// </summary>
    /// <returns>Numbers of bytes read until the (start of the) marker was encountered</returns>
    private int ReadPastMarker(char[] marker)
    {
      // Start reading bytes until we encouter a marker
      int read = 0;
      var window = new byte[marker.Length];
      bool hasMatchingPattern;
      do
      {
        // Shift & read data into the buffer
        for (int i = 0; i < window.Length; i++)
          window[i] = (i == window.Length - 1) ? (byte)port.ReadByte() : window[i + 1];
        read++;

        // Check the window
        hasMatchingPattern = !window.Where((t, i) => t != marker[i]).Any();
      } while (!hasMatchingPattern);
      return read - marker.Length;
    }

    public void Dispose()
    {
      frameWorker.CancelAsync();
      port.Dispose();
    }
  }
}
