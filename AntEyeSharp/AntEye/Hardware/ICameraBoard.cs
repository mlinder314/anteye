﻿using System;
using System.Drawing;

namespace AntEye.Hardware
{
  /// <summary>
  /// Represents a generic camera board that can capture frames
  /// </summary>
  public interface ICameraBoard
  {
    /// <summary>
    /// Returns the size of the total merged frame
    /// </summary>
    Size TotalFrameSize { get; }

    /// <summary>
    /// Returns the size of a single camera frame
    /// </summary>
    Size CameraFrameSize { get; }

    /// <summary>
    /// Total number of cameras present in an image
    /// </summary>
    int NumberOfCameras { get; }

    /// <summary>
    /// Called whenever a frame has been received
    /// </summary>
    event Action<Bitmap> FrameReceived;
  }
}
