﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AForge.Math;

namespace AntEye.Processing
{
  /// <summary>
  /// Matrix helper for AntEye calibration matrices. Requires a valid AntEye config file
  /// </summary>
  public static class CameraSphere
  {
    /// <summary>
    /// Distance of the panels to the center (determines image scale)
    /// </summary>
    public const float SPHERICAL_DISTANCE_FROM_CENTER = 75f;

    /// <summary>
    /// Returns the transformation matrix for a camera to so that a (40,30) x-y-image with a (0,0) origin
    /// is correctly transformed into a spheric ant-eye projection
    /// </summary>
    public static Matrix4x4 GetTransformation(char camera, bool useCalibration = true, double rotationOffset = 0)
    {
      var alignment = Config.Instance.GetAlignment(camera, useCalibration);

      // Start by shifting the plane so that it is centered around (20,15) in front of us
      var translation = Matrix4x4.CreateTranslation(new Vector3(-20, -15, SPHERICAL_DISTANCE_FROM_CENTER / (1+(float)alignment.Zoom)));

      // Mirror the plane vertically (GL starts on the bottom left, pixels start on the top left)
      var mirrorVertically = Matrix4x4.CreateDiagonal(new Vector4(1, -1, 1, 1));

      // Mirror the plane horizontally (to be honest...no idea why. but everything is mirrored otherwise)
      var mirrorHorizontally = Matrix4x4.CreateDiagonal(new Vector4(-1, 1, 1, 1));

      // Then shift it horizontally
      var rotZ = Matrix4x4.CreateRotationZ((float)  ((alignment.Rotation+rotationOffset) * Math.PI / 180.0));
      var rotX = Matrix4x4.CreateRotationX((float) -(alignment.Vertical * Math.PI / 180.0));
      var rotY = Matrix4x4.CreateRotationY((float) -(alignment.Horizontal * Math.PI / 180.0));

      return rotX * rotY * rotZ * mirrorHorizontally * mirrorVertically * translation;
    }
  }
}
