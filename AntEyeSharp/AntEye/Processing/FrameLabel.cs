﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AntEye.Hardware;

namespace AntEye.Processing
{
  /// <summary>
  /// Labels each frame/camera with its ID letter
  /// @author mlinder
  /// </summary>
  public static class FrameLabel
  {
    public static Bitmap Label(ICameraBoard board, Bitmap frame)
    {
      var clone = new Bitmap(frame);
      using (var g = Graphics.FromImage(clone))
      {
        var font = new Font(FontFamily.GenericSansSerif, 5f);
        for (int i = 0; i < board.NumberOfCameras; i++)
        {
          string str = ((char)('A' + i)).ToString();
          g.DrawString(str, font, Brushes.Yellow, 1, 1 + i * board.CameraFrameSize.Height);
        }
      }
      return clone;
    }
  }
}
