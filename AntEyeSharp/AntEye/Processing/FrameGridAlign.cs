﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AntEye.Hardware;

namespace AntEye.Processing
{
  /// <summary>
  /// Aligns the camera input frame in a nice, little grid
  /// @author mlinder
  /// </summary>
  public static class FrameGridAlign
  {
    public static Bitmap Align(ICameraBoard board, Bitmap frame, double aspectRatio = 0.5)
    {
      var cam = board.CameraFrameSize;

      // Calculate the required grid dimensions
      double xCams = Math.Sqrt(board.NumberOfCameras / aspectRatio /* maintain aspect ratio */);
      int xParts = (int)Math.Round(xCams);
      int yParts = (int)Math.Ceiling((double)board.NumberOfCameras / xParts);

      // Copy the camera images into the grid
      var output = new Bitmap(cam.Width * xParts, cam.Height * yParts);
      using (var g = Graphics.FromImage(output))
      {
        for (int i = 0; i < board.NumberOfCameras; i++)
        {
          var src = new Rectangle(0, i * cam.Height, cam.Width, cam.Height);
          var dest = new Rectangle(cam.Width * (i % xParts), cam.Height * (i / xParts), cam.Width, cam.Height);
          g.DrawImage(frame, dest, src, GraphicsUnit.Pixel);
        }
      }
      return output;
    }
  }
}
