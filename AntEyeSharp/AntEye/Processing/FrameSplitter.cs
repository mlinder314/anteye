﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AntEye.Hardware;

namespace AntEye.Processing
{
  /// <summary>
  /// Splits compound camera frames into single frames
  /// @author mlinder
  /// </summary>
  public class FrameSplitter
  {
    /// <summary>
    /// Underlying camera board
    /// </summary>
    public ICameraBoard Board { get; private set; }

    /// <summary>
    /// Defines for which cameras a split frame will not be created. Index 0 = 'A'
    /// </summary>
    public bool[] DisabledCameras { get; set; }

    /// <summary>
    /// Creates a new frame splitter
    /// </summary>
    /// <param name="board">Board from which the data is fetched</param>
    /// <param name="cameras">Cameras to select/process</param>
    public FrameSplitter(ICameraBoard board, params char[] cameras)
    {
      Board = board;
      board.FrameReceived += OnFrame;

      DisabledCameras = new bool[26];
      if (cameras.Length > 0)
      {
        for (int i = 0; i < DisabledCameras.Length; i++)
          DisabledCameras[i] = true;
        foreach (var cam in cameras)
          DisabledCameras[Char.ToUpper(cam) - 'A'] = false;
      }
    }

    private void OnFrame(Bitmap frame)
    {
      if (SplitFrameReceived == null)
        return;

      // Split the frame ...
      lock (frame)
      {
        int xParts = frame.Width/Board.CameraFrameSize.Width;
        int yParts = frame.Height/Board.CameraFrameSize.Height;
        char camera = 'A';
        for (int x = 0; x < xParts; x++)
        {
          for (int y = 0; y < yParts; y++)
          {
            // Copy the camera segment into a new image
            if (!DisabledCameras[camera - 'A'])
            {
              var splitFrame = new Bitmap(Board.CameraFrameSize.Width, Board.CameraFrameSize.Height);
              using (Graphics g = Graphics.FromImage(splitFrame))
                g.DrawImage(frame, new Rectangle(Point.Empty, splitFrame.Size), x*Board.CameraFrameSize.Width,
                  y*Board.CameraFrameSize.Height, splitFrame.Width, splitFrame.Height, GraphicsUnit.Pixel);
              SplitFrameReceived(camera, splitFrame);
            }
            camera++;
          }
        }
      }
    }

    /// <summary>
    /// Processes a split frame
    /// </summary>
    /// <param name="camera"></param>
    /// <param name="frame"></param>
    public delegate void SplitFrameHandler(char camera, Bitmap frame);

    /// <summary>
    /// Called whenever a split frame has been received
    /// </summary>
    public event SplitFrameHandler SplitFrameReceived;
  }
}
