﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using AntEye.Utils;

namespace AntEye
{
  /// <summary>
  /// AntEye Configuration file
  /// @author mlinder
  /// </summary>
  public class Config
  {
    #region Static

    /// <summary>
    /// Default name of the config file
    /// </summary>
    public const string DEFAULT_CONFIG_NAME = "anteye.json";

    /// <summary>
    /// Default instance of this config
    /// </summary>
    public static Config Instance { get; set; }

    /// <summary>
    /// Loads the specified config file (if it exists)
    /// </summary>
    /// <param name="file"></param>
    public static Config LoadConfig(string file)
    {
      if (!File.Exists(file))
        return null;
      using (var fs = File.OpenRead(file))
        return Json.Default.ParseJson<Config>(fs);
    }

    #endregion

    /// <summary>
    /// CameraAlignment configuration file
    /// </summary>
    public CameraAlignment[] Cameras { get; set; }

    /// <summary>
    /// Returns all used camera IDs
    /// </summary>
    public IEnumerable<char> CameraIDs
    {
      get { return Cameras.Select(cam => cam.ID); }
    }

    /// <summary>
    /// Additional calibration offsets
    /// </summary>
    public CameraAlignment[] CameraCalibrationOffsets { get; set; }

    /// <summary>
    /// Retrieves the camera alignment data for the specified camera
    /// </summary>
    public CameraAlignment GetAlignment(char camera, bool includeCalibration = true)
    {
      var alignment = new CameraAlignment();
      alignment.ID = camera;

      // Sum up all alignment data available
      var originalAlignment = Cameras.First(cam => cam.ID == camera);
      foreach (var alig in (includeCalibration) ? 
        new[] {originalAlignment, CameraCalibrationOffsets.FirstOrDefault(cam => cam.ID == camera)} : 
        new[] {originalAlignment})
      {
        if (alig == null)
          continue;
        alignment.Horizontal += alig.Horizontal;
        alignment.Vertical += alig.Vertical;
        alignment.Rotation += alig.Rotation;
        alignment.Zoom += alig.Zoom;
      }

      return alignment;
    }

    /// <summary>
    /// CameraAlignment config
    /// </summary>
    public class CameraAlignment
    {
      /// <summary>
      /// Camera ID, e.g. 'A'
      /// </summary>
      public char ID { get; set; }

      /// <summary>
      /// Horizontal rotation (in degrees, positive = right)
      /// </summary>
      public double Horizontal { get; set; }

      /// <summary>
      /// Vertical rotation (in degrees, positive = up)
      /// </summary>
      public double Vertical { get; set; }

      /// <summary>
      /// Pane rotation (in degrees to the right)
      /// </summary>
      public double Rotation { get; set; }

      /// <summary>
      /// Zoom
      /// </summary>
      public double Zoom { get; set; }
    }

    /// <summary>
    /// The center/origin camera
    /// </summary>
    public char CenterCamera { get; set; }
  }
}
