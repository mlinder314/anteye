﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using AntEye.Visualization;

namespace AntEye.Utils
{
  /// <summary>
  /// Window helper utility
  /// </summary>
  public static class WinUtil
  {
    /// <summary>
    /// Size of all visualization frames
    /// </summary>
    public static readonly Size VisualizationSize = new Size(320, 240);

    /// <summary>
    /// Cached Windows
    /// </summary>
    private static readonly Dictionary<string, ImageWindow> Windows = new Dictionary<string, ImageWindow>();

    /// <summary>
    /// Opens a named window for the specified CV image. Updates an existing window with that
    /// name if found.
    /// Needs any random prior open window to work correctly.
    /// </summary>
    public static void ShowImage(string name, Bitmap img)
    {
      // Retrieve/create the window
      ImageWindow window;
      if (!Windows.TryGetValue(name, out window))
      {
        if (Application.OpenForms.Count == 0)
          return;
        Application.OpenForms[0].Invoke(new Action(() =>
        {
          window = new ImageWindow(name, size: img.Size);
          window.Show();
          Windows.Add(name, window);
        }));
      }

      // Update the frame
      window.ProcessFrame(img);
      Application.DoEvents();
    }
  }
}
