﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AntEye.Hardware;
using AntEye.Processing;
using AntEye.Visualization;

namespace FrameRecorder
{
  public partial class FrameRecorderForm : Form
  {
    private CameraMasterBoard _board;
    private Bitmap _lastFrame;
    private readonly Timer _autoRecordTimer = new Timer();
    private int _autoRecordFrame = 0;

    public FrameRecorderForm()
    {
      InitializeComponent();
    }

    private void exposureSlider_Scroll(object sender, EventArgs e)
    {
      _board.SetExposure((byte)exposureSlider.Value);
    }

    private void gainSlider_Scroll(object sender, EventArgs e)
    {
      _board.SetGain((byte)gainSlider.Value);
    }

    private void saveButton_Click(object sender, EventArgs e)
    {
      lock (_lastFrame)
        _lastFrame.Save(string.Format("{0}.png", fileName.Text), ImageFormat.Png);
    }

    private void FrameRecorderForm_Load(object sender, EventArgs e)
    {
      _board = CameraMasterBoard.Attach();
      _board.ResetCameras();
      _board.AutomaticBrightnessControl = false;
      _board.StartProcessingData();

      var imgWindow = new ImageWindow("Frame");
      imgWindow.Show();
      _board.FrameReceived += (frame) => imgWindow.ProcessFrame(FrameGridAlign.Align(_board, frame, aspectRatio: 1.0));
      _board.FrameReceived += (frame) => _lastFrame = frame;

      _autoRecordTimer.Tick += OnAutoRecordTick;
    }

    private void fileName_TextChanged(object sender, EventArgs e)
    {
      saveButton.Enabled = !string.IsNullOrEmpty(fileName.Text);
    }

    private void resetButton_Click(object sender, EventArgs e)
    {
      _board.ResetCameras();
    }

    private void autoBrightness_CheckedChanged(object sender, EventArgs e)
    {
      _board.AutomaticBrightnessControl = autoBrightness.Checked;
      _board.ResetCameras();
    }

    private void autoRecordStart_Click(object sender, EventArgs e)
    {
      _autoRecordTimer.Interval = (int)Math.Round(1000f/int.Parse(fps.Text));
      _autoRecordFrame = 0;
      _autoRecordTimer.Start();
      autoRecordStart.Enabled = false;
      autoRecordStop.Enabled = true;
    }

    private void autoRecordStop_Click(object sender, EventArgs e)
    {
      _autoRecordTimer.Stop();
      autoRecordStart.Enabled = true;
      autoRecordStop.Enabled = false;
    }

    private void OnAutoRecordTick(object sender, EventArgs e)
    {
      var file = fileName.Text + _autoRecordFrame;
      currentFile.Text = file;
      _autoRecordFrame++;

      lock (_lastFrame)
        _lastFrame.Save(string.Format("{0}.png", file), ImageFormat.Png);
    }
  }
}
