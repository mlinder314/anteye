﻿namespace FrameRecorder
{
  partial class FrameRecorderForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.fileName = new System.Windows.Forms.TextBox();
      this.saveButton = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.exposureSlider = new System.Windows.Forms.TrackBar();
      this.gainSlider = new System.Windows.Forms.TrackBar();
      this.resetButton = new System.Windows.Forms.Button();
      this.autoBrightness = new System.Windows.Forms.CheckBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.label4 = new System.Windows.Forms.Label();
      this.currentFile = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.fps = new System.Windows.Forms.TextBox();
      this.autoRecordStart = new System.Windows.Forms.Button();
      this.autoRecordStop = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.exposureSlider)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.gainSlider)).BeginInit();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(49, 17);
      this.label1.TabIndex = 0;
      this.label1.Text = "Name:";
      // 
      // fileName
      // 
      this.fileName.Location = new System.Drawing.Point(67, 9);
      this.fileName.Name = "fileName";
      this.fileName.Size = new System.Drawing.Size(159, 22);
      this.fileName.TabIndex = 1;
      this.fileName.TextChanged += new System.EventHandler(this.fileName_TextChanged);
      // 
      // saveButton
      // 
      this.saveButton.Enabled = false;
      this.saveButton.Location = new System.Drawing.Point(233, 8);
      this.saveButton.Name = "saveButton";
      this.saveButton.Size = new System.Drawing.Size(75, 23);
      this.saveButton.TabIndex = 2;
      this.saveButton.Text = "Save";
      this.saveButton.UseVisualStyleBackColor = true;
      this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(12, 48);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(71, 17);
      this.label2.TabIndex = 3;
      this.label2.Text = "Exposure:";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(12, 96);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(42, 17);
      this.label3.TabIndex = 4;
      this.label3.Text = "Gain:";
      // 
      // exposureSlider
      // 
      this.exposureSlider.LargeChange = 25;
      this.exposureSlider.Location = new System.Drawing.Point(89, 48);
      this.exposureSlider.Maximum = 255;
      this.exposureSlider.Name = "exposureSlider";
      this.exposureSlider.Size = new System.Drawing.Size(219, 56);
      this.exposureSlider.SmallChange = 10;
      this.exposureSlider.TabIndex = 5;
      this.exposureSlider.TickFrequency = 10;
      this.exposureSlider.Value = 100;
      this.exposureSlider.Scroll += new System.EventHandler(this.exposureSlider_Scroll);
      // 
      // gainSlider
      // 
      this.gainSlider.LargeChange = 4;
      this.gainSlider.Location = new System.Drawing.Point(89, 96);
      this.gainSlider.Maximum = 31;
      this.gainSlider.Name = "gainSlider";
      this.gainSlider.Size = new System.Drawing.Size(219, 56);
      this.gainSlider.TabIndex = 6;
      this.gainSlider.TickFrequency = 2;
      this.gainSlider.Value = 31;
      this.gainSlider.Scroll += new System.EventHandler(this.gainSlider_Scroll);
      // 
      // resetButton
      // 
      this.resetButton.Location = new System.Drawing.Point(12, 139);
      this.resetButton.Name = "resetButton";
      this.resetButton.Size = new System.Drawing.Size(123, 23);
      this.resetButton.TabIndex = 7;
      this.resetButton.Text = "Reset";
      this.resetButton.UseVisualStyleBackColor = true;
      this.resetButton.Click += new System.EventHandler(this.resetButton_Click);
      // 
      // autoBrightness
      // 
      this.autoBrightness.AutoSize = true;
      this.autoBrightness.Location = new System.Drawing.Point(160, 141);
      this.autoBrightness.Name = "autoBrightness";
      this.autoBrightness.Size = new System.Drawing.Size(130, 21);
      this.autoBrightness.TabIndex = 8;
      this.autoBrightness.Text = "Auto Brightness";
      this.autoBrightness.UseVisualStyleBackColor = true;
      this.autoBrightness.CheckedChanged += new System.EventHandler(this.autoBrightness_CheckedChanged);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.autoRecordStop);
      this.groupBox1.Controls.Add(this.autoRecordStart);
      this.groupBox1.Controls.Add(this.fps);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.currentFile);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Location = new System.Drawing.Point(12, 169);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(296, 109);
      this.groupBox1.TabIndex = 9;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "AutoRecord";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(22, 27);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(89, 17);
      this.label4.TabIndex = 10;
      this.label4.Text = "Current File: ";
      // 
      // currentFile
      // 
      this.currentFile.AutoSize = true;
      this.currentFile.Location = new System.Drawing.Point(145, 27);
      this.currentFile.Name = "currentFile";
      this.currentFile.Size = new System.Drawing.Size(50, 17);
      this.currentFile.TabIndex = 11;
      this.currentFile.Text = "(none)";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(22, 54);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(38, 17);
      this.label5.TabIndex = 10;
      this.label5.Text = "FPS:";
      // 
      // fps
      // 
      this.fps.Location = new System.Drawing.Point(138, 51);
      this.fps.Name = "fps";
      this.fps.Size = new System.Drawing.Size(57, 22);
      this.fps.TabIndex = 10;
      this.fps.Text = "20";
      this.fps.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // autoRecordStart
      // 
      this.autoRecordStart.Location = new System.Drawing.Point(25, 79);
      this.autoRecordStart.Name = "autoRecordStart";
      this.autoRecordStart.Size = new System.Drawing.Size(123, 23);
      this.autoRecordStart.TabIndex = 10;
      this.autoRecordStart.Text = "Start";
      this.autoRecordStart.UseVisualStyleBackColor = true;
      this.autoRecordStart.Click += new System.EventHandler(this.autoRecordStart_Click);
      // 
      // autoRecordStop
      // 
      this.autoRecordStop.Location = new System.Drawing.Point(154, 79);
      this.autoRecordStop.Name = "autoRecordStop";
      this.autoRecordStop.Size = new System.Drawing.Size(123, 23);
      this.autoRecordStop.TabIndex = 12;
      this.autoRecordStop.Text = "Stop";
      this.autoRecordStop.UseVisualStyleBackColor = true;
      this.autoRecordStop.Click += new System.EventHandler(this.autoRecordStop_Click);
      // 
      // FrameRecorderForm
      // 
      this.AcceptButton = this.saveButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(320, 290);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.autoBrightness);
      this.Controls.Add(this.resetButton);
      this.Controls.Add(this.gainSlider);
      this.Controls.Add(this.exposureSlider);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.saveButton);
      this.Controls.Add(this.fileName);
      this.Controls.Add(this.label1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
      this.Name = "FrameRecorderForm";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "FrameRecorder";
      this.TopMost = true;
      this.Load += new System.EventHandler(this.FrameRecorderForm_Load);
      ((System.ComponentModel.ISupportInitialize)(this.exposureSlider)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.gainSlider)).EndInit();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox fileName;
    private System.Windows.Forms.Button saveButton;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TrackBar exposureSlider;
    private System.Windows.Forms.TrackBar gainSlider;
    private System.Windows.Forms.Button resetButton;
    private System.Windows.Forms.CheckBox autoBrightness;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button autoRecordStop;
    private System.Windows.Forms.Button autoRecordStart;
    private System.Windows.Forms.TextBox fps;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label currentFile;
    private System.Windows.Forms.Label label4;
  }
}

