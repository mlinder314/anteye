﻿using System;
using System.Threading;
using System.Windows.Forms;
using AntEye.Hardware;
using AntEye.Processing;
using AntEye.Visualization;

namespace AntEyeTest
{
  class Program
  {
    static void Main(string[] args)
    {
      while (!CameraMasterBoard.IsPresent())
      {
        Console.WriteLine("Waiting for camera board to attach...");
        Thread.Sleep(500);
      }

      Console.WriteLine("Attaching to board...");
      var board = CameraMasterBoard.Attach();
      board.ResetCameras();
      board.StartProcessingData();
      Console.WriteLine("All ready!");

      // Create a simple render window
      var imgWindow = new ImageWindow("Input", zoom: false);
      board.FrameReceived += imgWindow.ProcessFrame;
      imgWindow.Show();

      // Select a single camera ..
      var imgWindow2 = new ImageWindow("[E]");
      new FrameSplitter(board, 'E').SplitFrameReceived += (cam, img) => imgWindow2.ProcessFrame(img);
      imgWindow2.Show();

      // Show a camera grid
      var imgWindow3 = new ImageWindow("Grid");
      board.FrameReceived +=
        frame => imgWindow3.ProcessFrame(FrameGridAlign.Align(board, FrameLabel.Label(board, frame)));
      imgWindow3.Show();

      Application.Run();
    }
  }
}
