﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AntEye;
using AntEye.Utils;

namespace EyeCalibration
{
  public partial class CalibrationControlForm : Form
  {
    public CalibrationControlForm()
    {
      InitializeComponent();
      StartPosition = FormStartPosition.Manual;
      Location = new Point(0, 300);
    }

    private void buttonLoadSamples_Click(object sender, EventArgs e)
    {
      Program.DetectionPatternStore.LoadSamplesFile("samples.json");
      //MessageBox.Show("samples loaded");
    }

    private void buttonSaveSamples_Click(object sender, EventArgs e)
    {
      Program.DetectionPatternStore.SaveSamplesFile("samples.json");
      MessageBox.Show("samples saved");
    }

    private void CalibrationControlForm_FormClosed(object sender, FormClosedEventArgs e)
    {
      Application.Exit();
    }

    private void buttonCalibrate_Click(object sender, EventArgs e)
    {
      Program.Calibrate();
    }

    private void CalibrationControlForm_Load(object sender, EventArgs e)
    {
      // Start the auto-calibration timer
      var t = new Timer {Interval = 100};
      t.Tick += (o, args) =>
      {
        if (continousCalibration.Checked)
          buttonCalibrate_Click(sender, e);
      };
      t.Start();
    }

    private void buttonTestMode_Click(object sender, EventArgs e)
    {
      Program.TestCamera = 'N';
      Program.Detector.ShowDebugImages = true;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      File.WriteAllText("anteye2.json", Json.Default.ToJson(Config.Instance));
      MessageBox.Show("config saved to anteye2.json");
    }
  }
}
