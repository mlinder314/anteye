﻿namespace EyeCalibration
{
  partial class CalibrationControlForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.buttonLoadSamples = new System.Windows.Forms.Button();
      this.buttonSaveSamples = new System.Windows.Forms.Button();
      this.buttonCalibrate = new System.Windows.Forms.Button();
      this.continousCalibration = new System.Windows.Forms.CheckBox();
      this.buttonTestMode = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // buttonLoadSamples
      // 
      this.buttonLoadSamples.Location = new System.Drawing.Point(12, 12);
      this.buttonLoadSamples.Name = "buttonLoadSamples";
      this.buttonLoadSamples.Size = new System.Drawing.Size(127, 28);
      this.buttonLoadSamples.TabIndex = 0;
      this.buttonLoadSamples.Text = "Load Samples";
      this.buttonLoadSamples.UseVisualStyleBackColor = true;
      this.buttonLoadSamples.Click += new System.EventHandler(this.buttonLoadSamples_Click);
      // 
      // buttonSaveSamples
      // 
      this.buttonSaveSamples.Location = new System.Drawing.Point(12, 45);
      this.buttonSaveSamples.Name = "buttonSaveSamples";
      this.buttonSaveSamples.Size = new System.Drawing.Size(127, 28);
      this.buttonSaveSamples.TabIndex = 1;
      this.buttonSaveSamples.Text = "Save Samples";
      this.buttonSaveSamples.UseVisualStyleBackColor = true;
      this.buttonSaveSamples.Click += new System.EventHandler(this.buttonSaveSamples_Click);
      // 
      // buttonCalibrate
      // 
      this.buttonCalibrate.Location = new System.Drawing.Point(174, 12);
      this.buttonCalibrate.Name = "buttonCalibrate";
      this.buttonCalibrate.Size = new System.Drawing.Size(127, 28);
      this.buttonCalibrate.TabIndex = 2;
      this.buttonCalibrate.Text = "Calibrate";
      this.buttonCalibrate.UseVisualStyleBackColor = true;
      this.buttonCalibrate.Click += new System.EventHandler(this.buttonCalibrate_Click);
      // 
      // continousCalibration
      // 
      this.continousCalibration.AutoSize = true;
      this.continousCalibration.Location = new System.Drawing.Point(174, 50);
      this.continousCalibration.Name = "continousCalibration";
      this.continousCalibration.Size = new System.Drawing.Size(129, 21);
      this.continousCalibration.TabIndex = 3;
      this.continousCalibration.Text = "Cal. continously";
      this.continousCalibration.UseVisualStyleBackColor = true;
      // 
      // buttonTestMode
      // 
      this.buttonTestMode.Location = new System.Drawing.Point(12, 79);
      this.buttonTestMode.Name = "buttonTestMode";
      this.buttonTestMode.Size = new System.Drawing.Size(289, 28);
      this.buttonTestMode.TabIndex = 4;
      this.buttonTestMode.Text = "Test Mode";
      this.buttonTestMode.UseVisualStyleBackColor = true;
      this.buttonTestMode.Click += new System.EventHandler(this.buttonTestMode_Click);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(12, 113);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(289, 28);
      this.button1.TabIndex = 5;
      this.button1.Text = "Save Config";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // CalibrationControlForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(337, 150);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.buttonTestMode);
      this.Controls.Add(this.continousCalibration);
      this.Controls.Add(this.buttonCalibrate);
      this.Controls.Add(this.buttonSaveSamples);
      this.Controls.Add(this.buttonLoadSamples);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = "CalibrationControlForm";
      this.Text = "AntEye Calibration";
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CalibrationControlForm_FormClosed);
      this.Load += new System.EventHandler(this.CalibrationControlForm_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button buttonLoadSamples;
    private System.Windows.Forms.Button buttonSaveSamples;
    private System.Windows.Forms.Button buttonCalibrate;
    private System.Windows.Forms.CheckBox continousCalibration;
    private System.Windows.Forms.Button buttonTestMode;
    private System.Windows.Forms.Button button1;
  }
}