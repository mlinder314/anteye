﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.Mime;
using System.Runtime.InteropServices;
using System.Runtime.Versioning;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Math.Geometry;
using AntEye;
using AntEye.Hardware;
using AntEye.Processing;
using AntEye.Visualization;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using EyeCalibration.Calibration;
using OpenTK.Input;

namespace EyeCalibration
{
  /// <summary>
  /// Tries to automatically calibrate the camera Horizontal, Vertical and Rotational alignment
  /// by using object recognition
  /// </summary>
  static class Program
  {
    /// <summary>
    /// Defines how many frames to drop before processing a frame
    /// (Useful for visualizing what is happening)
    /// </summary>
    private const int FRAME_SKIP = 1; //30;

    /// <summary>
    /// If this is set, we will only create frames for one camera and display the individual steps
    /// </summary>
    public static char? TestCamera = null; //'N';

    /// <summary>
    /// Current frame number
    /// </summary>
    public static int CurrentFrameNum = 0;

    /// <summary>
    /// Pattern detector used
    /// </summary>
    public static PatternDetector Detector;

    /// <summary>
    /// Detection store
    /// </summary>
    public static PatternStore DetectionPatternStore;

    /// <summary>
    /// Calibrator
    /// </summary>
    public static Calibrator Calibrator;

    [System.Runtime.InteropServices.DllImport("user32")]
    private static extern bool SetProcessDPIAware();
    
    static void Main(string[] args)
    {
      SetProcessDPIAware();

      // Load the config
      Config.Instance = Config.LoadConfig("anteye.json");

      // Initialize the board
      var board = CameraMasterBoard.Attach();
      board.AutomaticBrightnessControl = true;
      board.HighSpeedMode = false;
      board.StartProcessingData();
      board.ResetCameras();

      // Visualize the cameras in a grid
      var imgWindow = new ImageWindow("Cameras", size: CvUtil.VisualizationSize);
      board.FrameReceived += frame => imgWindow.ProcessFrame(FrameGridAlign.Align(board, FrameLabel.Label(board, frame), aspectRatio: 1.0));
      imgWindow.Show();

      // Process split frames further
      Detector = new PatternDetector {ShowDebugImages = TestCamera.HasValue};
      DetectionPatternStore = new PatternStore();
      Calibrator = new Calibrator();
      new FrameSplitter(board, TestCamera.HasValue ? new[] { TestCamera.Value} : new char[0]).SplitFrameReceived += OnCameraFrame;
      board.FrameReceived += OnMergedFrame;

      // Show our control form
      new CalibrationControlForm().Show();

      // Initialize the panorama view
      var panorama = new PanoramaView(width: 900, height: 400) { UseCurvedPlanes = true };
      panorama.Location = new Point(360, 300);
      panorama.KeyDown += (obj, eargs) =>
      {
        if (eargs.Key == Key.BackSpace)
          board.ResetCameras();
      };
      board.FrameReceived += frame => panorama.UpdateFrame(FrameLabel.Label(board, frame));

      // Wait for all forms to close
      panorama.Run();
    }

    #region OpenCV Visualization Helpers
 
#endregion

    private static void OnCameraFrame(char camera, Bitmap frame)
    {
      // Skip some frames (potentially)
      if (CurrentFrameNum%FRAME_SKIP != 0)
        return;
      if (TestCamera != null && camera != TestCamera)
        return;
      
      // Find our pattern
      var sample = Detector.Detect(CurrentFrameNum, camera, frame);
      if (sample != null)
      {
        DetectionPatternStore.StoreSample(sample);
      }
    }

    private static void OnMergedFrame(Bitmap obj)
    {
      DetectionPatternStore.CreateJointSamples(CurrentFrameNum);
      DetectionPatternStore.VisualizeStats();
      CurrentFrameNum++;
    }

    /// <summary>
    /// Runs the calibration algorithm
    /// </summary>
    public static void Calibrate()
    {
      Calibrator.Run(DetectionPatternStore);
    }
  }
}
