﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeCalibration
{
  /// <summary>
  /// Extension methods
  /// </summary>
  internal static class Extensions
  {
    /// <summary>
    /// Draws a centered string
    /// </summary>
    public static void DrawCenteredString(this Graphics g, string text, Font font, Brush brush, float x, float y)
    {
      var measure = g.MeasureString(text, font);
      g.DrawString(text, font, brush, x - measure.Width/2f, y - measure.Height/2f);
    }

    /// <summary>
    /// Converts a RectangleF to a rounded Rectangle
    /// </summary>
    /// <param name="rectf"></param>
    /// <returns></returns>
    public static Rectangle ToRect(this RectangleF rectf)
    {
      return new Rectangle((int) Math.Round(rectf.X),
        (int) Math.Round(rectf.Y),
        (int) Math.Round(rectf.Width),
        (int) Math.Round(rectf.Height));
    }
  }
}
