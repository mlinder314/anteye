﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Emgu.CV.Structure;

namespace EyeCalibration.Calibration
{
  /// <summary>
  /// A saved detection result from our line detection
  /// </summary>
  public class DetectionSample
  {
    /// <summary>
    /// Camera that captured this sample
    /// </summary>
    public char Camera { get; set; }

    /// <summary>
    /// Frame Num
    /// </summary>
    public int FrameNum { get; set; }

    /// <summary>
    /// First line pair
    /// </summary>
    public LineSegment2D[] PairA { get; set; }

    /// <summary>
    /// Second line pair
    /// </summary>
    public LineSegment2D[] PairB { get; set; }

    /// <summary>
    /// Outer distance (between pairs)
    /// </summary>
    public double OuterDistance { get; set; }

    /// <summary>
    /// Inner distance (within a pair)
    /// </summary>
    public double InnerDistance { get; set; }

    /// <summary>
    /// Average length of a line
    /// </summary>
    public double AverageLineLength { get; set; }
  }
}
