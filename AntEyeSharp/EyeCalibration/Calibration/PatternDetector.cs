﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AForge.Math.Geometry;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;

namespace EyeCalibration.Calibration
{
  /// <summary>
  /// Calibration pattern detector. Tries to find the two lines "="-pattern in the camera image, and 
  /// analyses it.
  /// </summary>
  public class PatternDetector
  {
    private MCvFont DebugFont = new MCvFont(FONT.CV_FONT_HERSHEY_PLAIN, 0.7, 0.7);

    /// <summary>
    /// True if debug/step images should be shown
    /// </summary>
    public bool ShowDebugImages { get; set; }

    /// <summary>
    /// Runs the pattern detector on the given frame, and returns the detection sample (if found),
    /// or null otherwise
    /// </summary>
    public DetectionSample Detect(int frameNum, char camera, Bitmap frame)
    {
      // Smooth image
      var img = new Image<Rgb, byte>(frame);
      if (ShowDebugImages)
        CvUtil.ShowImage("Raw Image", img);

      // Convert the image to grayscale and filter out the noise
      Image<Gray, Byte> gray = img.Convert<Gray, Byte>();
      gray._SmoothGaussian(3);
      if (ShowDebugImages)
        CvUtil.ShowImage("Blurred Gray", gray);

      // Use Canny to find edges
      var cannyThreshold = new Gray(200);
      var cannyThresholdLinking = new Gray(150);

      Image<Gray, Byte> cannyEdges = gray.Canny(cannyThreshold, cannyThresholdLinking);
      if (ShowDebugImages)
        CvUtil.ShowImage("Canny", cannyEdges);

      // Visualization Image: update img
      img = gray.Convert<Rgb, Byte>();

      // Find continous line segments
      LineSegment2D[] lines = cannyEdges.HoughLinesBinary(
          1, //Distance resolution in pixel-related units
          Math.PI / 180, //Angle resolution measured in radians.
          12, //threshold
          7, //min Line width
          2 //gap between lines
          )[0]; //Get the lines from the first channel

      // Draw found lines
      if (ShowDebugImages)
      {
        var lineImage = img.Copy();
        foreach (LineSegment2D line in lines)
          lineImage.Draw(line, new Rgb(Color.Yellow), 1);
        lineImage.Draw(string.Format("{0}", lines.Length), ref DebugFont, new Point(12, 12), new Rgb(Color.Red));
        CvUtil.ShowImage("Lines", lineImage);
      }

      // Reject lines that do not meet these criteria:
      // * line does not exit through one end of the window
      // * line does not stop close to the middle of the screen
      var screenRect = new Rectangle(Point.Empty, img.Size);
      screenRect.Inflate(-screenRect.Width / 10, -screenRect.Height / 10);
      var acceptedLines = lines
        .Where(line => screenRect.Contains(line.P1) != screenRect.Contains(line.P2))
        .ToList();

      if (ShowDebugImages)
      {
        var acceptedLineImage = img.Copy();
        foreach (LineSegment2D line in acceptedLines)
          acceptedLineImage.Draw(line, new Rgb(Color.Yellow), 1);
        acceptedLineImage.Draw(string.Format("{0}", acceptedLines.Count), ref DebugFont, new Point(12, 12),
          new Rgb(Color.Red));
        acceptedLineImage.Draw(screenRect, new Rgb(Color.Lime), 1);
        CvUtil.ShowImage("Bordering Lines", acceptedLineImage);
      }

      // Remove duplicates of the same line; only keep longest pair
      const int minLineDist = 2;
      var toRemove = new List<LineSegment2D>();
      for (int i = 0; i < acceptedLines.Count - 1; i++)
      {
        for (int j = i + 1; j < acceptedLines.Count; j++)
        {
          var lineA = acceptedLines[i];
          var lineB = acceptedLines[j];
          if (IsParallelish(lineA, lineB) && OrthogonalDistance(lineA, lineB) < minLineDist)
            toRemove.Add(lineA.Length < lineB.Length ? lineA : lineB);
        }
      }
      foreach (var elem in toRemove)
        acceptedLines.Remove(elem);

      if (ShowDebugImages)
      {
        var dedupedLineImage = img.Copy();
        foreach (LineSegment2D line in acceptedLines)
          dedupedLineImage.Draw(line, new Rgb(Color.Yellow), 1);
        dedupedLineImage.Draw(string.Format("{0}", acceptedLines.Count), ref DebugFont, new Point(12, 12),
          new Rgb(Color.Red));
        CvUtil.ShowImage("Deduped Lines", dedupedLineImage);
      }

      // Build line pairs that have parallel lines (thus forming one "real" line box)
      const int LINE_LENGTH_THETA = 4; // maximum allowed difference in line length
      var parallelPairs = (from line1 in acceptedLines
                           from line2 in acceptedLines
                           let dist = OrthogonalDistance(line1, line2)
                           where
                             line1.GetHashCode() < line2.GetHashCode() && // prevent a<->b b<->a swap
                             IsParallelish(line1, line2) && // make sure they're parallel
                             Math.Abs(line1.Length - line2.Length) < LINE_LENGTH_THETA
                           select new { LineA = line1, LineB = line2, Distance = dist }).ToArray();

      var colors = new[] { Color.Red, Color.Yellow, Color.Orange, Color.Lime, Color.Blue, Color.DeepPink };
      int colorIndex = 0;
      if (ShowDebugImages)
      {
        var linePairImage = img.Copy();
        foreach (var pair in parallelPairs)
        {
          var color = colors[colorIndex++%colors.Length];
          linePairImage.Draw(pair.LineA, new Rgb(color), 1);
          linePairImage.Draw(pair.LineB, new Rgb(color), 1);
        }
        linePairImage.Draw(string.Format("{0}", parallelPairs.Length), ref DebugFont, new Point(12, 12),
          new Rgb(Color.Red));
        CvUtil.ShowImage("Parallel Pairs", linePairImage);
      }
      // Also remove lines that do not have a black pixel in the center
      const int BLACK_THETA = 80;
      var blackPixelPairs = (from pair in parallelPairs
        let centerX = (int) new[] {pair.LineA.P1, pair.LineA.P2, pair.LineB.P1, pair.LineB.P2}.Average(p => p.X)
        let centerY = (int) new[] {pair.LineA.P1, pair.LineA.P2, pair.LineB.P1, pair.LineB.P2}.Average(p => p.Y)
        where gray[centerY, centerX].Intensity < BLACK_THETA
        select pair).ToArray();

      colorIndex = 0;
      if (ShowDebugImages)
      {
        var blackPixelImg = img.Copy();
        foreach (var pair in blackPixelPairs)
        {
          var color = colors[colorIndex++ % colors.Length];
          blackPixelImg.Draw(pair.LineA, new Rgb(color), 1);
          blackPixelImg.Draw(pair.LineB, new Rgb(color), 1);
        }
        blackPixelImg.Draw(string.Format("{0}", blackPixelPairs.Length), ref DebugFont, new Point(12, 12),
          new Rgb(Color.Red));
        CvUtil.ShowImage("Black Pixel Pairs", blackPixelImg);
      }

      // Find two line pairs for which the following is true:
      // ** minimum gap between both line pairs is two times the gap inbetween a line pair
      const int LINE_INNER_DIST_THETA = 7; // maximum allowed difference in inner pair line dist
      const int INNER_TO_OUTER_RATIO = 2; // ratio between inner dist and outer dist
      const double LINE_OUTER_DIST_THETA = 0.33; // maximum allowed difference in outer pair line dist
      var combinedLinePairs = (from linePairA in blackPixelPairs
                               from linePairB in blackPixelPairs
                               let outerDist = new[]
        {
          OrthogonalDistance(linePairA.LineA, linePairB.LineA),
          OrthogonalDistance(linePairA.LineA, linePairB.LineB),
          OrthogonalDistance(linePairA.LineB, linePairB.LineA),
          OrthogonalDistance(linePairA.LineB, linePairB.LineB),
        }.Min()
                               let averageInnerDistance = new[]
        {
          linePairA.Distance, linePairB.Distance
        }.Average()
                               where
                                 linePairA.GetHashCode() < linePairB.GetHashCode() && /* not same pair */
                                 IsParallelish(linePairA.LineA, linePairB.LineA) && /* kind of parallel */
                                 // Check that both inner distances are similar
                                 Math.Abs(linePairA.Distance - linePairB.Distance) < LINE_INNER_DIST_THETA &&
                                 // Check that the outer:inner ratio is correct
                                 Math.Abs((outerDist / averageInnerDistance) - INNER_TO_OUTER_RATIO) < LINE_OUTER_DIST_THETA
                               select new DetectionSample
                               {
                                 Camera = camera,
                                 FrameNum = frameNum,
                                 PairA = new[] { linePairA.LineA, linePairA.LineB },
                                 PairB = new[] { linePairB.LineA, linePairB.LineB },
                                 OuterDistance = outerDist,
                                 InnerDistance = averageInnerDistance,
                                 AverageLineLength = new[]
          {
            linePairA.LineA.Length,
            linePairA.LineB.Length,
            linePairB.LineA.Length,
            linePairB.LineB.Length
          }.Average()
                               }).ToArray();

      // Visualize the found line pairs
      if (ShowDebugImages)
      {
        var combinedPairsImage = img.Copy();
        colorIndex = 0;
        foreach (var pair in combinedLinePairs.SelectMany(x => new[] {x.PairA, x.PairB}))
        {
          var color = colors[colorIndex++%colors.Length];
          foreach (var line in pair)
            combinedPairsImage.Draw(line, new Rgb(color), 1);
        }
        combinedPairsImage.Draw(string.Format("{0}", combinedLinePairs.Length), ref DebugFont, new Point(12, 12),
          new Rgb(Color.Red));
        CvUtil.ShowImage("Pairs of Pairs", combinedPairsImage);
      }

      // Return the first best result (or null)
      return combinedLinePairs.FirstOrDefault();
    }

    #region Geometry Helpers
    /// <summary>
    /// Returns true if the two line segments are .. kind of parallel
    /// </summary>
    private static bool IsParallelish(LineSegment2D a, LineSegment2D b)
    {
      var angle = a.GetExteriorAngleDegree(b);
      return angle < 10;
    }

    /// <summary>
    /// Calculates the orthogonal distance between two line segments
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    private static double OrthogonalDistance(LineSegment2D a, LineSegment2D b)
    {
      // Calculate distance: point <-> line
      var line1 = AForge(a);

      // Take the average of end and start point in case the line is not exactly parallel
      return new[]
      {
        line1.DistanceToPoint(AForge(b.P1)),
        line1.DistanceToPoint(AForge(b.P2))
      }.Average();
    }

    /// <summary>
    /// Converts a Drawing point to an APoint point
    /// </summary>
    private static AForge.Point AForge(System.Drawing.Point pt)
    {
      return new AForge.Point(pt.X, pt.Y);
    }

    /// <summary>
    /// Converts an OpenCV linesegment to an AForge line segment
    /// </summary>
    /// <param name="segment"></param>
    /// <returns></returns>
    private static AForge.Math.Geometry.Line AForge(LineSegment2D segment)
    {
      return Line.FromPoints(AForge(segment.P1), AForge(segment.P2));
    }
    #endregion
  }
}
