﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using AntEye;
using AntEye.Utils;

namespace EyeCalibration.Calibration
{
  /// <summary>
  /// Data store for storing detection results
  /// </summary>
  public class PatternStore
  {
    /// <summary>
    /// Maximum number of samples to store per camera
    /// </summary>
    public const int MAX_SAMPLES = 999;

    /// <summary>
    /// Look at the last X frames when combining samples
    /// </summary>
    public const int JOINT_SAMPLE_TIME_THETA = 10;

    private readonly Dictionary<char, List<DetectionSample>> PerCameraSamples = new Dictionary<char, List<DetectionSample>>();
    private readonly Dictionary<string, List<JointSample>> JointSamples = new Dictionary<string, List<JointSample>>();
    
    public PatternStore()
    {
      // Add all cameras to the sample list
      foreach (var cam in Config.Instance.Cameras)
        PerCameraSamples.Add(cam.ID, new List<DetectionSample>());
    }

    /// <summary>
    /// Stores the sample in the data store
    /// </summary>
    /// <param name="sample"></param>
    public void StoreSample(DetectionSample sample)
    {
      if (sample == null || !PerCameraSamples.ContainsKey(sample.Camera))
        return;
      if (PerCameraSamples[sample.Camera].Count >= MAX_SAMPLES)
        PerCameraSamples[sample.Camera].RemoveAt(0);
      PerCameraSamples[sample.Camera].Add(sample);
    }

    /// <summary>
    /// Stores a joined detection sample between two cameras
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    public void StoreJointSample(DetectionSample a, DetectionSample b)
    {
      if (a.Camera < b.Camera)
      {
        // Swap a and b
        var swp = b;
        b = a;
        a = swp;
      }

      // Store the joint sample
      string key = "" + a.Camera + b.Camera;
      if (!JointSamples.ContainsKey(key))
        JointSamples.Add(key, new List<JointSample>());
      if (JointSamples[key].Count >= MAX_SAMPLES)
        return;
      JointSamples[key].Add(new JointSample(a, b));
    }

    /// <summary>
    /// Shows an image that visualizes the pattern store content
    /// </summary>
    public void VisualizeStats()
    {
      // Draw single camera stats
      var img = new Bitmap(320, 240);
      using (var g = Graphics.FromImage(img))
      {
        // Clear the entire image
        g.Clear(Color.White);

        // Calculate cell size
        int rows = (int)Math.Ceiling(Math.Sqrt(Config.Instance.Cameras.Length));
        int cols = (int) Math.Ceiling((double)Config.Instance.Cameras.Length/rows);
        int cellWidth = img.Width/rows;
        int cellHeight = img.Height/cols;
        int i = 0;

        // Fonts
        var fontCamID = new Font(FontFamily.GenericSansSerif, cellWidth*0.16f, FontStyle.Bold);
        var fontStats = new Font(FontFamily.GenericSansSerif, cellWidth*0.14f, FontStyle.Regular);

        foreach (var cam in Config.Instance.Cameras.OrderBy(x=>x.ID))
        {
          // Calculate cell position
          int cellX = i%rows;
          int cellY = i/rows;
          i++;
          var cellRect = new RectangleF(cellX*cellWidth, cellY*cellHeight, cellWidth, cellHeight);
          cellRect.Inflate(-cellWidth*0.1f, -cellHeight*0.1f); // add some border

          // Draw the cell itself
          var stats = PerCameraSamples[cam.ID];
          g.SetClip(cellRect);
          g.TranslateTransform(cellRect.X, cellRect.Y); // [0,0] = top left of cell

          // Clear the background
          var color = Color.White;
          if (stats.Count > 50) color = Color.Lime;
          else if (stats.Count > 30) color = Color.Yellow;
          else if (stats.Count > 10) color = Color.Orange;
          else if (stats.Count > 0) color = Color.Red;
          g.Clear(color);

          // Draw cell data
          g.DrawCenteredString(cam.ID.ToString(), fontCamID, Brushes.Black, cellRect.Width * 0.3f, cellRect.Height * 0.5f);
          g.DrawCenteredString(stats.Count.ToString(), fontStats, Brushes.Black, cellRect.Width*0.7f, cellRect.Height *0.5f);

          g.ResetTransform();
          g.ResetClip();

          // Draw cell border
          g.DrawRectangle(Pens.Black, cellRect.ToRect());
        }
      }
      CvUtil.ShowImage("Detected Samples", img);

      // Draw pair stats
      var pairStats = new Bitmap(640, 480);
      int pairs = JointSamples.Count;
      int maxRows = 12;
      float rowHeight = pairStats.Height/maxRows;
      using (var g = Graphics.FromImage(pairStats))
      {
        g.Clear(Color.White);
        var fontRow = new Font(FontFamily.GenericSansSerif, rowHeight * 0.33f, FontStyle.Regular);
        int i = 0;
        foreach (var row in JointSamples.Keys.OrderBy(str => str))
        {
          int samples = JointSamples[row].Count;
          
          // Draw a separating line
          g.DrawLine(Pens.Gray, 0, 0, pairStats.Width, 0);

          // Draw our text
          g.DrawString(string.Format("Pair [{0}]: {1}", row, samples), fontRow, Brushes.Black, rowHeight, rowHeight*0.2f);

          // Draw a status indicator
          var brush = Brushes.Gray;
          if (samples > 30) brush = Brushes.Lime;
          else if (samples > 15) brush = Brushes.Yellow;
          else if (samples > 5) brush = Brushes.Orange;
          else brush = Brushes.Red;
          g.FillEllipse(brush, rowHeight*0.2f, rowHeight*0.2f, rowHeight*0.6f, rowHeight*0.6f);

          // Advance to next row
          g.TranslateTransform(0, rowHeight);
          if (i++ >= maxRows)
          {
            g.TranslateTransform(pairStats.Width/3f, -pairStats.Height);
            i = 0;
          }
        }
      }
      CvUtil.ShowImage("Joint Samples", pairStats);
    }

    /// <summary>
    /// Returns the last JOINT_SAMPLE_TIME_THETA samples for each camera
    /// </summary>
    /// <param name="currentFrameNum"></param>
    /// <returns></returns>
    private IEnumerable<DetectionSample> GetLastSamples(int currentFrameNum)
    {
      foreach (var cam in PerCameraSamples.Values)
      {
        int n = Math.Min(cam.Count, JOINT_SAMPLE_TIME_THETA);
        for (int i = cam.Count - 1; i > cam.Count - n; i--)
        {
          var sample = cam[i];
          if (sample.FrameNum < currentFrameNum - JOINT_SAMPLE_TIME_THETA)
            break;
          yield return sample;
        }
      }
    }

    /// <summary>
    /// Finds all samples that have been captured at the same time, and creates a joint sample
    /// </summary>
    public void CreateJointSamples(int currentFrameNum)
    {
      // Find out how many pairs there are
      var pairs = GetLastSamples(currentFrameNum)
        .GroupBy(d => d.Camera)
        .ToArray();

      // pattern can only be seen in two cams at any moment; rest is noise
      if (pairs.Length != 2)
        return;

      var camA = pairs[0].First();
      var camB = pairs[1].First();

      // Ignore cameras who haven't received any new samples
      if (camA.FrameNum != currentFrameNum && camB.FrameNum != currentFrameNum)
        return; 

      StoreJointSample(camA, camB);
    }

    /// <summary>
    /// Saves all samples to the specified file
    /// </summary>
    public void SaveSamplesFile(string file)
    {
      File.WriteAllText(file, Json.Default.ToJson(PerCameraSamples.Values.SelectMany(x => x).OrderBy(x=>x.FrameNum).ToArray()));
    }

    /// <summary>
    /// Loads samples from the specified file
    /// </summary>
    /// <param name="file"></param>
    public void LoadSamplesFile(string file)
    {
      var samples = Json.Default.ParseJson<DetectionSample[]>(File.ReadAllText(file));
      
      // Add each sample one by one ..
      foreach (var cam in PerCameraSamples)
        cam.Value.Clear();
      JointSamples.Clear();
      int frameNum = 0;
      foreach (var sample in samples)
      {
        // TODO: Do this in a more efficient way (but do we really care?)
        StoreSample(sample);
        if (sample.FrameNum > frameNum)
        {
          CreateJointSamples(frameNum);
          frameNum = sample.FrameNum;
        }
      }
    }

    /// <summary>
    /// Returns all joint samples
    /// </summary>
    /// <returns></returns>
    public IEnumerable<string> GetJointSampleKeys()
    {
      return JointSamples.Keys;
    }

    /// <summary>
    /// Returns all joint samples for the specified key
    /// </summary>
    /// <param name="key"></param>
    /// <returns></returns>
    public IEnumerable<JointSample> GetJointSamples(string key)
    {
      return JointSamples[key];
    } 
  }
}
