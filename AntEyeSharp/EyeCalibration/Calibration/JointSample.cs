﻿namespace EyeCalibration.Calibration
{
  /// <summary>
  /// A joint sample between two cameras
  /// </summary>
  public class JointSample
  {
    /// <summary>
    /// Creates a joint sample
    /// </summary>
    public JointSample(DetectionSample a, DetectionSample b)
    {
      Samples = new[] {a, b};
    }

    /// <summary>
    /// Both detection samples
    /// </summary>
    public DetectionSample[] Samples { get; set; }

    /// <summary>
    /// First camera id
    /// </summary>
    public char CameraA
    {
      get { return Samples[0].Camera; }
    }

    /// <summary>
    /// Second camera id
    /// </summary>
    public char CameraB
    {
      get { return Samples[1].Camera; }
    }
  }
}
