﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using AntEye;
using AntEye.Processing;
using Emgu.CV.Structure;
using OpenTK;
using Point = AForge.Point;
using Vector4 = AForge.Math.Vector4;

namespace EyeCalibration.Calibration
{
  /// <summary>
  /// Determines calibration values based upon joint samples
  /// </summary>
  public class Calibrator
  {
    /// <summary>
    /// Minimum of joint samples for a joint to be considered
    /// </summary>
    private const int MIN_SAMPLES = 12;

    /// <summary>
    /// Camera Frame Size
    /// </summary>
    private static readonly RectangleF CameraSize = new RectangleF(0, 0, 40, 30);

    /// <summary>
    /// Per-camera Calibration settings (computed recursively)
    /// </summary>
    private class CalibrationOffset
    {
      /// <summary>
      /// Parent node
      /// </summary>
      public CalibrationOffset Parent;

      /// <summary>
      /// SourceCamera to which this offset belongs to
      /// </summary>
      public char SourceCamera;

      /// <summary>
      /// SourceCamera to which this offset belongs to
      /// </summary>
      public char DestinationCamera;

      /// <summary>
      /// Relative horizontal offset to the parent
      /// </summary>
      public double HorizontalOffset { get; set; }

      /// <summary>
      /// Relative vertical offset to the parent
      /// </summary>
      public double VerticalOffset { get; set; }

      /// <summary>
      /// Relative rotation offset to the parent
      /// </summary>
      public double RotationOffset { get; set; }

      public CalibrationOffset Inverted()
      {
        var node = new CalibrationOffset();
        node.SourceCamera = DestinationCamera;
        node.DestinationCamera = SourceCamera;
        node.RotationOffset = -RotationOffset;
        node.HorizontalOffset = -HorizontalOffset;
        node.VerticalOffset = -VerticalOffset;
        return node;
      }

      /// <summary>
      /// Total transitive horizontal offset
      /// </summary>
      public double TransitiveHorizontalOffset
      {
        get
        {
          var offset = HorizontalOffset;
          if (Parent != null) offset += Parent.TransitiveHorizontalOffset;
          return offset;
        }
      }

      /// <summary>
      /// Total transitive horizontal offset
      /// </summary>
      public double TransitiveVerticalOffset
      {
        get
        {
          var offset = VerticalOffset;
          if (Parent != null) offset += Parent.TransitiveVerticalOffset;
          return offset;
        }
      }

      /// <summary>
      /// Total transitive horizontal offset
      /// </summary>
      public double TransitiveRotationOffset
      {
        get
        {
          var offset = RotationOffset;
          if (Parent != null) offset += Parent.TransitiveRotationOffset;
          return offset;
        }
      }
    }

    /// <summary>
    /// Runs the calibration algorithm on the pattern store
    /// </summary>
    /// <param name="patterns"></param>
    public void Run(PatternStore patterns)
    {
      lock (patterns)
      {
        // Find pairs that are good/have enough samples
        var q = from key in patterns.GetJointSampleKeys()
          let samples = patterns.GetJointSamples(key)
          where samples.Count() >= MIN_SAMPLES
          select new {Key = key, Samples = samples};

        // Calculate calibration matrices
        var offsets = q.ToDictionary(camSamples => camSamples.Key,
          camSamples => AverageOffsets(camSamples.Samples.Select(CalculateCalibrationOffset).ToArray()));

        // Figure out a transitive calibration scheme
        var transitiveOffsets = CalculateTransitiveClosure(offsets, Config.Instance.CenterCamera);

        lock (Config.Instance)
        {
          // Clear all old calibration data
          foreach (var cam in Config.Instance.CameraCalibrationOffsets)
          {
            cam.Horizontal = 0;
            cam.Rotation = 0;
            cam.Vertical = 0;
          }

          // Apply the new calibration data
          foreach (var offset in transitiveOffsets)
          {
            // Find element
            var config = Config.Instance.CameraCalibrationOffsets.FirstOrDefault(x => x.ID == offset.SourceCamera);
            if (config == null)
            {
              // Add a new element
              config = new Config.CameraAlignment();
              config.ID = offset.SourceCamera;
              Config.Instance.CameraCalibrationOffsets =
                Config.Instance.CameraCalibrationOffsets.Concat(new[] {config}).ToArray();
            }

            // Update values
            config.Horizontal += offset.TransitiveHorizontalOffset;
            config.Vertical += offset.TransitiveVerticalOffset;
            config.Rotation += offset.TransitiveRotationOffset;
          }
        }
      }
    }

    private CalibrationOffset AverageOffsets(CalibrationOffset[] data)
    {
      var res = new CalibrationOffset();
      res.SourceCamera = data[0].SourceCamera;
      res.DestinationCamera = data[1].DestinationCamera;
      res.HorizontalOffset = data.Average(x => x.HorizontalOffset);
      res.RotationOffset = data.Average(x => x.RotationOffset);
      res.VerticalOffset = data.Average(x => x.VerticalOffset);
      return res;
    }

    private IEnumerable<CalibrationOffset> CalculateTransitiveClosure(Dictionary<string, CalibrationOffset> offsets, char centerCamera)
    {
      var resultOffsets = new List<CalibrationOffset>();
      var offsetList = offsets.Values.ToList();

      // Always align to the camera *closer* to the origin
      var openNodes = new List<char> { centerCamera };
      while (openNodes.Count > 0)
      {
        // Retrieve the topmost node
        var node = openNodes[0];
        openNodes.RemoveAt(0);

        // Find connections from this node to any other node
        var connections = offsetList.Where(offs => offs.SourceCamera == node || offs.DestinationCamera == node).ToArray();
        foreach (var c in connections)
          offsetList.Remove(c);

        // Add all connections
        foreach (var con in connections)
        {
          var usedCon = con;
          // Check if we need to invert this node
          if (con.DestinationCamera != node)
            usedCon = con.Inverted();

          // Add this node for further processing
          openNodes.Add(usedCon.SourceCamera);

          // Add this calibration
          resultOffsets.Add(usedCon);
        }

        // Check if we are done
        if (openNodes.Count == 0 && offsetList.Count > 0)
        {
          // Still items missing..add the node closest to the center
          var centerAlig = Config.Instance.Cameras.First(x => x.ID == centerCamera);
          var closestNode = offsetList
            .SelectMany(x => new[] {x.SourceCamera, x.DestinationCamera})
            .Distinct()
            .Select(x => Config.Instance.Cameras.First(c => c.ID == x))
            .OrderBy(
              cam =>
                new Vector2((float) (cam.Horizontal - centerAlig.Horizontal),
                  (float) (cam.Vertical - centerAlig.Vertical)).LengthSquared)
            .First();
          openNodes.Add(closestNode.ID);
        }
      }

      // Return unchanged for now..
      return resultOffsets.ToArray();
    }

    /// <summary>
    /// Calculates the calibation offset to make a continous/straight connection
    /// from camera A to camera B based upon the current camera configuration
    /// </summary>
    /// <param name="samples"></param>
    /// <returns></returns>
    private CalibrationOffset CalculateCalibrationOffset(JointSample sample)
    {
      var samples = new[] {sample};

      // Figure out the rotational offset by matching lines
      var rotationOffsets = samples
        .Select(joint => joint.Samples.Select(GetLineDirection).ToArray()) // Get line directions
        .Select(joint => (joint[0]+180) - (joint[1])) // offset/difference in direction
        .Select(off => off*4/5)
        .ToArray();
      
      // Figure out translational offset after applying our fixed rotation
      var translationalOffsets = samples
        .Zip(rotationOffsets,
          (zipA, zipB) => new {SampleA = zipA.Samples[0], SampleB = zipA.Samples[1], Rotation = zipB})
        .Select(zip => GetTranslationalOffset(zip.SampleA, zip.SampleB, zip.Rotation))
        .ToArray();

      var offset = new CalibrationOffset();
      offset.SourceCamera = samples[0].CameraA;
      offset.DestinationCamera = samples[0].CameraB;
      offset.RotationOffset = rotationOffsets.Average();
      offset.HorizontalOffset = translationalOffsets.Select(x => x.X).Average();
      offset.VerticalOffset = translationalOffsets.Select(x => x.Y).Average();
      return offset;
    }

    private double GetLineDirection(DetectionSample sample)
    {
      return new[] {sample.PairA, sample.PairB}
        .SelectMany(l => l) // Select all single lines
        .Select(DirectionFromCenter)
        .Select(l => Math.Atan2(l.X, l.Y)*180.0/Math.PI) // Get line directions in degrees
        .Average(); // Average direction of all lines
    }

    private static Point DirectionFromCenter(LineSegment2D line)
    {
      var pt1 = new Point(line.P1.X, line.P1.Y);
      var pt2 = new Point(line.P2.X, line.P2.Y);

      // Find out which point of the line is in the center
      Point center, outer;
      var camCenter = new Point(CameraSize.Width/2, CameraSize.Height/2);
      if (pt1.SquaredDistanceTo(camCenter) < pt2.SquaredDistanceTo(camCenter))
      {
        center = pt1;
        outer = pt2;
      }
      else
      {
        center = pt2;
        outer = pt1;
      }

      if (outer == center) 
        return new Point(1, 0);

    // Calculate the angle from the center
      var dir = outer - center;
      dir /= dir.EuclideanNorm();
      return dir;
    }

    private Point GetTranslationalOffset(DetectionSample a, DetectionSample b, double rotationOffset)
    {
      // (1) Figure out the orthogonal distance
      // First, find the point in image b at which the line ends
      var pointsB = new[] {b.PairA, b.PairB}.SelectMany(m => m.SelectMany(o => new[] {o.P1, o.P2}));

      var borderRect = RectangleF.Inflate(CameraSize, -4, -4);
      var borderPointsB = pointsB.Where(p => !borderRect.Contains(p)).ToArray();

      // Take the middle between both lines as a point
      var borderX = borderPointsB.Select(p => p.X).Average();
      var borderY = borderPointsB.Select(p => p.Y).Average();
      // TODO: Advance this until we hit the border?

      var matrixA = CameraSphere.GetTransformation(a.Camera, useCalibration: false, rotationOffset: rotationOffset);
      var matrixB = CameraSphere.GetTransformation(b.Camera, useCalibration: false, rotationOffset: rotationOffset);

      // Calculate the location of the point in 3d space
      var pointInWorld = (matrixB * new Vector4((float)borderX, (float)borderY, 0, 1)).ToVector3();

      // Estimate the centered line in sample A
      var linePoints = new[] {a.PairA, a.PairB}.SelectMany(m => m.SelectMany(o => new[] {o.P1, o.P2})).ToArray();
      var lineCenterX = linePoints.Average(pt => pt.X);
      var lineCenterY = linePoints.Average(pt => pt.Y);

      // Put the line into world space
      var lineCenterInWorld = (matrixA*new Vector4((float) lineCenterX, (float)lineCenterY, 0, 1)).ToVector3();
      var lineDirection = GetLineDirection(a) * Math.PI / 180;
      var lineEndInWorld = (matrixA*
                           (new Vector4((float) Math.Sin(lineDirection), (float) Math.Cos(lineDirection), 0, 0)*25f +
                            new Vector4((float) lineCenterX, (float) lineCenterY, 0, 1))).ToVector3();
      var lineDirectionInWorld = (lineEndInWorld - lineCenterInWorld);
      lineDirectionInWorld.Normalize();

      // Move line orthogonal to the direction vector
      // Do this by first calculating the orthogonal vector
      var lineToPoint = pointInWorld - lineCenterInWorld;
      var vectorProjection = lineDirectionInWorld * AForge.Math.Vector3.Dot(lineToPoint, lineDirectionInWorld);
      
      // Calculate absolute world positions (is & should be)
      var oldPosition = lineCenterInWorld + vectorProjection;
      var newPosition = pointInWorld;

      // Calculate how much xShift and yShift is between both positions. Use atan2
      // on the sphere to determine angles
      var deltaAngleX = (Math.Atan2(newPosition.X, newPosition.Z) -
                         Math.Atan2(oldPosition.X, oldPosition.Z))*180/Math.PI;
      var deltaAngleY = (Math.Atan2(newPosition.Y, newPosition.Z) -
                         Math.Atan2(oldPosition.Y, oldPosition.Z)) * 180 / Math.PI;

      // (2) Figure out the shift along the line axis by examining line lengths.
      // 2cm = gap width between both lines
      // get scale invariant line length
      var lineLengthA = a.AverageLineLength*2.0/a.OuterDistance;
      var lineLengthB = b.AverageLineLength*2.0/b.OuterDistance;

      const double expectedLength = 15.7; // in cm
      double missingLength = expectedLength - lineLengthA - lineLengthB;

      // Move the whole board in direction of the line for the missing length
      // TODO: Make sure these directions work for all dirs
      double missingLengthPx = missingLength*(a.OuterDistance + b.OuterDistance)/2/2.0;
      var directionalOffsetPx = new Vector4((float) Math.Sin(lineDirection), (float) Math.Cos(lineDirection), 0, 0)*
                              (float) missingLengthPx * -1;
      var newLineCenterInWorld = (matrixA*
                                  (directionalOffsetPx +
                                   new Vector4((float) lineCenterX, (float) lineCenterY, 0, 1))).ToVector3();

      var deltaAngleX2 = (Math.Atan2(newLineCenterInWorld.X, newLineCenterInWorld.Z) -
                         Math.Atan2(lineCenterInWorld.X, lineCenterInWorld.Z)) * 180 / Math.PI;
      var deltaAngleY2 = (Math.Atan2(newLineCenterInWorld.Y, newLineCenterInWorld.Z) -
                         Math.Atan2(lineCenterInWorld.Y, lineCenterInWorld.Z)) * 180 / Math.PI;

      return new Point((float)(deltaAngleX+deltaAngleX2), (float)(deltaAngleY+deltaAngleY2));
    }
  }
}
