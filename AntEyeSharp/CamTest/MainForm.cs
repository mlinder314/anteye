﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Management.Instrumentation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Timer = System.Timers.Timer;

namespace CamTest
{
  public partial class MainForm : Form
  {
    // Serial Port
    private SerialPort tty;

    // Frame structure, reading
    private bool readingFrameData = true;
    private bool highSpeedMode = false;

    // Frame buffer
    private readonly byte[] frameBuffer = new byte[655360*2];
    private int frameBufferLen = 0;
    private int frameNum = 0;
    private double fps;
    private Stopwatch frameTimer = new Stopwatch();
    private Stopwatch lastFrameTimer = new Stopwatch();

    // Other visualizations
    private PanoramaView panorama;

    public MainForm()
    {
      InitializeComponent();
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      // Find the AntEye serial port
      // We're looking for the DeviceID for the device with the PNPDeviceID:
      // 		PNPDeviceID	"USB\\VID_04D8&PID_000A\\MATTHIASLINDER.COM:ANT-EYE"	string
      const string matchID = "\\VID_04D8&PID_000A\\MATTHIASLINDER.COM:ANT-EYE";
      var searcher = new ManagementObjectSearcher(@"Select * From Win32_SerialPort");
      string portName = null;
      foreach (var device in searcher.Get())
      {
        if (((string)device.GetPropertyValue("PNPDeviceID")).Contains(matchID))
        {
          portName = (string) device.GetPropertyValue("DeviceID");
          break;
        }
      }

      // Fall back to last available serial port otherwise
      if (portName == null)
      {
        var ports = SerialPort.GetPortNames();
        while ((ports = SerialPort.GetPortNames()).Length == 0)
        {
          // No part available, restart..
          Console.WriteLine("No port found; waiting..");
          Thread.Sleep(300);
        }
        portName = ports.Last();
      }
      tty = new SerialPort(portName ?? SerialPort.GetPortNames().Last(), 500000, Parity.None, 8, StopBits.One);
      tty.DtrEnable = false;
      tty.RtsEnable = false;
      tty.Handshake = Handshake.None;
      tty.ReadBufferSize = 65536;
      tty.Open();
      tty.DiscardInBuffer();
      tty.DataReceived += OnData;
      frameTimer.Start();
      lastFrameTimer.Start();
      
      // Check for USB detach
      var timer = new Timer(300);
      timer.AutoReset = true;
      timer.Elapsed += (o, args) =>
      {
        if (lastFrameTimer.Elapsed.TotalSeconds > 0.5)
          Application.Restart();
      };
      timer.Start();
    }

    // Read with a sliding window
    private readonly byte[] buf = new byte[2048];
    private readonly static byte[] readWindow = new byte[3];
    private int bytesToSkip = 0;
    private Image lastRawFrame;

    private void OnData(object sender, SerialDataReceivedEventArgs e)
    {
      int read = tty.Read(buf, 0, Math.Min(buf.Length, tty.BytesToRead));
      lastFrameTimer.Restart();

      // Create a 3 byte sliding window over the buffer
      for (int i = 0; i < read; i++)
      {
        // Shift buffer by 1
        readWindow[0] = readWindow[1];
        readWindow[1] = readWindow[2];
        readWindow[2] = buf[i];

        if (bytesToSkip > 0)
        {
          bytesToSkip--;
          continue;
        }

        // Check for special buffer states
        if (readingFrameData && readWindow[0] == 'E' && readWindow[1] == 'O' && readWindow[2] == 'F') {
          Console.WriteLine("(FRAME {0})", frameBufferLen);
          OnFrame(frameBuffer, frameBufferLen);
          frameBufferLen = 0;
          readingFrameData = false;
          bytesToSkip = 2;
        } else if (!readingFrameData && readWindow[0] == 'B' && readWindow[1] == 'O' && readWindow[2] == 'F') {
          readingFrameData = true;
          bytesToSkip = 2;
        } else if (readingFrameData) {
          frameBuffer[frameBufferLen++] = readWindow[0];
        } else {
          Console.Write((char)readWindow[0]);
        }
      }
    }

    private static byte ClipColor(double val)
    {
      if (val < 0) return 0;
      if (val > 255) return 255;
      return (byte) val;
    }

    private static Color YCbCr422(byte y, byte cb, byte cr)
    {
      return Color.FromArgb(ClipColor(y + 1.40200*(cr - 128)),
        ClipColor(y - 0.34414*(cb - 128) - 0.71414*(cr - 128)),
        ClipColor(y + 1.772*(cb - 128)));
    }

    private static Color RGB565(byte rg, byte gb)
    {
      // Extract components
      int r5 = (rg >> 3);
      int g6 = ((rg & 7) << 3) | (gb >> 5);
      int b5 = (gb & 31);

      // Align MSBs
      //int r = (r5 << 3) | (r5 >> 2); // R
      //int g = (g6 << 2) | (g6 >> 4); // G
      //int b = (b5 << 3) | (b5 >> 2); // B

      // Use different encoding (feels slightly smoother)
      byte r = (byte)((r5 * 527 + 23) >> 6);
      byte g = (byte)((g6 * 259 + 33) >> 6);
      byte b = (byte)((b5 * 527 + 23) >> 6);

      // R and G seem weirdly swapped here. No idea why.

      return Color.FromArgb(g, r, b);
    }

    private Image PostProcess(Image img)
    {
      lock (img)
      {
        if (gridAlign.Checked && img.Height > 30*4)
        {
          // *** Grid Align Postprocessing
          const int borderY = 11;
          var dest = new Bitmap(40*4, 30*4 + borderY);
          using (Graphics g = Graphics.FromImage(dest))
          {
            g.Clear(Color.Black);
            var font = new Font(FontFamily.GenericSansSerif, 4f);
            for (int bus = 0; bus < 4; bus++)
            {
              var destRect = new Rectangle(bus*40, borderY, 40, 4*30);
              var srcRect = new Rectangle(0, (bus*4*30), 40, 4*30);
              g.DrawImage(img, destRect, srcRect, GraphicsUnit.Pixel);
              var str = string.Format("{0} - {1}", (char) ('A' + bus*4), (char) ('D' + bus*4));
              g.DrawString(str, font, Brushes.Yellow, bus*40 + 20 - g.MeasureString(str, font).Width/2, 2);
            }
          }
          return dest;
        }

        return img; // no postprocessing
      }
    }

    private void OnFrame(byte[] frame, int frameLen)
    {
      if (frameLen == 0)
        return; // no data

      frameNum++;
      var newFps = 1000.0 / frameTimer.Elapsed.TotalMilliseconds;
      frameTimer.Restart();
      fps = (fps * 2.0 + newFps * 1.0)/3.0;

      // Determine Resolution
      int x = 20;
      int y = 15;
      if (frameLen <= 150+10) /* add noise margin */
      {
        x /= 4;
      }
      else if (frameLen <= 300+10)
      {
        x /= 2;
      }
      else if (frameLen <= 600 + 20)
      {
        x = 20;
        y = 15;
      }
      else if (frameLen <= 1200 + 40)
      {
        x = 20;
        y = 30;
      }
      else if (frameLen <= 2400 + 40)
      {
        x = 40;
        y = 30;
      }
      else
      {
        x = 80;
        y = 60;
      }

      // OVERRIDE: Master -- auto determine
      x = 40;
      y = Math.Max(30 * (frameLen / 40 / 30 / 2), 30);
      if (highSpeedMode)
      {
        x /= 2;
        y *= 2;
      }

      var frameImage = new Bitmap(x, y, PixelFormat.Format32bppArgb);

      // Blit image
      lock (frameImage)
      {
        int usedFrameLen = frameLen;
        if (usedFrameLen > frameImage.Width*frameImage.Height*2)
          usedFrameLen = frameImage.Width*frameImage.Height*2;

        // Clear the image
        using (Graphics g = Graphics.FromImage(frameImage))
          g.Clear(Color.Magenta);

        // Update the image data
        var raw = frameImage.LockBits(new Rectangle(Point.Empty, frameImage.Size), ImageLockMode.WriteOnly,
          PixelFormat.Format32bppArgb);
        unsafe
        {
          var rawImage = (int*) raw.Scan0;
          for (int i = 0; i < usedFrameLen; i += 4)
          {
            int pixel = i/2;

            // Format is YCbCr422
            byte cb0 = frame[i + 0];
            byte y0 =  frame[i + 1];
            byte cr0 = frame[i + 2];
            byte y1 =  frame[i + 3];
            //rawImage[pixel] = YCbCr422(y0, cr0, cb0).ToArgb();
            //rawImage[pixel+1] = YCbCr422(y1, cr0, cb0).ToArgb();
            rawImage[pixel] = RGB565(cb0, y0).ToArgb();
            rawImage[pixel + 1] = RGB565(cr0, y1).ToArgb();
          }
        }
        frameImage.UnlockBits(raw);
      }
      try
      {
        lastRawFrame = frameImage;
        if (panorama != null)
          panorama.InputFrame = frameImage;
        var newImg = PostProcess(frameImage);
        Invoke(new Action(() =>
        {
          lock (newImg)
          {
            imgZoom.Image = newImg;
          }
          txtFrames.Text = frameNum.ToString();
          txtBytes.Text = frameLen.ToString();
          txtResolution.Text = string.Format("{0} x {1}", frameImage.Width, frameImage.Height);
          txtFPS.Text = string.Format("{0:F2} Hz", fps);
        }));
      }
      catch (Exception)
      {
        // meh.
      }
    }

    private void scaleImage_CheckedChanged(object sender, EventArgs e)
    {
      imgZoom.SizeMode = scaleImage.Checked ? PictureBoxSizeMode.Zoom : PictureBoxSizeMode.CenterImage;
    }

    private Image GetCamImage(char camID)
    {
      var frame = new Bitmap(lastRawFrame);
      var img = new Bitmap(40, 30);
      int cam = camID - 'A';
      lock (frame)
      {
        using (var g = Graphics.FromImage(img))
          g.DrawImage(frame, new Rectangle(0, 0, 40, 30), new Rectangle(0, cam*30, 40, 30), GraphicsUnit.Pixel);
      }
      return img;
    }

    private void exportButtonClick(object sender, EventArgs e)
    {
      var frame = lastRawFrame;
      lock (frame)
      {
        // Create dirs
        if (!Directory.Exists("export"))
          Directory.CreateDirectory("export");
        //string dir = Path.Combine("export", DateTime.UtcNow.Ticks.ToString());
        //Directory.CreateDirectory(dir);
        string dir = "export";

        // Export cams
        for (char cam = 'A'; cam < 'A' + 16; cam++)
        {
          string file = Path.Combine(dir, string.Format("cam{0}_{1}.png", cam, DateTime.UtcNow.Ticks));
          GetCamImage(cam).Save(file, ImageFormat.Png);
        }
      }
    }

    private void open3DView_Click(object sender, EventArgs e)
    {
      if (panorama != null && panorama.Exists)
        return;
      panorama = new PanoramaView();
      panorama.Run(50, 50);
    }

    private void MainForm_KeyDown(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Escape)
        Close();
    }

    private void testButton_Click(object sender, EventArgs e)
    {
      //var cmd = new byte[] { (byte)'C', (byte)'I', 0x71, 1 << 7, 0xFF };
      //tty.Write(cmd, 0, cmd.Length);

      var cmd = new byte[] { (byte)'C', (byte)'H'};
      tty.Write(cmd, 0, cmd.Length);
      highSpeedMode = true;

      //var cmd = new byte[] { (byte)'C', (byte)'R' };
      //tty.Write(cmd, 0, cmd.Length);
    }
  }
}
