﻿namespace CamTest
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.imgZoom = new System.Windows.Forms.PictureBox();
      this.label1 = new System.Windows.Forms.Label();
      this.txtFrames = new System.Windows.Forms.Label();
      this.txtBytes = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.txtResolution = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.txtFPS = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.gridAlign = new System.Windows.Forms.CheckBox();
      this.scaleImage = new System.Windows.Forms.CheckBox();
      this.exportButton = new System.Windows.Forms.Button();
      this.open3DView = new System.Windows.Forms.Button();
      this.testButton = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.imgZoom)).BeginInit();
      this.SuspendLayout();
      // 
      // imgZoom
      // 
      this.imgZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.imgZoom.BackColor = System.Drawing.Color.White;
      this.imgZoom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.imgZoom.Location = new System.Drawing.Point(116, 12);
      this.imgZoom.Name = "imgZoom";
      this.imgZoom.Size = new System.Drawing.Size(383, 294);
      this.imgZoom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.imgZoom.TabIndex = 0;
      this.imgZoom.TabStop = false;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(12, 16);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(62, 17);
      this.label1.TabIndex = 2;
      this.label1.Text = "Frame#";
      // 
      // txtFrames
      // 
      this.txtFrames.Location = new System.Drawing.Point(15, 33);
      this.txtFrames.Name = "txtFrames";
      this.txtFrames.Size = new System.Drawing.Size(87, 17);
      this.txtFrames.TabIndex = 3;
      this.txtFrames.Text = "0";
      this.txtFrames.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // txtBytes
      // 
      this.txtBytes.Location = new System.Drawing.Point(18, 71);
      this.txtBytes.Name = "txtBytes";
      this.txtBytes.Size = new System.Drawing.Size(84, 17);
      this.txtBytes.TabIndex = 5;
      this.txtBytes.Text = "0";
      this.txtBytes.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(12, 54);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(78, 17);
      this.label4.TabIndex = 4;
      this.label4.Text = "Bytes/Img";
      // 
      // txtResolution
      // 
      this.txtResolution.Location = new System.Drawing.Point(18, 107);
      this.txtResolution.Name = "txtResolution";
      this.txtResolution.Size = new System.Drawing.Size(84, 17);
      this.txtResolution.TabIndex = 7;
      this.txtResolution.Text = "0";
      this.txtResolution.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(12, 90);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(85, 17);
      this.label3.TabIndex = 6;
      this.label3.Text = "Resolution";
      // 
      // txtFPS
      // 
      this.txtFPS.Location = new System.Drawing.Point(18, 143);
      this.txtFPS.Name = "txtFPS";
      this.txtFPS.Size = new System.Drawing.Size(84, 17);
      this.txtFPS.TabIndex = 9;
      this.txtFPS.Text = "0";
      this.txtFPS.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.Location = new System.Drawing.Point(12, 126);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(37, 17);
      this.label6.TabIndex = 8;
      this.label6.Text = "FPS";
      // 
      // gridAlign
      // 
      this.gridAlign.AutoSize = true;
      this.gridAlign.Checked = true;
      this.gridAlign.CheckState = System.Windows.Forms.CheckState.Checked;
      this.gridAlign.Location = new System.Drawing.Point(4, 170);
      this.gridAlign.Name = "gridAlign";
      this.gridAlign.Size = new System.Drawing.Size(107, 21);
      this.gridAlign.TabIndex = 10;
      this.gridAlign.Text = "Align in Grid";
      this.gridAlign.UseVisualStyleBackColor = true;
      // 
      // scaleImage
      // 
      this.scaleImage.AutoSize = true;
      this.scaleImage.Checked = true;
      this.scaleImage.CheckState = System.Windows.Forms.CheckState.Checked;
      this.scaleImage.Location = new System.Drawing.Point(4, 197);
      this.scaleImage.Name = "scaleImage";
      this.scaleImage.Size = new System.Drawing.Size(65, 21);
      this.scaleImage.TabIndex = 11;
      this.scaleImage.Text = "Scale";
      this.scaleImage.UseVisualStyleBackColor = true;
      this.scaleImage.CheckedChanged += new System.EventHandler(this.scaleImage_CheckedChanged);
      // 
      // exportButton
      // 
      this.exportButton.Location = new System.Drawing.Point(4, 251);
      this.exportButton.Name = "exportButton";
      this.exportButton.Size = new System.Drawing.Size(107, 25);
      this.exportButton.TabIndex = 13;
      this.exportButton.Text = "Export";
      this.exportButton.UseVisualStyleBackColor = true;
      this.exportButton.Click += new System.EventHandler(this.exportButtonClick);
      // 
      // open3DView
      // 
      this.open3DView.Location = new System.Drawing.Point(4, 281);
      this.open3DView.Name = "open3DView";
      this.open3DView.Size = new System.Drawing.Size(107, 25);
      this.open3DView.TabIndex = 14;
      this.open3DView.Text = "3D View";
      this.open3DView.UseVisualStyleBackColor = true;
      this.open3DView.Click += new System.EventHandler(this.open3DView_Click);
      // 
      // testButton
      // 
      this.testButton.Location = new System.Drawing.Point(4, 224);
      this.testButton.Name = "testButton";
      this.testButton.Size = new System.Drawing.Size(107, 25);
      this.testButton.TabIndex = 15;
      this.testButton.Text = "TEST";
      this.testButton.UseVisualStyleBackColor = true;
      this.testButton.Click += new System.EventHandler(this.testButton_Click);
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(511, 318);
      this.Controls.Add(this.testButton);
      this.Controls.Add(this.open3DView);
      this.Controls.Add(this.exportButton);
      this.Controls.Add(this.scaleImage);
      this.Controls.Add(this.gridAlign);
      this.Controls.Add(this.txtFPS);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.txtResolution);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txtBytes);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.txtFrames);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.imgZoom);
      this.MinimumSize = new System.Drawing.Size(197, 288);
      this.Name = "MainForm";
      this.Text = "AntCam";
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
      ((System.ComponentModel.ISupportInitialize)(this.imgZoom)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox imgZoom;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label txtFrames;
    private System.Windows.Forms.Label txtBytes;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label txtResolution;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label txtFPS;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.CheckBox gridAlign;
    private System.Windows.Forms.CheckBox scaleImage;
    private System.Windows.Forms.Button exportButton;
    private System.Windows.Forms.Button open3DView;
    private System.Windows.Forms.Button testButton;
  }
}

