﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace CamTest
{
  class PanoramaView : GameWindow
  {
    public Bitmap InputFrame;

    private const float CAM_DIST = 55f;

    private class Cam
    {
      public char ID;
      public float Horizontal;
      public float Vertical;
      public float Rot;
    }

    private static Cam[] Cameras =
    {
      // Front horizontal line
      new Cam { ID = 'J', Horizontal = -90, Vertical = 0, Rot = 45},  // left
      new Cam { ID = 'L', Horizontal = -45, Vertical = 0}, 
      new Cam { ID = 'O', Horizontal = 0, Vertical = 0}, 
      new Cam { ID = 'M', Horizontal = 45, Vertical = 0}, 
      new Cam { ID = 'K', Horizontal = 90, Vertical = 0, Rot = -45}, 

      // Top horizontal line
      new Cam { ID = 'N', Horizontal = 0, Vertical = 45}, 

      // Bottom horizontal line
      new Cam { ID = 'G', Horizontal = -45, Vertical = -45}, 
      new Cam { ID = 'P', Horizontal = 0, Vertical = -45}, 
      new Cam { ID = 'I', Horizontal = 45, Vertical = -45}, 
    };

    public PanoramaView() : base(1200, 600, GraphicsMode.Default, "AntEye Panorama View", GameWindowFlags.Default)
    //public PanoramaView() : base(1600, 900, GraphicsMode.Default, "AntEye Panorama View", GameWindowFlags.Fullscreen)
    {
      
    }

    protected override void OnResize(EventArgs e)
    {
      base.OnResize(e);
      GL.Viewport(0, 0, Width, Height);
    }

    protected override void OnLoad(EventArgs e)
    {
      base.OnLoad(e);
      VSync = VSyncMode.Off;

      GL.ClearColor(Color.CornflowerBlue);
      GL.CullFace(CullFaceMode.Front);
      GL.Enable(EnableCap.Texture2D);
      GL.Enable(EnableCap.DepthTest);
    }

    protected override void OnRenderFrame(FrameEventArgs e)
    {
      base.OnRenderFrame(e);

      // Clear all
      GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

      // Setup projection
      const float near = 1f;
      const float far = CAM_DIST + 10f;
      GL.MatrixMode(MatrixMode.Projection);
      float aspect = Width / (float)(Height);
      const float fov = (float) Math.PI/2f; /* 90 degrees */
      Matrix4 projection;
      Matrix4.CreatePerspectiveFieldOfView(fov, aspect, near, far, out projection);
      GL.LoadMatrix(ref projection);

      // Setup camera
      var center = Vector3.Zero;
      var xrot = (float)((Mouse.Y - Height / 2) * 1.75f * Math.PI / Height);
      var yrot = -(float)((Mouse.X - Width/2) * 1.75f * Math.PI / Width);
      var zrot = 0.0f;
      var rot = Matrix4.CreateRotationX(xrot) * Matrix4.CreateRotationY(yrot) * Matrix4.CreateRotationZ(zrot);
      var eyeTarget = (Vector3.TransformVector(new Vector3(0, 0, 1f), rot)) + center;
      var camera =  Matrix4.LookAt(center, eyeTarget, Vector3.UnitY);
      GL.MatrixMode(MatrixMode.Modelview);
      GL.LoadMatrix(ref camera);

      // Convert texture
      int tex = GL.GenTexture();
      GL.BindTexture(TextureTarget.Texture2D, tex);
      var frame = InputFrame;
      int cams = 1;
      if (frame != null)
      {
        lock (frame)
        {
          cams = frame.Height/30;
          BitmapData data = frame.LockBits(new Rectangle(0, 0, frame.Width, frame.Height),
            ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

          GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, data.Width, data.Height, 0,
            OpenTK.Graphics.OpenGL.PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

          frame.UnlockBits(data);
        }
      }
      GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
      GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);

      // Draw something
      const float zDist = CAM_DIST;
      foreach (var cam in Cameras)
      {
        GL.PushMatrix();
        var camrot = Matrix4.CreateRotationZ(cam.Rot*(float) Math.PI/180f) *
                     Matrix4.CreateRotationY(-cam.Horizontal*(float) Math.PI/180f)*
                     Matrix4.CreateRotationX(-cam.Vertical*(float) Math.PI/180f);
        GL.MultMatrix(ref camrot);

        GL.Color3(Color.White);

        float spanY = 1.0f/cams;
        float texY = (cam.ID - 'A') * spanY;

        // Simple planar mesh ..
        //GL.Begin(PrimitiveType.Quads);
        //GL.TexCoord2(1, texY + spanY);
        //GL.Vertex3(-20, -15, zDist); // bottom left

        //GL.TexCoord2(1, texY);
        //GL.Vertex3(-20, 15, zDist); // top left

        //GL.TexCoord2(0, texY);
        //GL.Vertex3(20, 15, zDist); // top right

        //GL.TexCoord2(0, texY + spanY);
        //GL.Vertex3(20, -15, zDist); // bottom right
        //GL.End();

        // Complex concave/spherical mesh
        int densityX = 16;
        int densityY = 16;
        //GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
        GL.Begin(PrimitiveType.Quads);
        for (int x = 0; x < densityX; x++)
        {
          for (int y = 0; y < densityY; y++)
          {
            GL.TexCoord2(1f - (x) / (float)densityX, texY + spanY - spanY * (y) / densityY);
            GL.Vertex3(-20f + 40f*(x)/densityX, -15f + 30f*(y)/densityY,
              zBend((float) (x)/densityX, (float) (y)/densityY, zDist));

            GL.TexCoord2(1f - (x) / (float)densityX, texY + spanY - spanY * (y+1) / densityY);
            GL.Vertex3(-20f + 40f*(x)/densityX, -15f + 30f*(y + 1)/densityY,
              zBend((float) (x)/densityX, (float) (y+1)/densityY, zDist));

            GL.TexCoord2(1f - (x+1) / (float)densityX, texY + spanY - spanY * (y+1) / densityY);
            GL.Vertex3(-20f + 40f*(x + 1)/densityX, -15f + 30f*(y + 1)/densityY,
              zBend((float) (x+1)/densityX, (float) (y+1)/densityY, zDist));

            GL.TexCoord2(1f - (x+1) / (float)densityX, texY + spanY - spanY * (y) / densityY);
            GL.Vertex3(-20f + 40f*(x + 1)/densityX, -15f + 30f*(y)/densityY,
              zBend((float) (x+1)/densityX, (float) (y)/densityY, zDist));
          }
        }
        GL.End();

        GL.PopMatrix();
      }

      GL.DeleteTexture(tex);

      SwapBuffers();
    }

    private float zBend(float xPercent, float yPercent, float zDist)
    {
      float horizontalFieldOfView = 45;
      float xBendFactor = horizontalFieldOfView; // percent to degree (also: field of view)
      float aspectRatio = 30f/40f;
      float yBendFactor = xBendFactor * aspectRatio; // percent to degree
      return (float)(zDist * Math.Cos((xPercent-0.5) * xBendFactor * Math.PI / 180f)
                           * Math.Cos((yPercent-0.5) * yBendFactor * Math.PI / 180f));
    }

    protected override void OnKeyDown(KeyboardKeyEventArgs e)
    {
      base.OnKeyDown(e);
      if (e.Key == Key.Escape)
      {
        Close();
      }
    }
  }
}
