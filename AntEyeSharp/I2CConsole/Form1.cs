﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Media;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using AntEye.Hardware;
using AntEye.Processing;
using NCalc;

namespace I2CConsole
{
  public partial class Form1 : Form
  {
    // Board
    private CameraMasterBoard Board;

    // Command Input
    private readonly List<string> commandBuffer = new List<string>(31);
    private int commandBufferIndex = 0;

    public Form1()
    {
      InitializeComponent();
    }

    private void Form1_Shown(object sender, EventArgs e)
    {
      Board = CameraMasterBoard.Attach();
      Board.AutomaticBrightnessControl = true;
      Board.FrameReceived += frame => frameView.Invoke(new Action(
        () => frameView.Image = FrameGridAlign.Align(Board, FrameLabel.Label(Board, frame), aspectRatio: 1.0)));
      Board.ResetCameras();
      Board.StartProcessingData();
    }

    private void commandBox_KeyUp(object sender, KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Return)
      {
        // Parse Command
        string command = commandBox.Text;
        commandBox.Text = "";
        e.Handled = true;

        commandBuffer.Insert(0, command);
        commandBufferIndex = -1;

        if (commandBuffer.Count > 30)
          commandBuffer.RemoveAt(commandBuffer.Count-1);

        try
        {
          ParseCommand(command);
        }
        catch (Exception)
        {
          SystemSounds.Beep.Play();
        }
      }
      else if (e.KeyCode == Keys.Up || e.KeyCode == Keys.Down)
      {
        // Scroll in buffer
        commandBufferIndex = commandBufferIndex + (e.KeyCode == Keys.Up ? 1 : -1);
        commandBufferIndex = Math.Min(Math.Max(-1, commandBufferIndex), commandBuffer.Count - 1);
        commandBox.Text = (commandBufferIndex == -1) ? "" : commandBuffer[commandBufferIndex];
        commandBox.Select(commandBox.TextLength, 0);
        e.Handled = true;
      }
    }

    private void ParseCommand(string command)
    {
      if (command == "RESET")
      {
        Board.ResetCameras();
        return;
      }

      // Replace other number bases ..
      command = Regex.Replace(command, @"0x([0-9a-fA-F]+)", match => int.Parse(match.Groups[1].Value, NumberStyles.HexNumber).ToString());
      command = Regex.Replace(command, @"0b([01]+)", match => ParseBinary(match.Groups[1].Value).ToString());
      command = command.Replace(',', ' ');

      string[] words = command.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
      var data = new List<byte>(3);
      foreach (var word in words)
      {
        // Parse more complex math expressions
        byte val = (byte) (int)new Expression(word).Evaluate();
        data.Add(val);
      }
      if (data.Count >= 3)
        Board.SendI2CConfig(data[0], data[1], data[2]);
    }

    private int ParseBinary(string binary)
    {
      int num = 0;
      foreach (var c in binary)
      {
        num <<= 1;
        if (c == '1') num |= 1;
      }
      return num;
    }

    private void commandBox_TextChanged(object sender, EventArgs e)
    {

    }
  }
}
