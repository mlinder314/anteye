﻿namespace I2CConsole
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.frameView = new System.Windows.Forms.PictureBox();
      this.commandBox = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.frameView)).BeginInit();
      this.SuspendLayout();
      // 
      // frameView
      // 
      this.frameView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.frameView.BackColor = System.Drawing.Color.Gray;
      this.frameView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.frameView.Location = new System.Drawing.Point(12, 12);
      this.frameView.Name = "frameView";
      this.frameView.Size = new System.Drawing.Size(646, 350);
      this.frameView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
      this.frameView.TabIndex = 0;
      this.frameView.TabStop = false;
      // 
      // commandBox
      // 
      this.commandBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.commandBox.BackColor = System.Drawing.Color.Black;
      this.commandBox.Font = new System.Drawing.Font("Lucida Console", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.commandBox.ForeColor = System.Drawing.Color.Lime;
      this.commandBox.Location = new System.Drawing.Point(93, 368);
      this.commandBox.Name = "commandBox";
      this.commandBox.Size = new System.Drawing.Size(565, 24);
      this.commandBox.TabIndex = 1;
      this.commandBox.Text = "0xADDRESS 0xMASK 0xVALUE";
      this.commandBox.TextChanged += new System.EventHandler(this.commandBox_TextChanged);
      this.commandBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.commandBox_KeyUp);
      // 
      // label1
      // 
      this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 371);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(75, 17);
      this.label1.TabIndex = 2;
      this.label1.Text = "Command:";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(670, 402);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.commandBox);
      this.Controls.Add(this.frameView);
      this.Name = "Form1";
      this.Text = "AntEye I2C Console";
      this.Shown += new System.EventHandler(this.Form1_Shown);
      ((System.ComponentModel.ISupportInitialize)(this.frameView)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox frameView;
    private System.Windows.Forms.TextBox commandBox;
    private System.Windows.Forms.Label label1;
  }
}

