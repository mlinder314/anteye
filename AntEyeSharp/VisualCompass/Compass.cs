﻿using System;
using System.Drawing;
using System.Linq;
using VisualCompass.Utils;

namespace VisualCompass
{
  /// <summary>
  /// Simple Visual Compass implementation based upon frame comparison
  /// </summary>
  public static class Compass
  {
    /// <summary>
    /// Calculates the frame delta of this image belonging to one of the frames specified in the frame set
    /// </summary>
    public static double[] CalculateDelta(Bitmap[] frameSet, Bitmap testFrame)
    {
      Console.WriteLine("Calculating delta...");
      return frameSet.Select(frame => CalculateDelta(frame, new Bitmap(testFrame))).AsParallel().ToArray();
    }

    private static double CalculateDelta(Bitmap frameA, Bitmap frameB)
    {
      double delta = 0;
      
      // Do a pixel-by-pixel comparison
      int w = frameA.Width;
      int h = frameA.Height;
      using (FastMap imgA = new FastMap(frameA),
        imgB = new FastMap(frameB))
      {
        imgA.Lock();
        imgB.Lock();
        for (int y = 0; y < h; y++)
        {
          for (int x = 0; x < w; x++)
          {
            var pixelA = imgA.GetPixel(x, y);
            var pixelB = imgB.GetPixel(x, y);
            delta += Math.Abs(pixelA.R - pixelB.R);
            delta += Math.Abs(pixelA.G - pixelB.G);
            delta += Math.Abs(pixelA.B - pixelB.B);
          }
        }  
        imgB.Release();
        imgA.Release();
      }

      return delta;
    }
  }
}
