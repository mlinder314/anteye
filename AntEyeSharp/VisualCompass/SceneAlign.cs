﻿using System;
using System.Drawing;
using AntEye;
using AntEye.Hardware;
using AntEye.Processing;

namespace VisualCompass
{
  /// <summary>
  /// Aligns the input Frame into a "sort-of" 2d panoramic representation (not accurately, though)
  /// </summary>
  public static class SceneAlign
  {
    private static Bitmap _frame;

    private class FakeBoard : ICameraBoard
    {
      public Bitmap Frame;

      public void NotifyFrame()
      {
        FrameReceived(Frame);
      }

      public Size TotalFrameSize { get { return Frame.Size; } }
      public Size CameraFrameSize { get { return new Size(Frame.Width, Frame.Height / NumberOfCameras);} }
      public int NumberOfCameras { get { return 16; }}
      public event Action<Bitmap> FrameReceived;
    }

    /// <summary>
    /// Returns the aligned frame
    /// </summary>
    /// <param name="inputFrame"></param>
    /// <returns></returns>
    public static Bitmap Align(Bitmap inputFrame)
    {
      // Create frame
      _frame = new Bitmap(360, 180);
      using (var g = Graphics.FromImage(_frame))
        g.Clear(Color.Transparent);

      // Process data
      var mockBoard = new FakeBoard {Frame = inputFrame};
      new FrameSplitter(mockBoard).SplitFrameReceived += OnSplitFrame;
      mockBoard.NotifyFrame();

      // Return result
      return _frame;
    }

    private static void OnSplitFrame(char camera, Bitmap cam)
    {
      var alignment = Config.Instance.GetAlignment(camera, includeCalibration: false);
      using (var g = Graphics.FromImage(_frame))
      {
        g.ResetTransform();


        // Center cam
        g.TranslateTransform(-cam.Width / 2, 0);
        g.TranslateTransform(_frame.Width / 2, _frame.Height / 2);

        // Use camera alignment data
        g.TranslateTransform((float)alignment.Horizontal, (float)-alignment.Vertical * cam.Height / 45f);
        g.RotateTransform((float)alignment.Rotation);

        // Fix size to fit our cam Frame
        float scaleFactor = (_frame.Width * 45f / 360f) / cam.Width;
        scaleFactor *= 1 + (float)alignment.Zoom;
        g.ScaleTransform(scaleFactor, scaleFactor);

        // Draw the camera image
        g.DrawImageUnscaled(cam, 0, 0);
      }
    }
  }
}
