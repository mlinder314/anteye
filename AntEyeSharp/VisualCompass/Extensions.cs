﻿using System;
using System.Drawing;

namespace VisualCompass
{
  /// <summary>
  /// Extension methods
  /// </summary>
  internal static class Extensions
  {
    /// <summary>
    /// Draws a centered string
    /// </summary>
    public static void DrawStringAlpha(this Graphics g, string text, Font font, Brush brush, float x, float y)
    {
      var measure = g.MeasureString(text, font);
      g.FillRectangle(new SolidBrush(Color.FromArgb(128, 255, 255, 255)), x, y, measure.Width, measure.Height);
      g.DrawString(text, font, brush, x, y);
    }

    /// <summary>
    /// Converts a RectangleF to a rounded Rectangle
    /// </summary>
    /// <param name="rectf"></param>
    /// <returns></returns>
    public static Rectangle ToRect(this RectangleF rectf)
    {
      return new Rectangle((int) Math.Round(rectf.X),
        (int) Math.Round(rectf.Y),
        (int) Math.Round(rectf.Width),
        (int) Math.Round(rectf.Height));
    }
  }
}
