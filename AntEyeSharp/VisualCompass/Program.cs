﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AntEye;
using AntEye.Hardware;
using AntEye.Processing;
using AntEye.Utils;
using AntEye.Visualization;

namespace VisualCompass
{
  /// <summary>
  /// Simple evaluation of the "VisualCompass" algorithm on the ant eye
  /// WITHOUT aligned picture (can be done by using SceneAlign.cs though)
  /// </summary>
  class Program
  {
    [System.Runtime.InteropServices.DllImport("user32")]
    private static extern bool SetProcessDPIAware();

    static void Main(string[] args)
    {
      SetProcessDPIAware();
      new ImageWindow("").Show(); // need one open image
      Application.DoEvents();

      // Load Frame sets
      var circleA = LoadFrameset("circleA");
      var circleB = LoadFrameset("circleB");
      var circleC = LoadFrameset("circleC");

      //PlotArray("circleB[90] in circleB[]", Compass.CalculateDelta(circleB, circleB[90]));
      //PlotArray("circleC[90] in circleB[]", Compass.CalculateDelta(circleB, circleC[90]));
      //PlotArray("circleC[140] in circleB[]", Compass.CalculateDelta(circleB, circleC[140]));
      //PlotArray("circleA[90] in circleB[]", Compass.CalculateDelta(circleB, circleA[90]));
      //PlotArray("circleB[90] in circleC[]", Compass.CalculateDelta(circleC, circleB[90]));
      //PlotArray("circleB[90] in circleA[]", Compass.CalculateDelta(circleA, circleB[90]));

      PlotArray("circleC[30] in circleB[]", Compass.CalculateDelta(circleB, circleC[30]));
      PlotArray("circleC[50] in circleB[]", Compass.CalculateDelta(circleB, circleC[50]));
      PlotArray("circleC[70] in circleB[]", Compass.CalculateDelta(circleB, circleC[70]));
      PlotArray("circleC[90] in circleB[]", Compass.CalculateDelta(circleB, circleC[90]));
      PlotArray("circleC[110] in circleB[]", Compass.CalculateDelta(circleB, circleC[110]));
      PlotArray("circleC[130] in circleB[]", Compass.CalculateDelta(circleB, circleC[130]));
      PlotArray("circleC[150] in circleB[]", Compass.CalculateDelta(circleB, circleC[150]));
      PlotArray("circleC[170] in circleB[]", Compass.CalculateDelta(circleB, circleC[170]));
      
      Application.Run();
    }

    /// <summary>
    /// Loads the specified frameset
    /// </summary>
    /// <param name="name"></param>
    /// <returns></returns>
    private static Bitmap[] LoadFrameset(string name)
    {
      var frames = new Bitmap[500];
      foreach (string file in Directory.GetFiles("testdata", string.Format("{0}*.png", name)))
      {
        int frameNum = int.Parse(Path.GetFileNameWithoutExtension(file).Substring(name.Length));
        frames[frameNum] = (Bitmap)Image.FromFile(file);
      }
        
      var destArray = frames.Where(f => f != null).ToArray();
      Console.WriteLine("Loaded frameset '{0}': {1} frames", name, destArray.Length);
      return destArray;
    }

    /// <summary>
    /// Plots a double array in a normalized diagram
    /// </summary>
    /// <param name="name"></param>
    /// <param name="array"></param>
    /// <param name="widthPerEntry"></param>
    private static void PlotArray(string name, double[] array, int widthPerEntry = 2, bool startAtZero = true)
    {
      // Create img
      const int height = 120;
      var img = new Bitmap(array.Length*widthPerEntry, height);

      // Gather some data stats
      double minValue = array.Min();
      if (startAtZero) minValue = 0;
      double maxValue = array.Max();
      double heightSpan = maxValue - minValue;
      var centerY = (float)(Math.Max(maxValue, 0)*height/heightSpan);

      // Draw img
      using (var g = Graphics.FromImage(img))
      {
        g.Clear(Color.White);

        // Draw the bars
        for (int i = 0; i < array.Length; i++)
        {
          int startX = i*widthPerEntry;
          //var brush = (i%2) == 0 ? Brushes.Red : Brushes.OrangeRed;
          var brush = (i % 2) == 0 ? Brushes.ForestGreen : Brushes.LawnGreen;
          var startY = (float) ((maxValue - array[i])*height/heightSpan);
          var endY = (startY < centerY) ? (centerY - startY) : (startY - centerY);
          if (startY > centerY)
            startY = centerY;
          var rect = new RectangleF(startX, startY, widthPerEntry, endY);
          g.FillRectangle(brush, rect);
        }

        // Draw the center line
        g.DrawLine(Pens.Black, 0, centerY, img.Width, centerY);

        // Draw some text
        var fontA = new Font(FontFamily.GenericSansSerif, 9, FontStyle.Bold);
        var fontB = new Font(FontFamily.GenericSansSerif, 9);
        g.DrawStringAlpha(string.Format("{0}", name), fontA, Brushes.Black, 5, 5);
        g.DrawStringAlpha(string.Format("values from {0:F1} to {1:F1}", minValue, maxValue), fontB, Brushes.Black, 5, height - 5 - g.MeasureString("foo", fontB).Height);
      }

      WinUtil.ShowImage(name, img);
    }
  }
}
