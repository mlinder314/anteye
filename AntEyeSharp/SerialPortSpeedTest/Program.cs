﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerialPortSpeedTest
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("UART Speed Test");
      Console.WriteLine("================");

      // Ask user for port
      string portStr;
      if (SerialPort.GetPortNames().Length > 1)
      {
        Console.WriteLine("Ports available: " + SerialPort.GetPortNames().Aggregate((a, b) => a + " " + b));

        int portNum;
        do
        {
          Console.Write("Specify port to use: COM");
        } while (!int.TryParse(Console.ReadLine(), out portNum));
        portStr = "COM" + portNum;
      }
      else
      {
        // Auto select port
        portStr = SerialPort.GetPortNames().First();
      }
      Console.WriteLine();

      const int baud = 4000000; // 4 MHz
      var port = new SerialPort(portStr, baud, Parity.None, 8, StopBits.One);
      port.Open();

      byte[] buffer = new byte[4096];
      new Random().NextBytes(buffer);

      port.DiscardInBuffer();
      port.DiscardOutBuffer();
      
      Console.WriteLine("Starting measurement ...");
      var timer = new Stopwatch();
      timer.Start();
      // -- BEGIN MEASURE
      int toRead = 0;
      int iters = 0;
      while (timer.Elapsed.TotalSeconds < 5)
      {
        iters++;

        //Console.WriteLine("Writing ..");
        //port.Write(buffer, 0, buffer.Length);
        toRead += buffer.Length;
        //Console.WriteLine("Write: {0}", toRead);
        do
        {
          toRead -= port.Read(buffer, 0, buffer.Length);
          //Console.WriteLine("toRead: {0}", toRead);
        } while (toRead > 0);
      }
      // -- END MEASURE
      timer.Stop();
      Console.WriteLine("Measurement completed: {0} iters in {1}", iters, timer.Elapsed);
      double bytesPerSec = (buffer.Length*iters)/(timer.Elapsed.TotalSeconds);
      Console.WriteLine("=> {0:F1}  Bytes/sec", bytesPerSec);
      Console.WriteLine("=> {0:F1} KBytes/sec", bytesPerSec / 1024.0);
      Console.WriteLine("=> {0:F1} MBytes/sec", bytesPerSec / 1024.0 / 1024.0);
      Console.ReadLine();
    }
  }
}
