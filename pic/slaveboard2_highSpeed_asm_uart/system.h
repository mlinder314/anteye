/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/

/* Microcontroller MIPs (FCY) */
#define SYS_FREQ        32000000L
#define FCY             SYS_FREQ/2

//#define UART_BAUD 250000L
#define UART_BAUD 500000L

#define I2C_DELAY __delay_us(5) // *2 = ~50 KHz

/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

void ConfigureOscillator(void); /* Handles clock switching/osc initialization */

inline void InitUART(void);
void PrintChar(char chr);
void PrintString(char* str);
void PrintNumber(uint16_t num, uint16_t start);

inline void InitI2C(void);
void I2CWrite(uint8_t device, uint8_t reg, uint8_t value);
uint8_t I2CRead(uint8_t device, uint8_t reg);
void I2CSet(uint8_t device, uint8_t reg, uint8_t mask, uint8_t value);