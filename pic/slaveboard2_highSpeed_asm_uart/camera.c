/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>         /* For true/false definition */

#include "system.h"
#include <libpic30.h>
#include <PPS.h>
#include <uart.h>
#include <ports.h>
#include <pmp.h>

#include "camera.h"  

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

// Create a framebuffer that can store the entire image. Add an overflow buffer
// so that we do not overwrite memory in case our VSYNC interrupt is handled late.
#define FRAME_BUF_LEN (40*30)
static uint16_t frame[FRAME_BUF_LEN + 16 /* overflow buffer */];
static uint16_t frameNum = 0;
//extern void* framePixelPtr; /* points to the current pixel written by PMP */

// Reserve registers for high speed asm interrupt routine
register uint16_t pixelWorkRegister1 asm ("w13");
register void* framePixelPtr asm ("w12");

void InitApp(void)
{
    PrintString("\r\n\r\n");
    PrintString("= AntEye SlaveBoard =\r\n");
    PrintString("=====================\r\n");

    // Setup LED output
    TRISAbits.TRISA2 = 0;
    PORTAbits.RA2 = 1; // Enable LED at start

    /**************** Reset Camera Module ****************/
    // Reset camera (active low)
    TRISAbits.TRISA4 = 0; // reset camera
    PORTAbits.RA4 = 0; // drive low
    __delay_ms(10);
    PORTAbits.RA4 = 1; // drive high

    //framePixelPtr = frame; // Reset pointer to beginning of the frame
    asm("mov #_frame, w12"); // framePixelPtr = &frame; (reset to start)
    asm("mov #PMDIN1, w13"); // store address of PMDIN1 in w12

    /**************** Setup Camera Module Config ****************/
    __delay_ms(10); // Wait for Module to load

    // Check that we are talking to the right module
    uint8_t midh = I2CRead(CAM, CAM_MIDH);
    uint8_t midl = I2CRead(CAM, CAM_MIDL);
    if (midh != CAM_MIDH_EXPECTED || midl != CAM_MIDL_EXPECTED)
    {
        PrintString("I2C Camera error:\r\n");
        PrintString("  MIDH = "); PrintNumber(midh, 100); PrintString("\r\n");
        PrintString("  MIDL = "); PrintNumber(midl, 100); PrintString("\r\n");
        while (1) ;
    }
    PrintString("I2C Camera detected\r\n");
    PrintString("  PID = "); PrintNumber(I2CRead(CAM, CAM_PID), 100); PrintString("\r\n");
    PrintString("  VER = "); PrintNumber(I2CRead(CAM, CAM_VER), 100); PrintString("\r\n");

    // Setup Clock Speed (need to get to 24 MHz: 8 * 6 / 2 )
    // ** 12 fps / 9.6 MHz (the only one that works without an ASM ISR)
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 5-1); // div 5
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6

    // ** 13.37 fps / 10.66 MHz
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 6-1); // div 6
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b11 << 6); // pll 8
    
    // ** 15 fps / 12 MHz
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 4-1); // div 4
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6

    // ** 16 fps / 12.8 MHz
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 5-1); // div 5
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b11 << 6); // pll 8

    // ** 20 fps / 16 MHz
    I2CSet(CAM, CAM_CLKRC, 0xFF, 3-1); // div 3
    I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6
    
    // Setup Clock Signals
    I2CSet(CAM, CAM_COM10, (1 << 4), 0xFF); // PCLK reverse (inactive = low)
    I2CSet(CAM, CAM_COM10, (1 << 5), 0xFF); // PCLK does not toggle during HBLANK
    I2CSet(CAM, CAM_COM12, (1 << 7), 0x00); // No HREF during low VSYNC
    I2CSet(CAM, CAM_COM14,  (0b111), 0b100); // PCLK div 16 = 40 pixels wide
    I2CSet(CAM, CAM_COM14,  (1 << 4), 0xFF); // Enable PCLK div
    I2CSet(CAM, CAM_COM14,  (1 << 3), 0xFF); // Enable manual scaling

    // Setup Frame size (640x480 -> 80x60, div 8)
    I2CWrite(CAM, CAM_SCALING_DCWCTR, 0b11111111); // round, div 8:8
    I2CSet(CAM, CAM_SCALING_PCLK_DIV, 0b1111, 0b0011); // dsp enabled, clock div 8
    I2CSet(CAM, CAM_COM3, (1 << 2), 0xFF); // enable down sampling

    // Fix Resolution (75x60 -> 80x60)
    I2CSet(CAM, CAM_HSTART, 0xFF, 0x0F); // empirically determined, no idea why
    I2CSet(CAM, CAM_HSTOP, 0xFF, 0x04);
    I2CSet(CAM, CAM_COM1, (1 << 6), 0xFF);

    //Scale to 40x30
    I2CSet(CAM, CAM_SCALING_XSC, 0b1111111, 32); // no scale, changed by pclk
    I2CSet(CAM, CAM_SCALING_YSC, 0b1111111, 64); // *0.5
    I2CSet(CAM, CAM_COM3, (1 << 3), 0xFF); // enable down zooming

    // Setup Image Format
    I2CSet(CAM, CAM_COM7, 0b101, 0b100); // rgb
    I2CSet(CAM, CAM_COM15, 0b11110000, 0b11010000); // rgb 565, 00-FF
    I2CSet(CAM, CAM_TSLB, (1 << 3), 0x00); // swap byte order LH -> HL

    // Color Settings
    I2CSet(CAM, CAM_COM8, (1 << 6), 0xFF); // unlimited AEC step size
    //I2CSet(CAM, CAM_ADCCTR0, (1 << 3), 0xFF); // 1.5 ADC range = more colorful
    //I2CSet(CAM, CAM_ADCCTR0, 0b111, 0xFF); // 1.2x ADC adjust = darker


    // HACK/Fastmode: Double framerate by dividing image width
    // Results in 20x60 at 21 Hz
#if 0
    I2CSet(CAM, CAM_COM14,  (0b111), 0b101); // PCLK div 32 = 20 pixels wide
    I2CSet(CAM, CAM_CLKRC, 0xFF, 3-1); // div 3 (this works barely!)
    I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6
    OSCTUNbits.TUN += 11; // ~ +0.5 Hz
#endif

    // DEBUG: Test Pattern
    //I2CSet(CAM, CAM_SCALING_YSC, (1 << 7), 0xFF); // 8 bar test pattern

    PrintString("I2C Camera configured\r\n");

    /**************** Setup Camera Clock Triggers, Data Ports ****************/
    // Setup PLCK input (RB14)
    TRISBbits.TRISB14 = 1;
    EnablePullUpCN12;

    // Setup HREF input (RB15)
    TRISBbits.TRISB15 = 1;
    EnablePullUpCN11;

    // Setup VSYNC input (RA1)
    TRISAbits.TRISA1 = 1;
    EnablePullUpCN3;
    EnableCN3; // need interrupt for this

    // Setup PMP module for pixel readout:
    // 8 bit buffered legacy slave mode with write strobe + cable select
    PMPClose();
    uint16_t pmp_control = BIT_PMP_ON | BIT_SIDL_OFF | BIT_BE_OFF | BIT_RD_WR_ON |
             BIT_USE_CS2_CS1 | BIT_CS1_HI | BIT_RD_WR_HI;
    uint16_t pmp_mode = BIT_IRQ_BUF | BIT_INC_ADDR_AUTO | BIT_DATA_8 |
            BIT_MODE_SLAVE | BIT_WAITM_0_TCY;
    uint16_t pmp_port = 0;
    uint16_t pmp_addr = BIT_CS1_ON;
    uint8_t pmp_intr = 0;
    PMPOpen(pmp_control, pmp_mode, pmp_port, pmp_addr, pmp_intr);

    /**************** Get Ready ****************/
    // Enable pin change interrupts (should be higher than pmp)
    ConfigIntCN(INT_ENABLE & INT_PRI_3);

    // Enable PMP interrupt (this has to be the last step!)
    IEC2bits.PMPIE = 1;
    IPC11bits.PMPIP = 7; // service with highest prio since fast action needed

    PrintString("Beginning Main Loop...\r\n");

    // TODO: Enable CS in PMAEN?
}

void __attribute__ ((interrupt,no_auto_psv)) _CNInterrupt(void) {
    InputChange_Clear_Intr_Status_Bit;

    // Check VSYNC (RA1)
    if (PORTAbits.RA1) {
        // Reset Pointer to the beginning of frame
        //framePixelPtr = frame; // Reset pointer to beginning of the frame
        asm("mov #_frame, w12"); // framePixelPtr = &frame; (reset to start)
        
        // Clear eventual overflows (needed to continue PMP operation)
        PMDIN1;
        PMDIN2;
        mPMPClearBufferOverflow;

        // Toggle LED
        LATAbits.LATA2 ^= 1;

        // Increase our frame stats
        frameNum++;
    }
}

inline void Loop(void)
{
    // Use synchronous UART for transmission
    PrintString("BOF"); // frame begins

    // Push the entire frame into the UART bus
    unsigned int i;
    for (i = 0; i < FRAME_BUF_LEN; i++) {
        uint16_t pixel = frame[i];
        PrintChar(pixel & 0xFF);
        PrintChar(pixel >> 8);
    }
    PrintString("EOF "); // frame ends

    // Print some additional textual stats
    PrintNumber(frameNum, 10000);
    PrintString("\r\n");
}