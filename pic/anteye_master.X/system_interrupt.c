/*******************************************************************************
 System Interrupt Source File

  Company:
    Microchip Technology Inc.
  
  File Name:
    system_interrupt.c

  Summary:
    Raw ISR definitions.

  Description:
    This file contains a definitions of the raw ISRs required to support the
    interrupt sub-system.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2011-2012 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include <xc.h>
#include <sys/attribs.h>
#include "app.h"
#include "dma/sys_dma.h"
#include "usart/drv_usart.h"


void __ISR( _TIMER_2_VECTOR, ipl3) _InterruptHandler_TMR ( void )
{
    DRV_TMR_Tasks(sys.tmrDrv);
}


void __ISR ( _USB_1_VECTOR,ipl4) _InterruptHandler_USB ( void )
{
    USB_DEVICE_Tasks_ISR(sys.usbDev);
}

/*** DMA Handlers ***/
void __ISR(_DMA_0_VECTOR,ipl5) _InterruptHandler_DMA0(void)
{
    SYS_DMA_TasksISR(sys.dmaSys, DMA_CHANNEL_0);
    SYS_DMA_TasksErrorISR(sys.dmaSys, DMA_CHANNEL_0);
}

void __ISR(_DMA_1_VECTOR,ipl5) _InterruptHandler_DMA1(void)
{
    SYS_DMA_TasksISR(sys.dmaSys, DMA_CHANNEL_1);
    SYS_DMA_TasksErrorISR(sys.dmaSys, DMA_CHANNEL_1);
}

void __ISR(_DMA_2_VECTOR,ipl5) _InterruptHandler_DMA2(void)
{
    SYS_DMA_TasksISR(sys.dmaSys, DMA_CHANNEL_2);
    SYS_DMA_TasksErrorISR(sys.dmaSys, DMA_CHANNEL_2);
}

void __ISR(_DMA_3_VECTOR,ipl5) _InterruptHandler_DMA3(void)
{
    SYS_DMA_TasksISR(sys.dmaSys, DMA_CHANNEL_3);
    SYS_DMA_TasksErrorISR(sys.dmaSys, DMA_CHANNEL_3);
}

/*** USART Handlers ***/
void __ISR(_UART_1_VECTOR,ipl5) _InterruptHandler_UART1(void) {
    // RX is done via DMA channel 0
    DRV_USART_TasksTransmit(sys.usartDrv0);

    // Clear errors?
    if (IFS0bits.U1EIF) {
        IFS0bits.U1EIF = 0;
    }
    if (U1STAbits.OERR) {
        U1STAbits.OERR = 0;
    }
}
void __ISR(_UART_2_VECTOR,ipl5) _InterruptHandler_UART2(void) {
    // RX is done via DMA channel 1
    DRV_USART_TasksTransmit(sys.usartDrv1);

    // Clear errors?
    if (IFS1bits.U2EIF) {
        IFS1bits.U2EIF = 0;
    }
    if (U2STAbits.OERR) {
        U2STAbits.OERR = 0;
    }
}
void __ISR(_UART_3_VECTOR,ipl5) _InterruptHandler_UART3(void) {
    // RX is done via DMA channel 2
    DRV_USART_TasksTransmit(sys.usartDrv2);

    // Clear errors?
    if (IFS1bits.U3EIF) {
        IFS1bits.U3EIF = 0;
    }
    if (U3STAbits.OERR) {
        U3STAbits.OERR = 0;
    }
}
void __ISR(_UART_4_VECTOR,ipl5) _InterruptHandler_UART4(void) {
    // RX is done via DMA channel 3
    DRV_USART_TasksTransmit(sys.usartDrv3);

    // Clear errors?
    if (IFS2bits.U4EIF) {
        IFS2bits.U4EIF = 0;
    }
    if (U4STAbits.OERR) {
        U4STAbits.OERR = 0;
    }
}

void __ISR(_UART_5_VECTOR,ipl5) _InterruptHandler_UART5(void) {
    DRV_USART_TasksReceive(sys.usartDrvDebug); // not really used..but deal with it anyway
    DRV_USART_TasksTransmit(sys.usartDrvDebug);
}