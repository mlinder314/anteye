/*******************************************************************************
  MPLAB Harmony Application

  Application Header
  
  Company:
    Microchip Technology Inc.

  File Name:
    app.h

  Summary:
	Application definitions. 

  Description:
	 This file contains the  application definitions.
*******************************************************************************/

//DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
//DOM-IGNORE-END

#ifndef _APP_HEADER_H
#define _APP_HEADER_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <xc.h>

#include "bsp_config.h"
#include "system_config.h"

#include "system/system.h"
#include "system/devcon/sys_devcon.h"
#include "system/int/sys_int.h"
#include "system/tmr/sys_tmr.h"
#include "system/clk/sys_clk.h"

#include "driver/driver_common.h"
#include "driver/usart/drv_usart.h"
#include "usb/usb_cdc.h"
#include "usb/usb_device.h"
#include "usb/usb_device_cdc.h"


// *****************************************************************************
// *****************************************************************************
// Section: Type Definitions
// *****************************************************************************
// *****************************************************************************

#define KHz * (1000)
#define MHz * (1000 KHz)

// The tmr_p32mx675f256h.h header is buggy and does not contain the correct
// Prescaler settings for this chip, so it sets the prescaler to 256 and thinks
// internally that it was set to 65536 -> we need to adjust all our time values
// by this factor
#define TIMER_BUG_FIX 256


/* Width of a single camera image*/
#define FRAME_CAM_WIDTH 40

/* Height of a single camera image */
#define FRAME_CAM_HEIGHT 30

/* Bytes per pixel */
#define FRAME_CAM_BPP 2

/* Length of a single camera frame */
#define FRAME_CAM_LEN (FRAME_CAM_WIDTH*FRAME_CAM_HEIGHT*FRAME_CAM_BPP)

/* Number of cameras */
#define FRAME_CAM_COUNT 16

/* Total content length of a frame */
#define FRAME_CONTENT_LEN (FRAME_CAM_LEN*FRAME_CAM_COUNT)

/* Length of a frame header*/
#define FRAME_HEADER_LEN 3

/* Length of a frame footer */
#define FRAME_FOOTER_LEN 3

/* Total lenth of the whole frame buffer */
#define FRAME_BUFFER_LEN (FRAME_HEADER_LEN + FRAME_CONTENT_LEN + FRAME_FOOTER_LEN)

/* Extra bytes to help against overruns */
#define FRAME_BUFFER_OVERRUN 1024

/* Half a framebuffer */
#define FRAME_BUFFER_HALF_LEN (FRAME_HEADER_LEN + FRAME_CONTENT_LEN/2 + FRAME_FOOTER_LEN)

/* Number of camera buses */
#define CAM_BUS_COUNT 4

/* The first camera ID available */
#define CAM_ID_START 'A'

/* Maximum number of camera IDs to probe for*/
#define CAM_ID_COUNT 16

/* Number of ticks the USART bus has to be idle for a command to be considered finished */
/* Required value is inversely correlated to the bus speed, and number of buses in use
   (since tickTime is directly affected by that ... )*/
#define MIN_BUS_IDLE_TICKS 5

/* Bus Speed on which the bus is running by default*/
#define CAM_BUS_INIT_SPEED 1000000

/* Camera bus speed in baud */
#define CAM_BUS_SPEED 1000000 /* this is the max; higher won't work */

// *****************************************************************************
/* Application states

  Summary:
    Application states enumeration

  Description:
    This enumeration defines the valid application states.  These states
    determine the behavior of the application at various times.
*/

typedef enum
{
	/* Application's state machine's initial state. */
	APP_STATE_INIT = 0,

        /* Application is currently waiting and not doing anything */
        APP_STATE_WAITING,

        /* Application is waiting for the cameras to reset */
        APP_STATE_RESET_CAM,

        /* Setup UART baudrate*/
        APP_STATE_SETUP_UART,
                
        /* Initialization has finished*/
        APP_STATE_RUNNING,
} APP_STATES;

typedef enum
{
	/* No action happening */
	IO_STATE_IDLE = 0,

        /* Operation has been scheduled, but has not yet been completed */
        IO_STATE_SCHEDULED,

        /* IO operation has been completed*/
        IO_STATE_COMPLETED,
} IO_STATES;


typedef enum
{
    /* Bus is not yet initialized */
    BUS_STATE_INIT = 0,

    /* Bus is currently being probed for cameras */
    BUS_STATE_PROBE_CAMERAS,

    /* Wait for probe response */
    BUS_STATE_PROBE_CAMERAS_RESPONSE,

    /* Start a new frame */
    BUS_STATE_START_NEW_FRAME,

    /* We should request a frame from a camera */
    BUS_STATE_REQUEST_IMAGE_FROM_CAMERA,

    /* We are waiting for the response from the camera */
    BUS_STATE_WAIT_FOR_CAMERA_IMAGE,
} BUS_STATES;


// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    Application strings and buffers are be defined outside this structure.
 */

typedef struct
{
        /*********** GENERAL *************/
	/* The application's current state */
	APP_STATES state;

        /* LED Blink Speed (0 to 7) */
        int ledBlinkSpeed;

        /* Image Frame Bufferr */
        char* frameBuffer;

        /*********** USB *************/

        /* Line Encoding (stopbits, parity, ...) -- needed for CDC */
        USB_CDC_LINE_CODING lineCoding;

        /* Handle to the opened usb device */
	USB_DEVICE_HANDLE usbDevice;

        /* True if this device is completely configured and can be used */
        bool usbReady;

        /* Read Buffer */
        char usbReadBuffer[USB_DEVICE_EP0_BUFFER_SIZE];

        /* Specifies the state of the IN-Endpoint */
        IO_STATES usbReadState;

        /* Specifies the state of the OUT-Endpoint */
        IO_STATES usbWriteState;

        /* Number of bytes read in the last read request */
        int usbLastReadBytes;

        /*********** USART *************/
        DRV_HANDLE usartHandle[CAM_BUS_COUNT];
        DRV_HANDLE usartDebugHandle;
        DRV_USART_BUFFER_HANDLE usartWriteHandle[CAM_BUS_COUNT];
        DRV_USART_BUFFER_HANDLE usartReadHandle[CAM_BUS_COUNT];

} APP_DATA;


// *****************************************************************************

/* Driver objects.

  Summary:
    Holds driver objects.

  Description:
    This structure contains driver objects returned by the driver init routines
    to the application. These objects are passed to the driver tasks routines.

  Remarks:
    None.
*/

typedef struct
{
    SYS_MODULE_OBJ tmrDrv;
    SYS_MODULE_OBJ tmrSys;

    SYS_MODULE_OBJ usbDev;

    SYS_MODULE_OBJ dmaSys;

    SYS_MODULE_OBJ usartDrv0;
    SYS_MODULE_OBJ usartDrv1;
    SYS_MODULE_OBJ usartDrv2;
    SYS_MODULE_OBJ usartDrv3;

    SYS_MODULE_OBJ usartDrvDebug; // Debug UART endpoint
} APP_DRV_OBJECTS;


// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************

void UsbDeviceEventHandler ( USB_DEVICE_EVENT event, void * eventData, uintptr_t context );
USB_DEVICE_CDC_EVENT_RESPONSE CdcEventHandler
(
    USB_DEVICE_CDC_INDEX index ,
    USB_DEVICE_CDC_EVENT event ,
    void * pData,
    uintptr_t userData
);
	
// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************

void debugPrint(const char* str);

/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Summary:
     MPLAB Harmony Demo application initialization routine

  Description:
    Opens necessary drivers and calls proper initializations

  Precondition:
    All other system initialization routines should be called before calling
    this routine (in "SYS_Initialize").

  Parameters:
    None.

  Returns:
    None.

  Example:
    APP_Initialize();


  Remarks:
    This routine must be called from the SYS_Initialize function.
*/

void APP_Initialize ( void );


/*******************************************************************************
  Function:
    void APP_Tasks ( void )

  Summary:
    Performs the applicationst asks on a regular basis

  Precondition:
    The system and application initialization ("SYS_Initialize") should be
    called before calling this.

  Parameters:
    None.

  Returns:
    None.

  Example:
    <code>
    APP_Tasks();
    </code>

  Remarks:
    This routine must be called from SYS_Tasks() routine.
 */

void APP_Tasks ( void );

void SYS_Initialize ( void* data );
void SYS_Tasks ( void );

// *****************************************************************************
// *****************************************************************************
// Section: extern declarations
// *****************************************************************************
// *****************************************************************************

extern APP_DRV_OBJECTS sys;

extern APP_DATA data;

extern const USB_DEVICE_FUNCTION_REGISTRATION_TABLE funcRegistrationTable[USB_DEVICE_CDC_INSTANCES_NUMBER];
extern const USB_DEVICE_MASTER_DESCRIPTOR usbMasterDescriptor;


#endif /* _APP_HEADER_H */

/*******************************************************************************
 End of File
 */



