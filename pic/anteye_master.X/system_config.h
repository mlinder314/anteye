/*******************************************************************************
  MPLAB Harmony Demo Configuration Header

  Company:
    Microchip Technology Inc.

  File Name:
    system_config.h

  Summary:
    Top-level configuration header file.

  Description:
    This file is the top-level configuration header for the Harmony Demo
    application for the Explorer-16 board with PIC32MX795F512L.
*******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
*******************************************************************************/
// DOM-IGNORE-END

#ifndef _SYS_CONFIG_H
#define _SYS_CONFIG_H


// *****************************************************************************
// *****************************************************************************
// Section: Included Files
// *****************************************************************************
// *****************************************************************************
/*  This section Includes other configuration headers necessary to completely
    define this configuration.
*/

/* System Peripheral Clock Speed */
#define SYS_PER_CLOCK 80000000

// *****************************************************************************
// *****************************************************************************
// Section: DEVCON System Service Configuration
// *****************************************************************************
// *****************************************************************************
#define SYS_DEVCON_PIC32MX_MAX_PB_FREQ 80000000


// *****************************************************************************
// *****************************************************************************
// Section: Clock System Service Configuration
// *****************************************************************************
// *****************************************************************************

/* Input crystal frequency for this board */
#define SYS_CLK_CONFIG_PRIMARY_XTAL               16000000 // 16 MHz

/* PLL input clock divider value*/
#define SYS_CLK_CONFIG_SYSPLL_INP_DIVISOR         4

/* There is no secondary crystal */
#define SYS_CLK_CONFIG_SECONDARY_XTAL             0L

/* USB PLL is enabled */
#define SYS_CLK_CONFIGBIT_USBPLL_ENABLE           true

/* USB divisor to get to 4 MHz */
#define SYS_CLK_CONFIGBIT_USBPLL_DIVISOR          4

/* Default USB Clock */
#define SYS_CLK_CONFIG_USB_CLOCK                  48000000L // 48 MHz

/* Frequency calculation tolerance */
#define SYS_CLK_CONFIG_FREQ_ERROR_LIMIT           10

// *****************************************************************************
// *****************************************************************************
// Section: TMR System Service Configuration
// *****************************************************************************
// *****************************************************************************

/* Support a maximum of 5 delay requests */
#define SYS_TMR_MAX_DELAY_EVENTS    5

/* Error tolerance */
#define SYS_TMR_ERROR_TOLERANCE     0

/* Disable interrupt mode */
#define SYS_TMR_INTERRUPT_MODE      true

// *****************************************************************************
// *****************************************************************************
// Section: TMR Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* Use a max of 1 hardware instance */
#define DRV_TMR_INSTANCES_NUMBER    1

/* Service a max of 1 client */
#define DRV_TMR_CLIENTS_NUMBER      1

/* Enable interrupt */
#define DRV_TMR_INTERRUPT_MODE      true

/* Support Prescaler */
#define DRV_TMR_PRESCALER_ENABLE    true

// *****************************************************************************
// *****************************************************************************
// Section: USART Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* We're using all UART interfaces*/
#define DRV_USART_INSTANCES_NUMBER      5

/* We have one client per interface */
#define DRV_USART_CLIENTS_NUMBER        5

/* Interrupts are always good*/
#define DRV_USART_INTERRUPT_MODE        true

/* Arbitrary buffer depth ... */
#define DRV_USART_QUEUE_DEPTH_COMBINED  52

/* Use the Queuing Model */
#define DRV_USART_BUFFER_QUEUE_SUPPORT  true

/* Disable byte model */
#define DRV_USART_BYTE_MODEL_SUPPORT  false

/* Disable ReadWrite model */
#define DRV_USART_READ_WRITE_MODEL_SUPPORT  false

/* Transmit DMA */
#define DRV_USART_SUPPORT_TRANSMIT_DMA false

/* Receive DMA */
#define DRV_USART_SUPPORT_RECEIVE_DMA true


// *****************************************************************************
// *****************************************************************************
// Section: USB controller Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* Enables Device Support */
#define DRV_USB_DEVICE_SUPPORT      true

/* Disables host support */
#define DRV_USB_HOST_SUPPORT        false

/* Provides 3 endpoints*/
#define DRV_USB_ENDPOINTS_NUMBER    3

/* Only one instance of the USB Peripheral*/
#define DRV_USB_INSTANCES_NUMBER    1

/* Enables interrupt mode */
#define DRV_USB_INTERRUPT_MODE      true

// *****************************************************************************
// *****************************************************************************
// Section: USB Device Layer Configuration
// *****************************************************************************
// *****************************************************************************

/* Maximum device layer instances */
#define USB_DEVICE_INSTANCES_NUMBER        1

/* Maximum clients for Device Layer */
#define USB_DEVICE_CLIENTS_NUMBER          1

/* EP0 size in bytes */
#define USB_DEVICE_EP0_BUFFER_SIZE          64

/* CDC Device index */
#define USB_DEVICE_CDC_INDEX_0 0


// *****************************************************************************
// *****************************************************************************
// Section: CDC Function Driver Configuration
// *****************************************************************************
// *****************************************************************************

/* Maximum instances of CDC function driver */
#define USB_DEVICE_CDC_INSTANCES_NUMBER   1

/* CDC Transfer Queue Size for both read and
   write. Applicable to all instances of the
   function driver */
#define USB_DEVICE_CDC_QUEUE_DEPTH_COMBINED      6


// *****************************************************************************
// *****************************************************************************
// Section: Application Configuration
// *****************************************************************************
// *****************************************************************************



#endif // _SYS_CONFIG_H
/*******************************************************************************
 End of File
*/

