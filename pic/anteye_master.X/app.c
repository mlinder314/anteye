/*******************************************************************************
 **  AntEye Master Board
 **
 **  Gathers data from all slave cameras and provides a USB interface
 **
 **  (c) 2014 Matthias Linder, University of Edinburgh
 *******************************************************************************/


// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "tmr/drv_tmr.h"
#include "usart/drv_usart.h"
#include "tmr/src/sys_tmr_local.h"

// *****************************************************************************
// *****************************************************************************
// Section: Global Variable Definitions
// *****************************************************************************
// *****************************************************************************


/*****************************************************
 * Initialize the application data structure. All
 * application related variables are stored in this
 * data structure.
 *****************************************************/

APP_DATA data =
{
    .ledBlinkSpeed = 4,
    .lineCoding = {9600, 0, 0, 8},
    .usbReadState = IO_STATE_IDLE,
    .usbWriteState = IO_STATE_IDLE,
};

/* The static camera that contains the merged camera image*/
static char framebuffer[FRAME_BUFFER_LEN + FRAME_BUFFER_OVERRUN];

/* Starting position of frame 0 */
static void * frame0 = &framebuffer[FRAME_HEADER_LEN];

/* Defines how many cameras there are on each bus. Values automatically updated
 * by the camera probing algorithm. Set value to 0 to disable a bus. */
static int8_t camerasOnBus[CAM_BUS_COUNT] = { 1, 1, 1, 1 };

/* Defines the ID of each camera on each of the busses. Automatically filed in
 * by the camera probing algorithm */
static uint8_t cameraIDs[CAM_BUS_COUNT][CAM_ID_COUNT];

/* State of each camera bus */
static BUS_STATES cameraBusState[CAM_BUS_COUNT];

/* Iterator for each camera bus, counts through cameras*/
static uint8_t cameraBusFrameIterator[CAM_BUS_COUNT];

/* Separate Command buffers for writing UART data to the camera */
static uint8_t cameraCommandBuffers[CAM_BUS_COUNT][16];

/* Larger command buffer for one-time setup commands */
static uint8_t genericCameraCommandBuffer[32];

/* How many idle ticks we counted on a bus */
static uint16_t cameraBusIdleTicks[CAM_BUS_COUNT];

/* Align by camera IDs? */
static bool alignByCameraID = 1;

/* Double speed/30hz mode? */
static bool highSpeedMode = 0;

//#define debugPrint2(str) debugPrint(str);
//#define debugPrint3(str) debugPrint(str);
#define debugPrint2(str) ;
#define debugPrint3(str) ;

// *****************************************************************************
/* Driver objects.

  Summary:
    Contains driver objects.

  Description:
    This structure contains driver objects returned by the driver init routines
    to the application. These objects are passed to the driver tasks routines.
*/

APP_DRV_OBJECTS sys;

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Routines
// *****************************************************************************
// *****************************************************************************

void EraseFrameBuffer() {
    // Setup our framebuffer
    int i = 0;
    framebuffer[i++] = 'B';
    framebuffer[i++] = 'O';
    framebuffer[i++] = 'F';
    int cam;
    for (cam = 0; cam < FRAME_CAM_COUNT; cam++) {
        int px;
        uint16_t color = (cam * 18383);
        for (px = 0; px < FRAME_CAM_LEN/2/(highSpeedMode ? 2 : 1); px++) {
            framebuffer[i++] = color & 0xFF;
            framebuffer[i++] = color >> 8;
        }
    }
    framebuffer[i++] = 'E';
    framebuffer[i++] = 'O';
    framebuffer[i++] = 'F';
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Routines
// *****************************************************************************
// *****************************************************************************
void UsbDeviceEventHandler ( USB_DEVICE_EVENT event, void * eventData, uintptr_t context )
{
    uint8_t * configuredEventData;

    switch ( event )
    {
        case USB_DEVICE_EVENT_RESET:

            /* Update LED to show reset state */
            data.usbReady = false;
            data.ledBlinkSpeed = 5; /* fast */
            
            /* Reset data structure */
            data.usbLastReadBytes = 0;
            data.usbWriteState = IO_STATE_IDLE;
            data.usbReadState = IO_STATE_IDLE;

            break;

        case USB_DEVICE_EVENT_CONFIGURED:

            /* Check the configuratio. We only support configuration 1 */
            configuredEventData = (uint8_t *)eventData;
            if ( *configuredEventData == 1)
            {
                /* Update LED to show configured state */
                data.ledBlinkSpeed = -1; // no blinky blinky required
                BSP_LED0 = true;

                /* Register the CDC Device application event handler here.
                 * Note how the appData object pointer is passed as the
                 * user data */
                USB_DEVICE_CDC_EventHandlerSet(USB_DEVICE_CDC_INDEX_0,
                        CdcEventHandler, (uintptr_t)&data);

                /* Mark that the device is now configured */
                data.usbReady = true;

            }
            break;

        case USB_DEVICE_EVENT_SUSPENDED:
            /* Switch LED to show suspended state */
            data.ledBlinkSpeed = 0; /* slow */
            break;

        case USB_DEVICE_EVENT_RESUMED:
        case USB_DEVICE_EVENT_ATTACHED:
        case USB_DEVICE_EVENT_DETACHED:
        case USB_DEVICE_EVENT_ERROR:
        default:
            break;
    }
}

/*******************************************************
 * USB CDC Device Events - Application Event Handler
 *******************************************************/

USB_DEVICE_CDC_EVENT_RESPONSE CdcEventHandler
(
    USB_DEVICE_CDC_INDEX index ,
    USB_DEVICE_CDC_EVENT event ,
    void * pData,
    uintptr_t userData
)
{
    APP_DATA *dataPtr;
    dataPtr = (APP_DATA *)userData;
    USB_DEVICE_CDC_EVENT_DATA_READ_COMPLETE * readData;

    switch ( event )
    {
        case USB_DEVICE_CDC_EVENT_GET_LINE_CODING:
            /* This means the host wants to know the current line
             * coding. This is a control transfer request. Use the
             * USB_DEVICE_ControlSend() function to send the data to
             * host.  */
            USB_DEVICE_ControlSend(dataPtr->usbDevice, &dataPtr->lineCoding, sizeof(USB_CDC_LINE_CODING));

            break;

        case USB_DEVICE_CDC_EVENT_SET_LINE_CODING:

            /* This means the host wants to set the line coding.
             * This is a control transfer request. Use the
             * USB_DEVICE_ControlReceive() function to receive the
             * data from the host */

            USB_DEVICE_ControlReceive(dataPtr->usbDevice, &dataPtr->lineCoding, sizeof(USB_CDC_LINE_CODING));
            break;

        case USB_DEVICE_CDC_EVENT_SET_CONTROL_LINE_STATE:

            /* This means the host is setting the control line state.
             * Read the control line state. We will accept this request
             * for now. */

            // Accept, but just ignore this request

            USB_DEVICE_ControlStatus(dataPtr->usbDevice, USB_DEVICE_CONTROL_STATUS_OK);

            break;

        case USB_DEVICE_CDC_EVENT_SEND_BREAK:

            /* This means that the host is requesting that a break of the
             * specified duration be sent. Read the break duration */

            break;

        case USB_DEVICE_CDC_EVENT_CONTROL_TRANSFER_DATA_RECEIVED:
            /* The data stage of the last control transfer is
             * complete. For now we accept all the data */

            USB_DEVICE_ControlStatus(dataPtr->usbDevice, USB_DEVICE_CONTROL_STATUS_OK);
            break;

        case USB_DEVICE_CDC_EVENT_CONTROL_TRANSFER_DATA_SENT:
            /* This means the GET LINE CODING function data is valid. */
            break;

        case USB_DEVICE_CDC_EVENT_READ_COMPLETE:
            readData = pData;

            /* This means that the host has sent some data*/
            dataPtr->usbLastReadBytes = readData->length;
            dataPtr->usbReadState = IO_STATE_COMPLETED;
            break;

        case USB_DEVICE_CDC_EVENT_WRITE_COMPLETE:
            /* This means that the data write got completed. We can schedule
             * the next read. */
            dataPtr->usbWriteState = IO_STATE_COMPLETED;
            break;

        default:
            break;
    }

    return USB_DEVICE_CDC_EVENT_RESPONSE_NONE;
}

// *****************************************************************************
// *****************************************************************************
// Section: AntEye code
// *****************************************************************************
// *****************************************************************************

static uint8_t usartReadBuffer[256];
static uint8_t usartWriteBuffer[] = "Hello World\r\n";

/** Returns true if the specified bus is currently idle */
bool UsartReceiverIdle(uint8_t bus) {
    /* This method is not particularly nice since we're accessing the UART directly without
       using the framework's driver -- but since it doesn't allow us to check it, we have
       no other choice. */
    switch (bus) {
        case 0: return U1STAbits.RIDLE;
        case 1: return U2STAbits.RIDLE;
        case 2: return U3STAbits.RIDLE;
        case 3: return U4STAbits.RIDLE;
        default:
            BSP_FatalError();
    }
}

/** Returns true if the specified receiver has no data available  */
bool UsartReceiverEmpty(uint8_t bus) {
    /* This method is not particularly nice since we're accessing the UART directly without
       using the framework's driver -- but since it doesn't allow us to check it, we have
       no other choice. */
    switch (bus) {
        case 0: return !U1STAbits.URXDA;
        case 1: return !U2STAbits.URXDA;
        case 2: return !U3STAbits.URXDA;
        case 3: return !U4STAbits.URXDA;
        default:
            BSP_FatalError();
    }
}

/** Returns true if the specified tx is currently idle */
bool UsartTransmitterIdle(uint8_t bus) {
    /* This method is not particularly nice since we're accessing the UART directly without
       using the framework's driver -- but since it doesn't allow us to check it, we have
       no other choice. */
    switch (bus) {
        case 0: return U1STAbits.TRMT;
        case 1: return U2STAbits.TRMT;
        case 2: return U3STAbits.TRMT;
        case 3: return U4STAbits.TRMT;
        default:
            BSP_FatalError();
    }
}

/** Waits for this Usart channel to become idle */
void WaitForUsartIdle(uint8_t bus) {
    int idle = 0;
    while (idle < MIN_BUS_IDLE_TICKS*4) {
        if (UsartTransmitterIdle(bus)
                && UsartReceiverIdle(bus)
                && data.usartWriteHandle[bus] == DRV_USART_BUFFER_HANDLE_INVALID
                /*&& data.usartReadHandle[bus] == DRV_USART_BUFFER_HANDLE_INVALID*/)
            idle++;
        else
            idle = 0;
    }
}

void BroadcastUsartCommand(uint8_t * command, uint8_t len) {
    uint8_t bus;
    for (bus = 0; bus < CAM_BUS_COUNT; bus++) {
        // Wait for bus to become idle -- we need to do this as the camera board
        // will ignore any input while it is sending out data ...
        WaitForUsartIdle(bus);
        DRV_USART_BufferAddWrite(data.usartHandle[bus], &data.usartWriteHandle[bus], command, len);
    }

    // Wait for all writes to complete (so that we can reuse the buffer)
    for (bus = 0; bus < CAM_BUS_COUNT; bus++)
        WaitForUsartIdle(bus);

    // Write some extra ticks since our slaves are much slower @ processing data..
    int i;
    for (i = 0; i < 80000; i++)
        ;
}

void OnUSBRead(char* buffer, int read) {
    // Called whenever the PC sends us a command of data

    // Search for commands..
    debugPrint2("USB Read\r\n");
    int i = 0;
    uint8_t j = 0;
    int w;
    uint8_t* uartCmd = genericCameraCommandBuffer;
    uint8_t bus;
    while (i < read) {
        if (buffer[i++] != 'C') {
            debugPrint("--> Illegal command character\r\n");
            continue; // not a command
        }
        int cmd = buffer[i++];
        switch (cmd) {
            default:
                debugPrint("--> Unknown Command\r\n");
                break;
            case 'P':
                debugPrint("USB: Ping!\r\n");
                break;
            case 'R':
                debugPrint("USB: Reset Cameras\r\n");
                for (bus = 0; bus < CAM_BUS_COUNT; bus++) {
                    WaitForUsartIdle(bus);
                    cameraBusState[bus] = BUS_STATE_INIT;
                }
                data.state = APP_STATE_RESET_CAM;

                highSpeedMode = false;

                EraseFrameBuffer();

                // Wait some extra ticks ...
                for (w = 0; w < 10000; w++)
                    ;
                break;

            case 'I': /* I2C Set */
                debugPrint("USB: Change I2C Settings\r\n");
                uartCmd[j++] = 'R'; // Request
                uartCmd[j++] = '@'; // to everyone
                uartCmd[j++] = '!'; // change
                uartCmd[j++] = 'I'; // I2C camera settings
                uartCmd[j++] = buffer[i++]; // addr
                uartCmd[j++] = buffer[i++]; // bitmask
                uartCmd[j++] = buffer[i++]; // bits
                BroadcastUsartCommand(uartCmd, j);
                break;

            case 'H': /* High Speed Mode (30hz) */
                debugPrint("USB: High Speed Mode\r\n");
                uartCmd[j++] = 'R'; // Request
                uartCmd[j++] = '@'; // to everyone
                uartCmd[j++] = '!'; // change
                uartCmd[j++] = 'H'; // High Speed Mode
                BroadcastUsartCommand(uartCmd, j);
                highSpeedMode = true;

                /* Erase the existing framebuffer ... */
                EraseFrameBuffer();
                break;
        }
    }
}


/** Discards the entire receiver buffer */
void UsartDiscardReceiverBuffer(uint8_t bus) {
        switch (bus) {
        case 0: while (U1STAbits.URXDA) { uint8_t valu = U1RXREG; }; break;
        case 1: while (U2STAbits.URXDA) { uint8_t valu = U2RXREG; }; break;
        case 2: while (U3STAbits.URXDA) { uint8_t valu = U3RXREG; }; break;
        case 3: while (U4STAbits.URXDA) { uint8_t valu = U4RXREG; }; break;
        default:
            BSP_FatalError();
    }
}

void UsartEventHandler(DRV_USART_BUFFER_EVENT event,
    DRV_USART_BUFFER_HANDLE bufferHandle,
    uintptr_t context) {
    int bus = (int)context;

    // Clear existing handles
    if (bufferHandle == data.usartWriteHandle[bus]) {
        // TODO: Support multiple buffers here?
        debugPrint3("[TX] ");
        data.usartWriteHandle[bus] = DRV_USART_BUFFER_HANDLE_INVALID;
    } else if (bufferHandle == data.usartReadHandle[bus]) {
        debugPrint3("[RX] ");
        data.usartReadHandle[bus] = DRV_USART_BUFFER_HANDLE_INVALID;
    } else {
        debugPrint("[??] ");
    }

    if (event != DRV_USART_BUFFER_EVENT_COMPLETE)
        debugPrint("[ERR]");
}

void HandleCameraBus() {
    static uint8_t probeIndex[CAM_BUS_COUNT];
    uint8_t* buffer;

    /* Iterate through all camera buses */
    int bus = 0;
    for (bus = 0; bus < CAM_BUS_COUNT; bus++) {
        // Skip disabled buses
        if (camerasOnBus[bus] == 0)
            continue; // no need to do senseless loops through our state machine

        // Wait for transfer to finish before processing any state. Wait for buffered
        // transfer & actual transmitter to finish their work
        if (data.usartWriteHandle[bus] != DRV_USART_BUFFER_HANDLE_INVALID || !UsartTransmitterIdle(bus)) {
            cameraBusIdleTicks[bus] = 0;
            continue;
        }

        // Wait for channel to become idle; give some margin for error
        if (!UsartReceiverIdle(bus)) {
            cameraBusIdleTicks[bus] = 0; // still working
            continue;
        } else {
            if (++cameraBusIdleTicks[bus] < MIN_BUS_IDLE_TICKS)
                continue; // not yet idle long enough
            cameraBusIdleTicks[bus] = 0;
        }

        // Perform a simple state machine that will go through all cameras and
        // request a frame from each camera
        switch (cameraBusState[bus]) {
            // ===============================================================
            // ====================== INITIALIZATION
            // ===============================================================
            default:
            case BUS_STATE_INIT:
                // TODO: Change bus speed here
                debugPrint("Camera Bus INIT\r\n");
                cameraBusState[bus] = BUS_STATE_PROBE_CAMERAS;
                camerasOnBus[bus] = 1;
                probeIndex[bus] = 0;
                break;

            case BUS_STATE_PROBE_CAMERAS:
                // Check if we're already done
                if (probeIndex[bus] >= CAM_ID_COUNT) {
                    camerasOnBus[bus]--; /* we start with +1 here to circumvent the inactive-bus logic */
                    debugPrint("[DONE]\r\n");
                    cameraBusState[bus] = BUS_STATE_START_NEW_FRAME;
                    break;
                } else if (probeIndex[bus] == 0) {
                    // Start probing ...
                    debugPrint("Probing bus for cameras: \r\n");
                }

                // Clear noise
                UsartDiscardReceiverBuffer(bus); // clear residual noise

                // Build a ping command
                char deviceToProbe = CAM_ID_START + probeIndex[bus];
                buffer = cameraCommandBuffers[bus];
                buffer[0] = 'R'; // Request
                buffer[1] = deviceToProbe;
                buffer[2] = 'P'; // Ping
                DRV_USART_BufferAddWrite(data.usartHandle[bus], &data.usartWriteHandle[bus], buffer, 3);

                cameraBusState[bus] = BUS_STATE_PROBE_CAMERAS_RESPONSE;
                break;

            case BUS_STATE_PROBE_CAMERAS_RESPONSE:
                // Check if there was a response
                if (!UsartReceiverEmpty(bus)) {
                    // Yes, we found a new camera!
                    debugPrint("[CAM] ");
                    cameraIDs[bus][camerasOnBus[bus]-1] = CAM_ID_START + probeIndex[bus];
                    camerasOnBus[bus]++;
                    UsartDiscardReceiverBuffer(bus);
                }

                probeIndex[bus]++;
                cameraBusState[bus] = BUS_STATE_PROBE_CAMERAS;
                break;

            // ===============================================================
            // ====================== FRAME PROCESSING
            // ===============================================================
            case BUS_STATE_START_NEW_FRAME:
                debugPrint3("Starting new frame...\r\n");
                cameraBusFrameIterator[bus] = 0;
                cameraBusState[bus] = BUS_STATE_REQUEST_IMAGE_FROM_CAMERA;
                break;
                
            case BUS_STATE_REQUEST_IMAGE_FROM_CAMERA:
                // Check if we're past the number of cameras on this bus ...
                if (cameraBusFrameIterator[bus] >= camerasOnBus[bus]) {
                    cameraBusState[bus] = BUS_STATE_START_NEW_FRAME;
                    switch (bus) {
                        case 0: BSP_LED1 ^= 1; break;
                        case 1: BSP_LED2 ^= 1; break;
                        case 2: BSP_LED3 ^= 1; break;
                        case 3: BSP_LED4 ^= 1; break;
                    }
                    
                    break;
                }

                // Clear residual noise
                UsartDiscardReceiverBuffer(bus); // clear residual noise

                // Calculate frame buffer destination
                int camNum = cameraBusFrameIterator[bus];
                int j;
                for (j = bus-1; j >= 0; j--)
                    camNum += camerasOnBus[j]; // add all previous cams

                // Await the answer first so that DMA can immediately begin
                uint8_t camID = cameraIDs[bus][cameraBusFrameIterator[bus]];
                int frameLen = FRAME_CAM_LEN;
                if (highSpeedMode)
                    frameLen /= 2;
                void * readPtr = frame0 + frameLen*(alignByCameraID ? camID - CAM_ID_START : camNum);
                DRV_USART_BufferAddRead(data.usartHandle[bus], &data.usartReadHandle[bus], readPtr, frameLen);

                // Send a request to the camera
                debugPrint3("Requesting frame...\r\n");
                buffer = cameraCommandBuffers[bus];
                buffer[0] = 'R'; // Request
                buffer[1] = camID;
                buffer[2] = highSpeedMode ? 'H' : 'F'; // Half or Full frame
                DRV_USART_BufferAddWrite(data.usartHandle[bus], &data.usartWriteHandle[bus], buffer, 3);

                cameraBusState[bus] = BUS_STATE_WAIT_FOR_CAMERA_IMAGE;
                cameraBusIdleTicks[bus] = 0;
                break;

            case BUS_STATE_WAIT_FOR_CAMERA_IMAGE:
                /* This frame has been read asynchronously. Since the receiver is idle now, we can
                   assume that it is finished. Cancel the transfer in case one is still active */
                if (data.usartReadHandle[bus] != DRV_USART_BUFFER_HANDLE_INVALID) {
                    // TODO: How to abort a transfer? API doesn't seem to support this ...
                    // We'll just let this transfer finish on the next frame. not nice, but will work
                    debugPrint("!! Got stale read, need to abort. \r\n");
                    data.usartReadHandle[bus] = DRV_USART_BUFFER_HANDLE_INVALID;
                }
                debugPrint3("Reading camera finished -> next cam\r\n");
                cameraBusFrameIterator[bus]++; // advance to next camera
                cameraBusState[bus] = BUS_STATE_REQUEST_IMAGE_FROM_CAMERA;
                break;
        }
    }
}

// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine
// *****************************************************************************
// *****************************************************************************

/******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */

void APP_Initialize ( void )
{
    /* TODO: Initialize your application's state machine and other
     * parameters.
     */

    /* Place the App state machine in its initial state. */
    data.state = APP_STATE_INIT;
    data.frameBuffer = &framebuffer[0];

    EraseFrameBuffer();
}

void debugPrint(const char* str) {
    // Write the data (assume that there is no overflow ..)
    DRV_USART_BUFFER_HANDLE write;
    DRV_USART_BufferAddWrite(data.usartDebugHandle, &write, (void*)str, strlen(str));
}

/**********************************************************
 * Application tasks routine. This function implements the
 * application state machine.
 ***********************************************************/

void ApplicationInitWaitCallback() {
    data.state = APP_STATE_RESET_CAM;
}
void ApplicationInitWaitCallback2() {
    data.state = APP_STATE_SETUP_UART;
}

void BaudRateChangeCallback() {
    // Update UARTs
    uint8_t i;
    for (i = 0; i < CAM_BUS_COUNT; i++)
        DRV_USART_BaudSet(data.usartHandle[i], CAM_BUS_SPEED);

    debugPrint("UART Speeds set \r\n");
    data.state = APP_STATE_RUNNING;
}

void APP_Tasks ( void )
{
    /* check the application state*/
    int i = 0;
    uint8_t * buffer = genericCameraCommandBuffer;
    uint8_t bus;
    switch ( data.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
            // Wait for all modules to become operational first
            if (SYS_TMR_Status(sys.tmrSys) != SYS_STATUS_READY)  break;
            if (DRV_USART_Status(sys.usartDrv0) != SYS_STATUS_READY) break;
            if (DRV_USART_Status(sys.usartDrv1) != SYS_STATUS_READY) break;
            if (DRV_USART_Status(sys.usartDrv2) != SYS_STATUS_READY) break;
            if (DRV_USART_Status(sys.usartDrv3) != SYS_STATUS_READY) break;
            if (DRV_USART_Status(sys.usartDrvDebug) != SYS_STATUS_READY) break;
            if (USB_DEVICE_Status(sys.usbDev) != SYS_STATUS_READY) break;

            /* Open the USART modules */
            data.usartHandle[0] = DRV_USART_Open(DRV_USART_INDEX_0, DRV_IO_INTENT_READWRITE | DRV_IO_INTENT_NONBLOCKING);
            data.usartHandle[1] = DRV_USART_Open(DRV_USART_INDEX_1, DRV_IO_INTENT_READWRITE | DRV_IO_INTENT_NONBLOCKING);
            data.usartHandle[2] = DRV_USART_Open(DRV_USART_INDEX_2, DRV_IO_INTENT_READWRITE | DRV_IO_INTENT_NONBLOCKING);
            data.usartHandle[3] = DRV_USART_Open(DRV_USART_INDEX_3, DRV_IO_INTENT_READWRITE | DRV_IO_INTENT_NONBLOCKING);
            data.usartDebugHandle = DRV_USART_Open(DRV_USART_INDEX_4, DRV_IO_INTENT_WRITE | DRV_IO_INTENT_NONBLOCKING);
            if (data.usartHandle[0] == DRV_HANDLE_INVALID || data.usartHandle[1] == DRV_HANDLE_INVALID ||
                data.usartHandle[2] == DRV_HANDLE_INVALID || data.usartHandle[3] == DRV_HANDLE_INVALID ||
                data.usartDebugHandle == DRV_HANDLE_INVALID)
                BSP_FatalError();

            // Print some info on the debug port
            debugPrint("\r\n\r\n**** Compound Ant-Eye ****\r\n (c) 2014 Matthias Linder \r\n  University of Edinburgh \r\n==========================\r\n");

            // Register event handlers for read completions
            DRV_USART_BufferEventHandlerSet(data.usartHandle[0], UsartEventHandler, 0);
            DRV_USART_BufferEventHandlerSet(data.usartHandle[1], UsartEventHandler, 1);
            DRV_USART_BufferEventHandlerSet(data.usartHandle[2], UsartEventHandler, 2);
            DRV_USART_BufferEventHandlerSet(data.usartHandle[3], UsartEventHandler, 3);

            // Initiate handles
            data.usartWriteHandle[0] = DRV_USART_BUFFER_HANDLE_INVALID;
            data.usartWriteHandle[1] = DRV_USART_BUFFER_HANDLE_INVALID;
            data.usartWriteHandle[2] = DRV_USART_BUFFER_HANDLE_INVALID;
            data.usartWriteHandle[3] = DRV_USART_BUFFER_HANDLE_INVALID;
            data.usartReadHandle[0] = DRV_USART_BUFFER_HANDLE_INVALID;
            data.usartReadHandle[1] = DRV_USART_BUFFER_HANDLE_INVALID;
            data.usartReadHandle[2] = DRV_USART_BUFFER_HANDLE_INVALID;
            data.usartReadHandle[3] = DRV_USART_BUFFER_HANDLE_INVALID;
            
            /* Open the USB Device */
            data.usbDevice = USB_DEVICE_Open(USB_DEVICE_INDEX_0, DRV_IO_INTENT_READWRITE);
            if (data.usbDevice == USB_DEVICE_HANDLE_INVALID)
                BSP_FatalError();

             /* Attach the device to the PC */
            USB_DEVICE_Attach(data.usbDevice);

            /* Register an event callback */
            USB_DEVICE_EventHandlerSet(data.usbDevice, UsbDeviceEventHandler, 0);

            // Setup Coretimer
            WriteCoreTimer(0);

            // Give the attached cameras some ms to settle
            SYS_TMR_CallbackSingle(200*TIMER_BUG_FIX, ApplicationInitWaitCallback);
            data.state = APP_STATE_WAITING;
            break;

        case APP_STATE_WAITING:
            /* NOP */
            break;

        case APP_STATE_RESET_CAM:
            // Send the reset packet
            debugPrint("Resetting Cameras\r\n");

            buffer[i++] = 'R'; // Request
            buffer[i++] = '@'; // to everyone
            buffer[i++] = '!'; // change
            buffer[i++] = 'R'; // reset

            for (bus = 0; bus < CAM_BUS_COUNT; bus++)
                DRV_USART_BufferAddWrite(data.usartHandle[bus], &data.usartWriteHandle[bus], buffer, i);


            // Give the attached cameras some ms to settle
            SYS_TMR_CallbackSingle(300*TIMER_BUG_FIX, ApplicationInitWaitCallback2);
            data.state = APP_STATE_WAITING;
            break;

        case APP_STATE_SETUP_UART:
            // Set debug settings ..
            if (BSP_INPUT3) {
                buffer[i++] = 'R'; // Request
                buffer[i++] = '@'; // to everyone
                buffer[i++] = '!'; // change
                buffer[i++] = 'I'; // I2C camera settings
                buffer[i++] = 0x71; // addr = SCALING_YSC
                buffer[i++] = (1 << 7); // affect 7th bit
                buffer[i++] = 0xFF; // enable color pattern
            }

            // Change bus speed
            if (CAM_BUS_INIT_SPEED != CAM_BUS_SPEED) {
                buffer[i++] = 'R'; // Request
                buffer[i++] = '@'; // to everyone
                buffer[i++] = '!'; // change
                buffer[i++] = 'U'; // uart baudrate
                int newBaud = CAM_BUS_SPEED;
                buffer[i++] = (newBaud >> 0) & 0xFF;
                buffer[i++] = (newBaud >> 8) & 0xFF;
                buffer[i++] = (newBaud >> 16) & 0xFF;
                buffer[i++] = (newBaud >> 24) & 0xFF;
            }

            if (i > 0) {
                debugPrint("Setting up Camera & Changing Camera Bus Speed...\r\n");
                
                for (bus = 0; bus < CAM_BUS_COUNT; bus++) {
                    DRV_USART_BufferAddWrite(data.usartHandle[bus], &data.usartWriteHandle[bus], buffer, i);
                    WaitForUsartIdle(bus);
                }

                SYS_TMR_CallbackSingle(100*TIMER_BUG_FIX, BaudRateChangeCallback);
                data.state = APP_STATE_WAITING;
            } else
                data.state = APP_STATE_RUNNING;
            break;

        case APP_STATE_RUNNING:
            // Update LED
            if (data.ledBlinkSpeed != -1)
                BSP_LED0 = (ReadCoreTimer() >> (27 - data.ledBlinkSpeed)) & 1;

            HandleCameraBus();

            // Check the USB CDC data state (can we send more data?)
            // We continuously stream frames as fast as we can on the USB bus
            // and kind of assume that the internal frame buffer gets updated via
            // the usarts.
            if (data.usbReady) {
                // Check if we can schedule a new write
                if (data.usbWriteState != IO_STATE_SCHEDULED) {
                    // Send an entire frame to the PC
                    data.usbWriteState = IO_STATE_SCHEDULED;
                    USB_DEVICE_CDC_TRANSFER_HANDLE writeHandle = USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID;
                    int bufferLen;
                    if (highSpeedMode) {
                        bufferLen = FRAME_BUFFER_HALF_LEN;

                        // Set earlier EOF marker
                        data.frameBuffer[bufferLen-3] = 'E';
                        data.frameBuffer[bufferLen-2] = 'O';
                        data.frameBuffer[bufferLen-1] = 'F';
                    } else bufferLen = FRAME_BUFFER_LEN;
                    USB_DEVICE_CDC_Write(USB_DEVICE_CDC_INDEX_0, &writeHandle, data.frameBuffer, bufferLen, USB_DEVICE_CDC_TRANSFER_FLAGS_DATA_COMPLETE);
                    if (writeHandle == USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID)
                        BSP_FatalError();
                }

                // Check if we have a completed read that we can proces
                if (data.usbReadState == IO_STATE_COMPLETED) {
                    OnUSBRead(&data.usbReadBuffer, data.usbLastReadBytes);
                    data.usbReadState = IO_STATE_IDLE;
                }

                // Check if we can schedule another read
                if (data.usbReadState == IO_STATE_IDLE) {
                    data.usbReadState = IO_STATE_SCHEDULED;
                    USB_DEVICE_CDC_TRANSFER_HANDLE readHandle = USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID;
                    USB_DEVICE_CDC_Read(USB_DEVICE_CDC_INDEX_0, &readHandle, &data.usbReadBuffer, USB_DEVICE_EP0_BUFFER_SIZE);
                     if (readHandle == USB_DEVICE_CDC_TRANSFER_HANDLE_INVALID)
                        BSP_FatalError();
                }
            }
            break;

        /* The default state should never be executed. */
        default:
            BSP_FatalError();
            break;

    }   
} 

/*******************************************************************************
 End of File
 */

