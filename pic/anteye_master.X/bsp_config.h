/* 
 * File:   bspconfig.h
 * Author: Matthias
 *
 * Created on July 5, 2014, 12:58 PM
 */

#ifndef BSPCONFIG_H
#define	BSPCONFIG_H

#define BSP_LED0 LATBbits.LATB3
#define BSP_LED1 LATBbits.LATB1
#define BSP_LED2 LATBbits.LATB4
#define BSP_LED3 LATBbits.LATB0
#define BSP_LED4 LATBbits.LATB2

#define BSP_INPUT1 !PORTEbits.RE5
#define BSP_INPUT2 !PORTEbits.RE6
#define BSP_INPUT3 !PORTEbits.RE7

/** Initializes the board */
void BSP_Initialize();

/** Uses the board to signal that a fatal, non-recoverable error occured */
void BSP_FatalError();

#endif	/* BSPCONFIG_H */


