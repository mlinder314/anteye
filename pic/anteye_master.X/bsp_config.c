/***************************************/
/*** Board Specific Configuration File */
/***************************************/
#include "app.h"

/** Initializes the board */
void BSP_Initialize() {
    // Set LEDs as Outputs
    TRISBbits.TRISB0 = 0;
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    TRISBbits.TRISB4 = 0;

    // Disable all LEDs by default
    BSP_LED0 = 0;
    BSP_LED1 = 0;
    BSP_LED2 = 0;
    BSP_LED3 = 0;
    BSP_LED4 = 0;

    // Setup inputs.. (dip switches)
    TRISEbits.TRISE5 = 1;
    TRISEbits.TRISE6 = 1;
    TRISEbits.TRISE7 = 1;

    AD1PCFG = -1;
}

/** Uses the board to signal that a fatal, non-recoverable error occured */
void BSP_FatalError() {
    // Flash both LEDs alternating
    BSP_LED0 = 1;
    BSP_LED1 = 0;
    while (1) {
        BSP_LED0 ^= 1;
        BSP_LED1 ^= 1;
        int i = 0;
        for (i = 0; i < 8000000; i++) ; /* pulsing */
    }
}
