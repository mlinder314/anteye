/*******************************************************************************
 System Initialization File

  Company:
    Microchip Technology Inc.
  
  File Name:
    system_init.c

  Summary:
    System Initialization.

  Description:
    This file contains source code necessary to initialize the system.
 *******************************************************************************/

// DOM-IGNORE-BEGIN
/*******************************************************************************
Copyright (c) 2013 released Microchip Technology Inc.  All rights reserved.

Microchip licenses to you the right to use, modify, copy and distribute
Software only when embedded on a Microchip microcontroller or digital signal
controller that is integrated into your product or third party product
(pursuant to the sublicense terms in the accompanying license agreement).

You should refer to the license agreement accompanying this Software for
additional information regarding your rights and obligations.

SOFTWARE AND DOCUMENTATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
(INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.
 *******************************************************************************/
// DOM-IGNORE-END

#include "app.h"
#include "clk/sys_clk.h"
#include "tmr/drv_tmr.h"
#include "dma/sys_dma.h"
#include "usart/drv_usart.h"

// ****************************************************************************
// ****************************************************************************
// Section: Configuration Bits
// ****************************************************************************
// ****************************************************************************

// DEVCFG3
// USERID = No Setting
#pragma config FSRSSEL = PRIORITY_7     // SRS Select (SRS Priority 7)
#pragma config FMIIEN = ON              // Ethernet RMII/MII Enable (MII Enabled)
#pragma config FETHIO = ON              // Ethernet I/O Pin Select (Default Ethernet I/O)
#pragma config FUSBIDIO = OFF           // USB USID Selection (Controlled by Port Function)
#pragma config FVBUSONIO = ON           // USB VBUS ON Selection (Controlled by USB Module)

// DEVCFG2 (configured for a 16 MHz external crystal; setup 80 MHz base clock)
#pragma config FPLLIDIV = DIV_4         // PLL Input Divider (4x Divider)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier) [ CAN ALSO RUN THIS AT 96 MHz or so ]
#pragma config UPLLIDIV = DIV_4         // USB PLL Input Divider (4x Divider)
#pragma config UPLLEN = ON              // USB PLL Enable (Enabled)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)

// DEVCFG1 (use external crystal only)
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = HS             // Primary Oscillator Configuration (HS osc mode)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_1           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/1)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))

// DEVCFG0
#pragma config DEBUG = OFF              // Background Debugger Enable (Debugger is disabled)
#pragma config ICESEL = ICS_PGx2        // ICE/ICD Comm Channel Select (ICE EMUC2/EMUD2 pins shared with PGC2/PGD2)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)


// *****************************************************************************
// *****************************************************************************
// Section: Driver Initialization Data
// *****************************************************************************
// *****************************************************************************


/*************************************************
 * Timer Driver Initialization
 ************************************************/

DRV_TMR_INIT drvTmrInit =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .tmrId            = TMR_ID_2,
    .clockSource      = TMR_CLOCK_SOURCE_PERIPHERAL_CLOCK,
    .syncMode         = DRV_TMR_SYNC_MODE_SYNCHRONOUS_INTERNAL,
    .prescale         = TMR_PRESCALE_VALUE_256, /* this won't work, and will set it to 65536 instead */
    .combineTimers    = false,
    .interruptSource  = INT_SOURCE_TIMER_2,
};


// ****************************************************************************
// ****************************************************************************
// Section: System Initialization
// ****************************************************************************
// ****************************************************************************

/*************************************************
 * System Timer Service Initialization
 ************************************************/

SYS_TMR_INIT sysTmrInit =
{
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .drvIndex = 0,
    .alarmPeriodMs = 10*TIMER_BUG_FIX,
};

/*************************************************
 * System Devcon Service Initialization
 ************************************************/

SYS_DEVCON_INIT sysDevconInit =
{
    .moduleInit = {0},
};


/*************************************************
 * System DMA Initialization
 ************************************************/

SYS_DMA_INIT sysDmaInit =
{
    .sidl = SYS_DMA_SIDL_DISABLE, /* continue in idle mode*/
};

/*************************************************
 * System USART Initialization
 ************************************************/

DRV_USART_INIT drvUsartInit0 = /* this uses UART 1 */
{
    .usartID = USART_ID_1,
    .mode = DRV_USART_OPERATION_MODE_NORMAL,
    .baud = CAM_BUS_INIT_SPEED,
    .lineControl = DRV_USART_LINE_CONTROL_8NONE1,
    .flags = DRV_USART_INIT_FLAG_NONE,
    .handshake = DRV_USART_HANDSHAKE_NONE,
    .interruptError = INT_SOURCE_USART_1_ERROR,
    .interruptReceive = INT_SOURCE_USART_1_RECEIVE,
    .interruptTransmit = INT_SOURCE_USART_1_TRANSMIT,
    .brgClock = SYS_PER_CLOCK,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .queueSizeReceive = 1, /* setting this to 1 helps with wrongly sized frames due to auto-discard */
    .queueSizeTransmit = 2,
    .dmaChannelReceive = DMA_CHANNEL_0,
    .dmaInterruptReceive = INT_SOURCE_DMA_0,
    //.dmaChannelReceive = DMA_CHANNEL_NONE,
    .dmaChannelTransmit = DMA_CHANNEL_NONE, // not needed since low speed
};

DRV_USART_INIT drvUsartInit1 = /* this uses UART 2 */
{
    .usartID = USART_ID_2,
    .mode = DRV_USART_OPERATION_MODE_NORMAL,
    .baud = CAM_BUS_INIT_SPEED,
    .lineControl = DRV_USART_LINE_CONTROL_8NONE1,
    .flags = DRV_USART_INIT_FLAG_NONE,
    .handshake = DRV_USART_HANDSHAKE_NONE,
    .interruptError = INT_SOURCE_USART_2_ERROR,
    .interruptReceive = INT_SOURCE_USART_2_RECEIVE,
    .interruptTransmit = INT_SOURCE_USART_2_TRANSMIT,
    .brgClock = SYS_PER_CLOCK,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .queueSizeReceive = 1,
    .queueSizeTransmit = 2,
    .dmaChannelReceive = DMA_CHANNEL_1,
    .dmaInterruptReceive = INT_SOURCE_DMA_1,
    .dmaChannelTransmit = DMA_CHANNEL_NONE, // not needed since low speed
};

DRV_USART_INIT drvUsartInit2 = /* this uses UART 3 */
{
    .usartID = USART_ID_3,
    .mode = DRV_USART_OPERATION_MODE_NORMAL,
    .baud = CAM_BUS_INIT_SPEED,
    .lineControl = DRV_USART_LINE_CONTROL_8NONE1,
    .flags = DRV_USART_INIT_FLAG_NONE,
    .handshake = DRV_USART_HANDSHAKE_NONE,
    .interruptError = INT_SOURCE_USART_3_ERROR,
    .interruptReceive = INT_SOURCE_USART_3_RECEIVE,
    .interruptTransmit = INT_SOURCE_USART_3_TRANSMIT,
    .brgClock = SYS_PER_CLOCK,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .queueSizeReceive = 1,
    .queueSizeTransmit = 2,
    .dmaChannelReceive = DMA_CHANNEL_2,
    .dmaInterruptReceive = INT_SOURCE_DMA_2,
    .dmaChannelTransmit = DMA_CHANNEL_NONE, // not needed since low speed
};

DRV_USART_INIT drvUsartInit3 = /* this uses UART 4 */
{
    .usartID = USART_ID_4,
    .mode = DRV_USART_OPERATION_MODE_NORMAL,
    .baud = CAM_BUS_INIT_SPEED,
    .lineControl = DRV_USART_LINE_CONTROL_8NONE1,
    .flags = DRV_USART_INIT_FLAG_NONE,
    .handshake = DRV_USART_HANDSHAKE_NONE,
    .interruptError = INT_SOURCE_USART_4_ERROR,
    .interruptReceive = INT_SOURCE_USART_4_RECEIVE,
    .interruptTransmit = INT_SOURCE_USART_4_TRANSMIT,
    .brgClock = SYS_PER_CLOCK,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .queueSizeReceive = 1,
    .queueSizeTransmit = 2,
    .dmaChannelReceive = DMA_CHANNEL_3,
    .dmaInterruptReceive = INT_SOURCE_DMA_3,
    .dmaChannelTransmit = DMA_CHANNEL_NONE, // not needed since low speed
};

// ----------------------

DRV_USART_INIT drvUsartInitDebug = /* this uses UART 5 */
{
    .usartID = USART_ID_5,
    .mode = DRV_USART_OPERATION_MODE_NORMAL,
    .baud = 250000,
    .lineControl = DRV_USART_LINE_CONTROL_8NONE1,
    .flags = DRV_USART_INIT_FLAG_NONE,
    .handshake = DRV_USART_HANDSHAKE_NONE,
    .interruptError = INT_SOURCE_USART_5_ERROR,
    .interruptReceive = INT_SOURCE_USART_5_RECEIVE,
    .interruptTransmit = INT_SOURCE_USART_5_TRANSMIT,
    .brgClock = SYS_PER_CLOCK,
    .moduleInit.value = SYS_MODULE_POWER_RUN_FULL,
    .queueSizeReceive = 0,
    .queueSizeTransmit = 40, /* pretty much a write-only endpoint */
    .dmaChannelReceive = DMA_CHANNEL_NONE,
    .dmaChannelTransmit = DMA_CHANNEL_NONE, // not needed since low speed
};

/*************************************************
 * USB Device Initialization
 ************************************************/
uint8_t __attribute__((aligned(512))) endpointTable[USB_DEVICE_ENDPOINT_TABLE_SIZE];
        
USB_DEVICE_INIT devUsbInit =
{
    /* System module initialization */
    .moduleInit = {SYS_MODULE_POWER_RUN_FULL},

    /* Identifies peripheral (PLIB-level) ID */
    .usbID = USB_ID_1,

    /* stop in idle */
    .stopInIdle = false,

    /* suspend in sleep */
    .suspendInSleep = false,

    /* Endpoint table */
    .endpointTable = endpointTable,

    /* Interrupt Source for USB module */
    .interruptSource = INT_SOURCE_USB_1,

    /* Number of function drivers registered to this instance of the
       USB device layer */
    .registeredFuncCount = 1,

    /* Function driver table registered to this instance of the USB device layer*/
    .registeredFunctions = (USB_DEVICE_FUNCTION_REGISTRATION_TABLE*)funcRegistrationTable,

    /* Pointer to USB Descriptor structure */
    .usbMasterDescriptor = (USB_DEVICE_MASTER_DESCRIPTOR*)&usbMasterDescriptor,

    /* USB Device Speed */
    .deviceSpeed = USB_SPEED_FULL
};
  
/*******************************************************************************
  Function:
    void SYS_Initialize ( SYS_INIT_DATA *data )

  Summary:
    Initializes the board, services, drivers, application and other modules

  Description:
    This routine initializes the board, services, drivers, application and other
    modules as configured at build time.  In a bare-metal environment (where no
    OS is supported), this routine should be called almost immediately after
    entering the "main" routine.

  Precondition:
    The C-language run-time environment and stack must have been initialized.

  Parameters:
    data        - Pointer to the system initialization data structure containing
                  pointers to the board, system service, and driver
                  initialization routines
  Returns:
    None.

  Example:
    <code>
    SYS_INT_Initialize(NULL);
    </code>

  Remarks:
    Basic System Initialization Sequence:

    1.  Initialize minimal board services and processor-specific items
        (enough to use the board to initialize drivers and services)
    2.  Initialize all supported system services
    3.  Initialize all supported modules
        (libraries, drivers, middleware, and application-level modules)
    4.  Initialize the main (static) application, if present.

    The order in which services and modules are initialized and started may be
    important.

    For a static system (a system not using the Harmony's dynamic implementation
    of the initialization and "Tasks" services) this routine is implemented
    for the specific configuration of an application.
 */



void SYS_Initialize ( void* data )
{
    /* Initialize the BSP */
    BSP_Initialize ( );

    /* Initialize The base system clock */
    SYS_CLK_Initialize(NULL);
    if (SYS_CLK_SystemFrequencyGet() != SYS_PER_CLOCK) {
        // Something is wrong ...
        BSP_FatalError();
    }

    // TODO: Add clock switching here

    /* Initialize Interrupts */
    SYS_INT_Initialize();

    /* Initialize the timer driver */
    sys.tmrDrv = DRV_TMR_Initialize(DRV_TMR_INDEX_0, (SYS_MODULE_INIT *) &drvTmrInit);
    if (DRV_TMR_Status(sys.tmrDrv) != SYS_STATUS_READY) {
        BSP_FatalError();
    }

    /* Initialize the timer system module */
    sys.tmrSys = SYS_TMR_Initialize(SYS_TMR_INDEX_0, (SYS_MODULE_INIT *) &sysTmrInit);
    if (sys.tmrSys == SYS_MODULE_OBJ_INVALID) {
        BSP_FatalError();
    }
    SYS_INT_VectorPrioritySet(INT_VECTOR_T2, INT_PRIORITY_LEVEL3);

    /* Optimize System Performance; Enable prefetch cache */
    SYS_DEVCON_Initialize(SYS_DEVCON_INDEX_0, (SYS_MODULE_INIT *) &sysDevconInit);
    SYS_DEVCON_PerformanceConfig(SYS_CLK_SystemFrequencyGet());

    /* Setup the DMA module */
    sys.dmaSys = SYS_DMA_Initialize((SYS_MODULE_INIT *) &sysDmaInit);
    if (sys.dmaSys == SYS_MODULE_OBJ_INVALID)
        BSP_FatalError();
    SYS_INT_VectorPrioritySet(INT_VECTOR_DMA0, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_DMA1, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_DMA2, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_DMA3, INT_PRIORITY_LEVEL3);

    /* Setup all USART channels */
    sys.usartDrv0 = DRV_USART_Initialize(DRV_USART_INDEX_0,  (SYS_MODULE_INIT *) &drvUsartInit0);
    sys.usartDrv1 = DRV_USART_Initialize(DRV_USART_INDEX_1,  (SYS_MODULE_INIT *) &drvUsartInit1);
    sys.usartDrv2 = DRV_USART_Initialize(DRV_USART_INDEX_2,  (SYS_MODULE_INIT *) &drvUsartInit2);
    sys.usartDrv3 = DRV_USART_Initialize(DRV_USART_INDEX_3,  (SYS_MODULE_INIT *) &drvUsartInit3);
    sys.usartDrvDebug = DRV_USART_Initialize(DRV_USART_INDEX_4,  (SYS_MODULE_INIT *) &drvUsartInitDebug);
    if (sys.usartDrv0 == SYS_MODULE_OBJ_INVALID || sys.usartDrv1 == SYS_MODULE_OBJ_INVALID ||
        sys.usartDrv2 == SYS_MODULE_OBJ_INVALID || sys.usartDrv3 == SYS_MODULE_OBJ_INVALID ||
        sys.usartDrvDebug == SYS_MODULE_OBJ_INVALID) {
        BSP_FatalError();
    }
    SYS_INT_VectorPrioritySet(INT_VECTOR_UART1, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_UART2, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_UART3, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_UART4, INT_PRIORITY_LEVEL3);
    SYS_INT_VectorPrioritySet(INT_VECTOR_UART5, INT_PRIORITY_LEVEL5); /* dbg shouldn't hang */

    CNPUEbits.CNPUE17 = 1; // Enable UART 2 pullup
    CNPUEbits.CNPUE9 = 1; // Enable UART 3 pullup
    // other pins have hardware pullups

    /* Initialize the USB connection */
    sys.usbDev = USB_DEVICE_Initialize(USB_DEVICE_INDEX_0, (SYS_MODULE_INIT *) &devUsbInit);
    if (sys.usbDev == SYS_MODULE_OBJ_INVALID) {
        BSP_FatalError();
    }
    SYS_INT_VectorPrioritySet(INT_VECTOR_USB, INT_PRIORITY_LEVEL4); /* usb is sensitive */

    /* Enable interrupts */
    SYS_INT_Enable();
    
    /* Initialize the Application */
    APP_Initialize();    
}


/*******************************************************************************/
/*******************************************************************************
 End of File
*/
