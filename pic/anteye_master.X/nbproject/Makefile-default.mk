#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=F:/MSc/pic/anteye_master.X/system_config.c F:/MSc/pic/anteye_master.X/system_init.c F:/MSc/pic/anteye_master.X/system_interrupt.c F:/MSc/pic/anteye_master.X/system_tasks.c F:/MSc/pic/anteye_master.X/main.c F:/MSc/pic/anteye_master.X/app.c bsp_config.c C:/microchip/harmony/v0_80_02b/framework/driver/tmr/src/dynamic/drv_tmr_dynamic.c C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_buffer_queue_dma.c C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_dma.c C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb.c C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb_device.c C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk.c C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk_pic32mx.c C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon.c C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon_pic32mx.c C:/microchip/harmony/v0_80_02b/framework/system/dma/src/sys_dma.c C:/microchip/harmony/v0_80_02b/framework/system/int/src/sys_int_pic32.c C:/microchip/harmony/v0_80_02b/framework/system/ports/src/sys_ports.c C:/microchip/harmony/v0_80_02b/framework/system/tmr/src/sys_tmr.c C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device.c C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc.c C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc_acm.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/12832417/system_config.o ${OBJECTDIR}/_ext/12832417/system_init.o ${OBJECTDIR}/_ext/12832417/system_interrupt.o ${OBJECTDIR}/_ext/12832417/system_tasks.o ${OBJECTDIR}/_ext/12832417/main.o ${OBJECTDIR}/_ext/12832417/app.o ${OBJECTDIR}/bsp_config.o ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o ${OBJECTDIR}/_ext/203677798/drv_usb.o ${OBJECTDIR}/_ext/203677798/drv_usb_device.o ${OBJECTDIR}/_ext/1138024653/sys_clk.o ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o ${OBJECTDIR}/_ext/1231681268/sys_devcon.o ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o ${OBJECTDIR}/_ext/2044922275/sys_dma.o ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o ${OBJECTDIR}/_ext/1227416125/sys_ports.o ${OBJECTDIR}/_ext/919188156/sys_tmr.o ${OBJECTDIR}/_ext/401753235/usb_device.o ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/12832417/system_config.o.d ${OBJECTDIR}/_ext/12832417/system_init.o.d ${OBJECTDIR}/_ext/12832417/system_interrupt.o.d ${OBJECTDIR}/_ext/12832417/system_tasks.o.d ${OBJECTDIR}/_ext/12832417/main.o.d ${OBJECTDIR}/_ext/12832417/app.o.d ${OBJECTDIR}/bsp_config.o.d ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o.d ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o.d ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o.d ${OBJECTDIR}/_ext/203677798/drv_usb.o.d ${OBJECTDIR}/_ext/203677798/drv_usb_device.o.d ${OBJECTDIR}/_ext/1138024653/sys_clk.o.d ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o.d ${OBJECTDIR}/_ext/1231681268/sys_devcon.o.d ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o.d ${OBJECTDIR}/_ext/2044922275/sys_dma.o.d ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o.d ${OBJECTDIR}/_ext/1227416125/sys_ports.o.d ${OBJECTDIR}/_ext/919188156/sys_tmr.o.d ${OBJECTDIR}/_ext/401753235/usb_device.o.d ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o.d ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/12832417/system_config.o ${OBJECTDIR}/_ext/12832417/system_init.o ${OBJECTDIR}/_ext/12832417/system_interrupt.o ${OBJECTDIR}/_ext/12832417/system_tasks.o ${OBJECTDIR}/_ext/12832417/main.o ${OBJECTDIR}/_ext/12832417/app.o ${OBJECTDIR}/bsp_config.o ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o ${OBJECTDIR}/_ext/203677798/drv_usb.o ${OBJECTDIR}/_ext/203677798/drv_usb_device.o ${OBJECTDIR}/_ext/1138024653/sys_clk.o ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o ${OBJECTDIR}/_ext/1231681268/sys_devcon.o ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o ${OBJECTDIR}/_ext/2044922275/sys_dma.o ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o ${OBJECTDIR}/_ext/1227416125/sys_ports.o ${OBJECTDIR}/_ext/919188156/sys_tmr.o ${OBJECTDIR}/_ext/401753235/usb_device.o ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o

# Source Files
SOURCEFILES=F:/MSc/pic/anteye_master.X/system_config.c F:/MSc/pic/anteye_master.X/system_init.c F:/MSc/pic/anteye_master.X/system_interrupt.c F:/MSc/pic/anteye_master.X/system_tasks.c F:/MSc/pic/anteye_master.X/main.c F:/MSc/pic/anteye_master.X/app.c bsp_config.c C:/microchip/harmony/v0_80_02b/framework/driver/tmr/src/dynamic/drv_tmr_dynamic.c C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_buffer_queue_dma.c C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_dma.c C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb.c C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb_device.c C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk.c C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk_pic32mx.c C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon.c C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon_pic32mx.c C:/microchip/harmony/v0_80_02b/framework/system/dma/src/sys_dma.c C:/microchip/harmony/v0_80_02b/framework/system/int/src/sys_int_pic32.c C:/microchip/harmony/v0_80_02b/framework/system/ports/src/sys_ports.c C:/microchip/harmony/v0_80_02b/framework/system/tmr/src/sys_tmr.c C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device.c C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc.c C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc_acm.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
	${MAKE} ${MAKE_OPTIONS} -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX675F256H
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/12832417/system_config.o: F:/MSc/pic/anteye_master.X/system_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_config.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_config.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_config.o.d" -o ${OBJECTDIR}/_ext/12832417/system_config.o F:/MSc/pic/anteye_master.X/system_config.c   
	
${OBJECTDIR}/_ext/12832417/system_init.o: F:/MSc/pic/anteye_master.X/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_init.o.d" -o ${OBJECTDIR}/_ext/12832417/system_init.o F:/MSc/pic/anteye_master.X/system_init.c   
	
${OBJECTDIR}/_ext/12832417/system_interrupt.o: F:/MSc/pic/anteye_master.X/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/12832417/system_interrupt.o F:/MSc/pic/anteye_master.X/system_interrupt.c   
	
${OBJECTDIR}/_ext/12832417/system_tasks.o: F:/MSc/pic/anteye_master.X/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_tasks.o.d" -o ${OBJECTDIR}/_ext/12832417/system_tasks.o F:/MSc/pic/anteye_master.X/system_tasks.c   
	
${OBJECTDIR}/_ext/12832417/main.o: F:/MSc/pic/anteye_master.X/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/main.o.d" -o ${OBJECTDIR}/_ext/12832417/main.o F:/MSc/pic/anteye_master.X/main.c   
	
${OBJECTDIR}/_ext/12832417/app.o: F:/MSc/pic/anteye_master.X/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/app.o.d" -o ${OBJECTDIR}/_ext/12832417/app.o F:/MSc/pic/anteye_master.X/app.c   
	
${OBJECTDIR}/bsp_config.o: bsp_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/bsp_config.o.d 
	@${RM} ${OBJECTDIR}/bsp_config.o 
	@${FIXDEPS} "${OBJECTDIR}/bsp_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/bsp_config.o.d" -o ${OBJECTDIR}/bsp_config.o bsp_config.c   
	
${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o: C:/microchip/harmony/v0_80_02b/framework/driver/tmr/src/dynamic/drv_tmr_dynamic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1542292883 
	@${RM} ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o.d" -o ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o C:/microchip/harmony/v0_80_02b/framework/driver/tmr/src/dynamic/drv_tmr_dynamic.c   
	
${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o: C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_buffer_queue_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1310334617 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o.d" -o ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_buffer_queue_dma.c   
	
${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o: C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1310334617 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o.d" -o ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_dma.c   
	
${OBJECTDIR}/_ext/203677798/drv_usb.o: C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/203677798 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb.o.d 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/203677798/drv_usb.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/203677798/drv_usb.o.d" -o ${OBJECTDIR}/_ext/203677798/drv_usb.o C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb.c   
	
${OBJECTDIR}/_ext/203677798/drv_usb_device.o: C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/203677798 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/203677798/drv_usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/203677798/drv_usb_device.o.d" -o ${OBJECTDIR}/_ext/203677798/drv_usb_device.o C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb_device.c   
	
${OBJECTDIR}/_ext/1138024653/sys_clk.o: C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1138024653 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1138024653/sys_clk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1138024653/sys_clk.o.d" -o ${OBJECTDIR}/_ext/1138024653/sys_clk.o C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk.c   
	
${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o: C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk_pic32mx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1138024653 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o.d" -o ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk_pic32mx.c   
	
${OBJECTDIR}/_ext/1231681268/sys_devcon.o: C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1231681268 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1231681268/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1231681268/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1231681268/sys_devcon.o C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon.c   
	
${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o: C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon_pic32mx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1231681268 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o.d" -o ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon_pic32mx.c   
	
${OBJECTDIR}/_ext/2044922275/sys_dma.o: C:/microchip/harmony/v0_80_02b/framework/system/dma/src/sys_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2044922275 
	@${RM} ${OBJECTDIR}/_ext/2044922275/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/2044922275/sys_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2044922275/sys_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/2044922275/sys_dma.o.d" -o ${OBJECTDIR}/_ext/2044922275/sys_dma.o C:/microchip/harmony/v0_80_02b/framework/system/dma/src/sys_dma.c   
	
${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o: C:/microchip/harmony/v0_80_02b/framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2061317862 
	@${RM} ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o C:/microchip/harmony/v0_80_02b/framework/system/int/src/sys_int_pic32.c   
	
${OBJECTDIR}/_ext/1227416125/sys_ports.o: C:/microchip/harmony/v0_80_02b/framework/system/ports/src/sys_ports.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1227416125 
	@${RM} ${OBJECTDIR}/_ext/1227416125/sys_ports.o.d 
	@${RM} ${OBJECTDIR}/_ext/1227416125/sys_ports.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1227416125/sys_ports.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1227416125/sys_ports.o.d" -o ${OBJECTDIR}/_ext/1227416125/sys_ports.o C:/microchip/harmony/v0_80_02b/framework/system/ports/src/sys_ports.c   
	
${OBJECTDIR}/_ext/919188156/sys_tmr.o: C:/microchip/harmony/v0_80_02b/framework/system/tmr/src/sys_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/919188156 
	@${RM} ${OBJECTDIR}/_ext/919188156/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/919188156/sys_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/919188156/sys_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/919188156/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/919188156/sys_tmr.o C:/microchip/harmony/v0_80_02b/framework/system/tmr/src/sys_tmr.c   
	
${OBJECTDIR}/_ext/401753235/usb_device.o: C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/401753235 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/401753235/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/401753235/usb_device.o.d" -o ${OBJECTDIR}/_ext/401753235/usb_device.o C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device.c   
	
${OBJECTDIR}/_ext/401753235/usb_device_cdc.o: C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/401753235 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/401753235/usb_device_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/401753235/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc.c   
	
${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o: C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc_acm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/401753235 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc_acm.c   
	
else
${OBJECTDIR}/_ext/12832417/system_config.o: F:/MSc/pic/anteye_master.X/system_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_config.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_config.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_config.o.d" -o ${OBJECTDIR}/_ext/12832417/system_config.o F:/MSc/pic/anteye_master.X/system_config.c   
	
${OBJECTDIR}/_ext/12832417/system_init.o: F:/MSc/pic/anteye_master.X/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_init.o.d" -o ${OBJECTDIR}/_ext/12832417/system_init.o F:/MSc/pic/anteye_master.X/system_init.c   
	
${OBJECTDIR}/_ext/12832417/system_interrupt.o: F:/MSc/pic/anteye_master.X/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/12832417/system_interrupt.o F:/MSc/pic/anteye_master.X/system_interrupt.c   
	
${OBJECTDIR}/_ext/12832417/system_tasks.o: F:/MSc/pic/anteye_master.X/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/system_tasks.o.d" -o ${OBJECTDIR}/_ext/12832417/system_tasks.o F:/MSc/pic/anteye_master.X/system_tasks.c   
	
${OBJECTDIR}/_ext/12832417/main.o: F:/MSc/pic/anteye_master.X/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/main.o.d" -o ${OBJECTDIR}/_ext/12832417/main.o F:/MSc/pic/anteye_master.X/main.c   
	
${OBJECTDIR}/_ext/12832417/app.o: F:/MSc/pic/anteye_master.X/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/12832417 
	@${RM} ${OBJECTDIR}/_ext/12832417/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/12832417/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/12832417/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/12832417/app.o.d" -o ${OBJECTDIR}/_ext/12832417/app.o F:/MSc/pic/anteye_master.X/app.c   
	
${OBJECTDIR}/bsp_config.o: bsp_config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR} 
	@${RM} ${OBJECTDIR}/bsp_config.o.d 
	@${RM} ${OBJECTDIR}/bsp_config.o 
	@${FIXDEPS} "${OBJECTDIR}/bsp_config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/bsp_config.o.d" -o ${OBJECTDIR}/bsp_config.o bsp_config.c   
	
${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o: C:/microchip/harmony/v0_80_02b/framework/driver/tmr/src/dynamic/drv_tmr_dynamic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1542292883 
	@${RM} ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o.d" -o ${OBJECTDIR}/_ext/1542292883/drv_tmr_dynamic.o C:/microchip/harmony/v0_80_02b/framework/driver/tmr/src/dynamic/drv_tmr_dynamic.c   
	
${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o: C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_buffer_queue_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1310334617 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o.d" -o ${OBJECTDIR}/_ext/1310334617/drv_usart_buffer_queue_dma.o C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_buffer_queue_dma.c   
	
${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o: C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1310334617 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o.d" -o ${OBJECTDIR}/_ext/1310334617/drv_usart_dma.o C:/microchip/harmony/v0_80_02b/framework/driver/usart/src/dynamic/drv_usart_dma.c   
	
${OBJECTDIR}/_ext/203677798/drv_usb.o: C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/203677798 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb.o.d 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/203677798/drv_usb.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/203677798/drv_usb.o.d" -o ${OBJECTDIR}/_ext/203677798/drv_usb.o C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb.c   
	
${OBJECTDIR}/_ext/203677798/drv_usb_device.o: C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/203677798 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/203677798/drv_usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/203677798/drv_usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/203677798/drv_usb_device.o.d" -o ${OBJECTDIR}/_ext/203677798/drv_usb_device.o C:/microchip/harmony/v0_80_02b/framework/driver/usb/usbfs/src/dynamic/drv_usb_device.c   
	
${OBJECTDIR}/_ext/1138024653/sys_clk.o: C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1138024653 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1138024653/sys_clk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1138024653/sys_clk.o.d" -o ${OBJECTDIR}/_ext/1138024653/sys_clk.o C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk.c   
	
${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o: C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk_pic32mx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1138024653 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o.d" -o ${OBJECTDIR}/_ext/1138024653/sys_clk_pic32mx.o C:/microchip/harmony/v0_80_02b/framework/system/clk/src/sys_clk_pic32mx.c   
	
${OBJECTDIR}/_ext/1231681268/sys_devcon.o: C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1231681268 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1231681268/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1231681268/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/1231681268/sys_devcon.o C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon.c   
	
${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o: C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon_pic32mx.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1231681268 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o.d 
	@${RM} ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o.d" -o ${OBJECTDIR}/_ext/1231681268/sys_devcon_pic32mx.o C:/microchip/harmony/v0_80_02b/framework/system/devcon/src/sys_devcon_pic32mx.c   
	
${OBJECTDIR}/_ext/2044922275/sys_dma.o: C:/microchip/harmony/v0_80_02b/framework/system/dma/src/sys_dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2044922275 
	@${RM} ${OBJECTDIR}/_ext/2044922275/sys_dma.o.d 
	@${RM} ${OBJECTDIR}/_ext/2044922275/sys_dma.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2044922275/sys_dma.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/2044922275/sys_dma.o.d" -o ${OBJECTDIR}/_ext/2044922275/sys_dma.o C:/microchip/harmony/v0_80_02b/framework/system/dma/src/sys_dma.c   
	
${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o: C:/microchip/harmony/v0_80_02b/framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/2061317862 
	@${RM} ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/2061317862/sys_int_pic32.o C:/microchip/harmony/v0_80_02b/framework/system/int/src/sys_int_pic32.c   
	
${OBJECTDIR}/_ext/1227416125/sys_ports.o: C:/microchip/harmony/v0_80_02b/framework/system/ports/src/sys_ports.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/1227416125 
	@${RM} ${OBJECTDIR}/_ext/1227416125/sys_ports.o.d 
	@${RM} ${OBJECTDIR}/_ext/1227416125/sys_ports.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1227416125/sys_ports.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/1227416125/sys_ports.o.d" -o ${OBJECTDIR}/_ext/1227416125/sys_ports.o C:/microchip/harmony/v0_80_02b/framework/system/ports/src/sys_ports.c   
	
${OBJECTDIR}/_ext/919188156/sys_tmr.o: C:/microchip/harmony/v0_80_02b/framework/system/tmr/src/sys_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/919188156 
	@${RM} ${OBJECTDIR}/_ext/919188156/sys_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/919188156/sys_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/919188156/sys_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/919188156/sys_tmr.o.d" -o ${OBJECTDIR}/_ext/919188156/sys_tmr.o C:/microchip/harmony/v0_80_02b/framework/system/tmr/src/sys_tmr.c   
	
${OBJECTDIR}/_ext/401753235/usb_device.o: C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/401753235 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device.o.d 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/401753235/usb_device.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/401753235/usb_device.o.d" -o ${OBJECTDIR}/_ext/401753235/usb_device.o C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device.c   
	
${OBJECTDIR}/_ext/401753235/usb_device_cdc.o: C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/401753235 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o.d 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/401753235/usb_device_cdc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/401753235/usb_device_cdc.o.d" -o ${OBJECTDIR}/_ext/401753235/usb_device_cdc.o C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc.c   
	
${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o: C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc_acm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} ${OBJECTDIR}/_ext/401753235 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o.d 
	@${RM} ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION) -O1 -I"." -I"C:/microchip/harmony/v0_80_02b/framework/peripheral" -I"C:/microchip/harmony/v0_80_02b/framework/driver" -I"C:/microchip/harmony/v0_80_02b/bsp" -I"C:/microchip/harmony/v0_80_02b/framework" -I"C:/microchip/harmony/v0_80_02b/framework/system" -I"C:/microchip/harmony/v0_80_02b/framework/usb" -MMD -MF "${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o.d" -o ${OBJECTDIR}/_ext/401753235/usb_device_cdc_acm.o C:/microchip/harmony/v0_80_02b/framework/usb/src/dynamic/usb_device_cdc_acm.c   
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  C:\\microchip\\harmony\\v0_80_02b\\bin\\framework\\peripheral\\PIC32MX675F256H_peripherals.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    C:\microchip\harmony\v0_80_02b\bin\framework\peripheral\PIC32MX675F256H_peripherals.a       -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  C:\\microchip\\harmony\\v0_80_02b\\bin\\framework\\peripheral\\PIC32MX675F256H_peripherals.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    C:\microchip\harmony\v0_80_02b\bin\framework\peripheral\PIC32MX675F256H_peripherals.a      -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/anteye_master.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
