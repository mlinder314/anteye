/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition                  */

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp              */

/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

/* i.e. uint16_t <variable_name>; */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

int16_t main(void)
{

    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize IO ports and peripherals */
    InitApp();

    // Set all of the pins on Port A to be outputs,
    // Cleared bits are outputs.
    TRISAbits.TRISA2 = 0;

    while(1)
    {
        // Toggle RA2
        unsigned int count;

        // Turn the LED on
        PORTAbits.RA2 = 1;

        // Create a short delay
        for( count = 0; count < 64000; count++) {}

        // Turn the LED off
        PORTAbits.RA2 = 0;

        // Create a short delay
        for( count = 0; count < 64000; count++) {}
    }
}
