/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

// I2C address of the camera
#define CAM 0x21 // I2C address of the camera

// The "any-device" id
#define DEVICE_ID_ANY '@'

// Our device ID. Has to be different.
#define OUR_DEVICE_ID 'P'

// Extra bytes that we send to prevent buffer-underruns on the master
#define FRAME_SAFETY_MARGIN 2

// I2C Registers
#define CAM_MIDH 0x1C
#define CAM_MIDL 0x1D
#define CAM_PID 0x0A
#define CAM_VER 0x0B
#define CAM_CLKRC 0x11
#define CAM_DBLV 0x6B
#define CAM_COM3 0x0C
#define CAM_SCALING_DCWCTR 0x72
#define CAM_SCALING_PCLK_DIV 0x73
#define CAM_SCALING_PCLK_DELAY 0xA2
#define CAM_SCALING_XSC 0x70
#define CAM_SCALING_YSC 0x71
#define CAM_COM1 0x04
#define CAM_COM10 0x15
#define CAM_COM12 0x3C
#define CAM_COM14 0x3E
#define CAM_COM7 0x12
#define CAM_COM15 0x40
#define CAM_HREF 0x32
#define CAM_PSHFT 0x1B
#define CAM_HSYST 0x30
#define CAM_HSYEN 0x31
#define CAM_COM11 0x3B
#define CAM_HSTART 0x17
#define CAM_HSTOP 0x18
#define CAM_TSLB 0x3A
#define CAM_COM8 0x13
#define CAM_ADCCTR0 0x20

// I2C Constants
#define CAM_MIDH_EXPECTED 0x7F
#define CAM_MIDL_EXPECTED 0xA2

/******************************************************************************/
/* Data types  */
/******************************************************************************/

extern uint8_t deviceID;

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/

inline void InitApp(void);         /* I/O and Peripheral Initialization */
inline void Loop(void);