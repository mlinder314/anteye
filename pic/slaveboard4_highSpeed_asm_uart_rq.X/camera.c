/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>         /* For true/false definition */

#include "system.h"
#include "config.h"

#include <libpic30.h>
#include <PPS.h>
#include <uart.h>
#include <ports.h>
#include <pmp.h>

#include "camera.h"  

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/



// Create a framebuffer that can store the entire image. Add an overflow buffer
// so that we do not overwrite memory in case our VSYNC interrupt is handled late.
#define FRAME_W 40
#define FRAME_H 30
#define FRAME_BUF_LEN (FRAME_W*FRAME_H)
static uint16_t frame[FRAME_BUF_LEN + 16 /* overflow buffer */];
static uint16_t frameNum = 0;

// Reserve registers for high speed asm interrupt routine
register uint16_t pixelWorkRegister1 asm ("w13");
register void* framePixelPtr asm ("w12");

void InitApp(void)
{
    // Setup LED output
    TRISAbits.TRISA2 = 0;
    PORTAbits.RA2 = 1; // Enable LED at start

    /**************** Reset Camera Module ****************/
    // Reset camera (active low)
    TRISAbits.TRISA4 = 0; // reset camera
    PORTAbits.RA4 = 0; // drive low
    __delay_ms(3);
    PORTAbits.RA4 = 1; // drive high

    //framePixelPtr = frame; // Reset pointer to beginning of the frame
    asm("mov #_frame, w12"); // framePixelPtr = &frame; (reset to start)
    asm("mov #PMDIN1, w13"); // store address of PMDIN1 in w12

    /**************** Setup Camera Module Config ****************/
    __delay_ms(10); // Wait for Module to load

    // Check that we are talking to the right module
    uint8_t midh = I2CRead(CAM, CAM_MIDH);
    uint8_t midl = I2CRead(CAM, CAM_MIDL);
    if (midh != CAM_MIDH_EXPECTED || midl != CAM_MIDL_EXPECTED)
    {
        while (1) ;
    }

    // Setup Clock Speed (need to get to 24 MHz: 8 * 6 / 2 )
    // ** 12 fps / 9.6 MHz (the only one that works without an ASM ISR)
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 5-1); // div 5
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6

    // ** 13.37 fps / 10.66 MHz
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 6-1); // div 6
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b11 << 6); // pll 8
    
    // ** 15 fps / 12 MHz
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 4-1); // div 4
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6

    // ** 16 fps / 12.8 MHz
    //I2CSet(CAM, CAM_CLKRC, 0xFF, 5-1); // div 5
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b11 << 6); // pll 8

    // ** 20 fps / 16 MHz
    I2CSet(CAM, CAM_CLKRC, 0xFF, 3-1); // div 3
    I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6
    
    // Setup Clock Signals
    I2CSet(CAM, CAM_COM10, (1 << 4), 0xFF); // PCLK reverse (inactive = low)
    I2CSet(CAM, CAM_COM10, (1 << 5), 0xFF); // PCLK does not toggle during HBLANK
    I2CSet(CAM, CAM_COM12, (1 << 7), 0x00); // No HREF during low VSYNC
    I2CSet(CAM, CAM_COM14,  (0b111), 0b100); // PCLK div 16 = 40 pixels wide
    I2CSet(CAM, CAM_COM14,  (1 << 4), 0xFF); // Enable PCLK div
    I2CSet(CAM, CAM_COM14,  (1 << 3), 0xFF); // Enable manual scaling

    // Setup Frame size (640x480 -> 80x60, div 8)
    I2CWrite(CAM, CAM_SCALING_DCWCTR, 0b11111111); // round, div 8:8
    I2CSet(CAM, CAM_SCALING_PCLK_DIV, 0b1111, 0b0011); // dsp enabled, clock div 8
    I2CSet(CAM, CAM_COM3, (1 << 2), 0xFF); // enable down sampling

    // Fix Resolution (75x60 -> 80x60)
    I2CSet(CAM, CAM_HSTART, 0xFF, 0x0F); // empirically determined, no idea why
    I2CSet(CAM, CAM_HSTOP, 0xFF, 0x04);
    I2CSet(CAM, CAM_COM1, (1 << 6), 0xFF);

    //Scale to 40x30
    I2CSet(CAM, CAM_SCALING_XSC, 0b1111111, 32); // no scale, changed by pclk
    I2CSet(CAM, CAM_SCALING_YSC, 0b1111111, 64); // *0.5
    I2CSet(CAM, CAM_COM3, (1 << 3), 0xFF); // enable down zooming

    // Setup Image Format
    I2CSet(CAM, CAM_COM7, 0b101, 0b100); // rgb
    I2CSet(CAM, CAM_COM15, 0b11110000, 0b11010000); // rgb 565, 00-FF
    I2CSet(CAM, CAM_TSLB, (1 << 3), 0x00); // swap byte order LH -> HL

    // Color Settings
    I2CSet(CAM, CAM_COM8, (1 << 6), 0xFF); // unlimited AEC step size
    //I2CSet(CAM, CAM_ADCCTR0, (1 << 3), 0xFF); // 1.5 ADC range = more colorful
    //I2CSet(CAM, CAM_ADCCTR0, 0b111, 0xFF); // 1.2x ADC adjust = darker


    // HACK/Fastmode: Double framerate by dividing image width by 2
    // -> 30 Hz
    // Seems to be broken though: the image gets displaced randomly on start
    // --> can never find start of image
#if 0
    I2CSet(CAM, CAM_CLKRC, 0xFF, 2-1); // div 3
    I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6
    I2CSet(CAM, CAM_COM14,  (0b111), 0b101); // PCLK div 32 = 20 pixels wide

#endif

    // DEBUG: Test Pattern
    //I2CSet(CAM, CAM_SCALING_YSC, (1 << 7), 0xFF); // 8 bar test pattern

    /**************** Setup Camera Clock Triggers, Data Ports ****************/
    // Setup PLCK input (RB14)
    TRISBbits.TRISB14 = 1;
    EnablePullUpCN12;

    // Setup HREF input (RB15)
    TRISBbits.TRISB15 = 1;
    EnablePullUpCN11;

    // Setup VSYNC input (RA1)
    TRISAbits.TRISA1 = 1;
    EnablePullUpCN3;
    EnableCN3; // need interrupt for this

    // Setup PMP module for pixel readout:
    // 8 bit buffered legacy slave mode with write strobe + cable select
    PMPClose();
    uint16_t pmp_control = BIT_PMP_ON | BIT_SIDL_OFF | BIT_BE_OFF | BIT_RD_WR_ON |
             BIT_USE_CS2_CS1 | BIT_CS1_HI | BIT_RD_WR_HI;
    uint16_t pmp_mode = BIT_IRQ_BUF | BIT_INC_ADDR_AUTO | BIT_DATA_8 |
            BIT_MODE_SLAVE | BIT_WAITM_0_TCY;
    uint16_t pmp_port = 0;
    uint16_t pmp_addr = BIT_CS1_ON;
    uint8_t pmp_intr = 0;
    PMPOpen(pmp_control, pmp_mode, pmp_port, pmp_addr, pmp_intr);

    /**************** Get Ready ****************/
    // Enable pin change interrupts (should be higher than pmp)
    ConfigIntCN(INT_ENABLE & INT_PRI_3);

    // Enable PMP interrupt (this has to be the last step!)
    IEC2bits.PMPIE = 1;
    IPC11bits.PMPIP = 6; // service with highest prio since fast action needed
}

void __attribute__ ((interrupt,no_auto_psv)) _CNInterrupt(void) {
    InputChange_Clear_Intr_Status_Bit;

    // Check VSYNC (RA1)
    if (PORTAbits.RA1) {
        // Reset Pointer to the beginning of frame
        //framePixelPtr = frame; // Reset pointer to beginning of the frame
        asm("mov #_frame, w12"); // framePixelPtr = &frame; (reset to start)
        
        // Clear eventual overflows (needed to continue PMP operation)
        PMDIN1;
        PMDIN2;
        mPMPClearBufferOverflow;

        // Toggle LED
        LATAbits.LATA2 ^= 1;

        // Increase our frame stats
        frameNum++;
    }
}

inline void Loop(void)
{
    unsigned int i;

    // Wait for command begin
    while (ReadByte() != 'R') // "Request"
        ;

    // Check if device id matches
    uint8_t deviceID = ReadByte();
    if (deviceID != DEVICE_ID_ANY && deviceID != OUR_DEVICE_ID)  {
        return; // invalid seq, start over again
    }

    // Read the command type byte
    char cmd = ReadByte();
    
    // Use synchronous UART for transmission. Enable transmitter temporarily
    // so that we can share the bus between multiple devices
    if (deviceID != DEVICE_ID_ANY) {
         // only do this for non-broadcast ops
        EnableTransmitter();
    }

    /***** Frame Rendering Commands ********/
    if (cmd == 'F') { // F
        /* Framing test */
#if FRAME_TEST
        PrintChar('S');
        for (i = 0; i < FRAME_BUF_LEN; i++) {
            PrintChar('A');
            PrintChar('B');
        }
        PrintChar('E');
#else

        /* ### Request Full Frame ## */
#ifdef LOSE_FRAMES
        static int lostFrameSimulation = 0;
        lostFrameSimulation++;
#endif
        for (i = 0; i < FRAME_BUF_LEN; i++) {
#ifdef LOSE_FRAMES
            if (lostFrameSimulation % 5 == 0 && i >= 230 && i < 234)
                continue;
#endif
            uint16_t pixel = frame[i];
            PrintChar(pixel & 0xFF);
            PrintChar(pixel >> 8);
        }
        
        // Send a few bytes to fix stale frames/lost bytes
        // This will cause the master read to complete if data was lost before
        for (i = 0; i < FRAME_SAFETY_MARGIN; i++)
            PrintChar(0x00);
#endif
    } else if (cmd == 'H') { // F
        /* ### Request Half Frame (e.g. in 30 Hz mode) ## */
        for (i = 0; i < FRAME_BUF_LEN/2; i++) {
            uint16_t pixel = frame[i];
            PrintChar(pixel & 0xFF);
            PrintChar(pixel >> 8);
        }

        // Send a few bytes to fix stale frames/lost bytes
        // This will cause the master read to complete if data was lost before
        for (i = 0; i < FRAME_SAFETY_MARGIN; i++)
            PrintChar(0x00);
    } else if (cmd == 'T') { // T [SkipX] [SkipY]
        /* ### Request Tiny/Thumbnail Frame ## */
        int w;
        int h;
        uint8_t skipX = ReadByte();
        uint8_t skipY = ReadByte();
        for (h = 0; h < FRAME_H; h += 1 + skipY) {
            for (w = 0; w < FRAME_W; w += 1 + skipX) {
                // Calculate pixel index
                i = w + (h * FRAME_W);
                uint16_t pixel = frame[i];
                PrintChar(pixel & 0xFF);
                PrintChar(pixel >> 8);
            }
        }

        // Send a few bytes to fix stale frames/lost bytes
        // This will cause the master read to complete if data was lost before
        for (i = 0; i < FRAME_SAFETY_MARGIN; i++)
            PrintChar(0x00);
    } else if (cmd == 'S') {
        /* ### Request SELECTED Frame (Pixels) only ## */
        for (i = 0; i < Config.selectedPixelCount; i += 1) {
            uint16_t pixel = frame[Config.selectedPixels[i]];
            PrintChar(pixel & 0xFF);
            PrintChar(pixel >> 8);
        }

        // Send a few bytes to fix stale frames/lost bytes
        // This will cause the master read to complete if data was lost before
        for (i = 0; i < FRAME_SAFETY_MARGIN; i++)
            PrintChar(0x00);
    } else if (cmd == 'V') {
        /* ### Version #### */
        PrintString("AntEye Camera Board v1.0");
    } else if (cmd == 'P') {
        PrintString("PING");
    }
     /***** GET settings ********/
    else if (cmd == '?') {
        char subCmd = ReadByte();
        if (subCmd == 'I') {
            // I2C Read
            uint8_t addr = ReadByte();
            uint8_t data = I2CRead(CAM, addr);
            PrintChar(data);
        } else if (subCmd == 'F') {
            // Read Flash
            uint16_t startAddr = ReadByte() | (ReadByte() << 8);
            uint16_t len = ReadByte() | (ReadByte() << 8);
            uint8_t * cfgPtr = &Config;
            while (len > 0) {
                uint8_t data = cfgPtr[startAddr++];
                PrintChar(data);
                len--;
            }
        } else if (subCmd == 'P') {
            // Get selected pixel count
            PrintChar(Config.selectedPixelCount & 0xFF);
            PrintChar(Config.selectedPixelCount >> 8);
        }
    }
    /***** Frame Rendering Commands ********/
    else if (cmd == '!') { /* ### Change Settings / Danger */
        char subCmd = ReadByte();
        if (subCmd == 'I') {
            uint8_t addr = ReadByte();
            uint8_t mask = ReadByte();
            uint8_t data = ReadByte();
            I2CSet(CAM, addr, mask, data);
        } else if (subCmd == 'F') {
            // Write Flash
            uint16_t startAddr = (uint16_t)ReadByte() | ((uint16_t)ReadByte() << 8);
            uint16_t len = (uint16_t)ReadByte() | ((uint16_t)ReadByte() << 8);
            uint8_t * cfgPtr = &Config;
            while (len > 0) {
                cfgPtr[startAddr++] = ReadByte();
                len--;
            }
        } else if (subCmd == 'S') {
            saveConfig();
        }
        else if (subCmd == 'R') {
            // Reset module
            if (deviceID != DEVICE_ID_ANY) {
               PrintString("RESET");
               DisableTransmitter();
               __delay_ms(10);
            }
            asm ("RESET");
            while (1) {}
        }
        else if (subCmd == 'U') {
            /* Change Baudrate */
            DisableTransmitter();

            // Change UART speed
            unsigned long newSpeed = (uint32_t)ReadByte() | ((uint32_t)ReadByte() << 8) | ((uint32_t)ReadByte() << 16) | ((uint32_t)ReadByte() << 24);
            U1BRG = FCY / (4L*newSpeed) - 1;

            __delay_ms(50);
        } else if (subCmd == 'H') {
            /* High speed / 30 Hz mode */
            I2CSet(CAM, CAM_CLKRC, 0xFF, 2-1); // div 3
            I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // pll 6 = 24 MHz
            I2CSet(CAM, CAM_COM14,  (0b111), 0b101); // PCLK div 32 = 20 pixels wide
        }

        if (deviceID != DEVICE_ID_ANY)
            PrintChar('*'); /* ACK */
    }

    // Wait until everything has been printed, then disable transmitter
    if (deviceID != DEVICE_ID_ANY) {
        DisableTransmitter();
    }
}