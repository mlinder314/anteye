/*
 * File:   pmpInterrupt.s
 *
 * Handles the pixel-clock/parallel master port interrupt. Stores the bytes
 * in an internal framebuffer (dma-like) so that it can be processed further.
 */

.include "p24Fxxxx.inc"

;..............................................................................
;Global Declarations:
;..............................................................................

.global __PMPInterrupt ; Called when the PMP 4 byte buffer is full
;.global _framePixelPtr ; Contains the address of where we will store the next pixel

;..............................................................................
;Uninitialized variables in Near data memory (Lower 8Kb of RAM)
;..............................................................................

.section .nbss, bss, near
;    _framePixelPtr: .space 2              ;uint16_t ptr

;..............................................................................
;Code Section in Program Memory
;..............................................................................

.text                             ;Start of Code section

;; Interrupt implementation WITHOUT reserved registers: 10 cycles
;__PMPInterrupt:
;    push.s ; save w0-w3 registers to shadows (warning - only one copy exists)
;    mov _framePixelPtr, w0 ; load pointer - must be absolute
;    mov PMDIN1, w1
;    mov PMDIN2, w2
;    mov w1,[w0++]
;    mov w2,[w0++]
;    mov w0, _framePixelPtr ; save new pointer
;    pop.s ; restore registers
;    bclr IFS2, #PMPIF ; whatever you need to clear interrupt
;    retfie

;; Interrupt implementation WITH two reserved registers: 6 cycles
__PMPInterrupt:
    ;; 2+2+2+2 = 8 cycles
    ;mov PMDIN1, w12 ; pixel buffer
    ;mov w12,[w13++] ; pixel buffer -> frame pixel ptr
    ;mov PMDIN2, w12
    ;mov w12,[w13++]
    ;bclr IFS2, #PMPIF ; clear interrupt

    ;; 2+2+3 = 7 cycles
    ;mov PMDIN1, w10
    ;mov PMDIN2, w11
    ;mov.d w10, [w12++]
    ;bclr IFS2, #PMPIF ; clear interrupt

    ;; 2+2 = 4 cycles -- as close as it'll get to real DMA
    mov [w13++],[w12++] ; move PMDIN1 and re-point w12 to PMDIN2
    bclr IFS2, #PMPIF
    mov [w13--],[w12++] ; move PMDIN2 and re-point w12 to PMDIN1

    retfie

;--------End of All Code Sections ---------------------------------------------

.end                               ;End of program code in this file
