#include "config.h"
#include "rtsp.h"

#include <xc.h>
#include "system.h"

/** Ram Config */
CONFIG Config;

/** Flash Config */
// One row = 64 ints
// One page = 8 rows = 512 ints
// Need multiple of page num for erases
const PM_ROW_START ConfigInFlash0[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash1[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash2[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash3[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash4[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash5[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash6[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash7[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash8[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash9[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash10[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash11[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash12[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash13[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash14[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash15[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash16[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash17[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash18[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash19[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash20[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash21[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash22[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash23[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
const PM_ROW ConfigInFlash24[64] = {  0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


#define READ(src, off) flashPageRead(__builtin_tblpage(&src[0]), __builtin_tbloffset(&src[0]), (i16 *)&Config + (64*off))
#define ERASE(src) { \
    PrintString("ERASE: "); \
    PrintNumber(__builtin_tblpage(&src[0]), 10000); \
    PrintString(" "); \
    PrintNumber(__builtin_tbloffset(&src[0]), 10000); \
    PrintString("\r\n"); \
    flashPageErase(__builtin_tblpage(&src[0]), __builtin_tbloffset(&src[0])); PrintString("DONE\r\n"); \
}
#define WRITE(src, off) { PrintString("WRITE\r\n"); flashPageWrite(__builtin_tblpage(&src[0]), __builtin_tbloffset(&src[0]), (i16 *)&Config + (64*off)); }

/** Restores the configuration from flash */
void restoreConfig() {
    // Enable Flash -> RAM mapping
    PSVPAG = 0;
    CORCONbits.PSV = 1;

    READ(ConfigInFlash0, 0);
    READ(ConfigInFlash1, 1);
    READ(ConfigInFlash2, 2);
    READ(ConfigInFlash3, 3);
    READ(ConfigInFlash4, 4);
    READ(ConfigInFlash5, 5);
    READ(ConfigInFlash6, 6);
    READ(ConfigInFlash7, 7);
    READ(ConfigInFlash8, 8);
    READ(ConfigInFlash9, 9);
    READ(ConfigInFlash10, 10);
    READ(ConfigInFlash11, 11);
    READ(ConfigInFlash12, 12);
    READ(ConfigInFlash13, 13);
    READ(ConfigInFlash14, 14);
    READ(ConfigInFlash15, 15);
    READ(ConfigInFlash16, 16);
    READ(ConfigInFlash17, 17);
    READ(ConfigInFlash18, 18);
    READ(ConfigInFlash19, 19);
    READ(ConfigInFlash20, 20);
    READ(ConfigInFlash21, 21);
    READ(ConfigInFlash22, 22);
    READ(ConfigInFlash23, 23);
}

void testConfig() {
    U1STAbits.UTXEN = 1;

    PrintString("Restoring Config from Flash ...\r\n");
    restoreConfig();

    // Assert the initial state of the config
    PrintString("Checking for blank config...\r\n");
    u8 * cfgPtr = &Config;
    int i;
    for (i = 0; i < CONFIG_LEN; i++) {
        u8 valu = cfgPtr[i];
        if (valu != 0x00) {
            PrintString("ERROR: Config NOT blank @ i="); PrintNumber(i, 10000); PrintString("\r\n");
            PrintString("  Value is "); PrintNumber(valu, 10000); PrintString("\r\n");
            while (1) ;
        }
    }

    // Make a change
    PrintString("Changing RAM Config ..\r\n");
    Config.selectedPixelCount = 123;
    Config.selectedPixels[66] = 19;
    PrintString("Saving to Flash ..\r\n");
    saveConfig();

    // Undo the change
    PrintString("Resetting RAM config.\r\n");
    Config.selectedPixelCount = 0;
    Config.selectedPixels[66] = 0;

    // Restore the config
    PrintString("Restoring Config from flash ... \r\n");
    restoreConfig();
    u16 cnt = Config.selectedPixelCount;
    u16 px = Config.selectedPixels[66];
    PrintString("cnt="); PrintNumber(cnt, 10000); PrintString("\r\n");
    PrintString("px="); PrintNumber(px, 10000); PrintString("\r\n");
    if (cnt != 123 || px != 19) {
        PrintString("ERROR: restored config is invalid\r\n");
        while (1) ;
    }

    PrintString("Checking remaining config...\r\n");
    for (i = 10; i < CONFIG_LEN; i++) {
        u8 valu = cfgPtr[i];
        if (valu != 0x00) {
            PrintString("ERROR: Value at index is not zero: "); PrintNumber(i, 10000); PrintString("\r\n");
            //while (1) ;
        }
    }
    PrintString("All valid.\r\n");
    while (1) {}
}

/** Saves the configuration to the flash */
void saveConfig() {
    ERASE(ConfigInFlash0);
    WRITE(ConfigInFlash0, 0);
    WRITE(ConfigInFlash1, 1);
    WRITE(ConfigInFlash2, 2);
    WRITE(ConfigInFlash3, 3);
    WRITE(ConfigInFlash4, 4);
    WRITE(ConfigInFlash5, 5);
    WRITE(ConfigInFlash6, 6);
    WRITE(ConfigInFlash7, 7);

    ERASE(ConfigInFlash8);
    WRITE(ConfigInFlash8, 8);
    WRITE(ConfigInFlash9, 9);
    WRITE(ConfigInFlash10, 10);
    WRITE(ConfigInFlash11, 11);
    WRITE(ConfigInFlash12, 12);
    WRITE(ConfigInFlash13, 13);
    WRITE(ConfigInFlash14, 14);
    WRITE(ConfigInFlash15, 15);

    ERASE(ConfigInFlash16);
    WRITE(ConfigInFlash16, 16);
    WRITE(ConfigInFlash17, 17);
    WRITE(ConfigInFlash18, 18);
    WRITE(ConfigInFlash19, 19);
    WRITE(ConfigInFlash20, 20);
    WRITE(ConfigInFlash21, 21);
    WRITE(ConfigInFlash22, 22);
    WRITE(ConfigInFlash23, 23);
}