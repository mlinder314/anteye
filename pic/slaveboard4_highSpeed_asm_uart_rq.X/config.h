/* 
 * File:   config.h
 * Author: Matthias
 *
 * Created on July 6, 2014, 4:51 AM
 */

#ifndef CONFIG_H
#define	CONFIG_H

#include "typeDef.h"

// First row needs to be aligned on a 1024 (page) block
#define PM_ROW_START	int __attribute__((keep, space(prog), aligned(1024)))
#define PM_ROW	int __attribute__((keep, space(prog), aligned(128)))
#define CONFIG_LEN (1024*3)

/** Defines what kind of data will be stored in our config*/
typedef struct {
    u16 RESERVED1;
    u16 RESERVED2;
    u16 RESERVED3;
    u16 RESERVED4;
    u16 RESERVED5;
    u16 RESERVED6;
    u16 RESERVED7;
    u16 RESERVED8;
    u16 selectedPixelCount;
    u16 selectedPixels[CONFIG_LEN/2 - 1];
} CONFIG;

/** Ram Config */
extern CONFIG Config;

/** Restores the configuration from flash */
void restoreConfig();

/** Saves the configuration to the flash */
void saveConfig();

void testConfig();

#endif	/* CONFIG_H */

