/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition       */
#include <wdt.h>

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "config.h"
#include "camera.h"          /* User funct/params, such as InitApp              */

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

int16_t main(void)
{
    EnableWDT(WDT_DISABLE);

    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize low level subcomponents */
    AD1PCFG = -1; // disable all analog pins (necessary for PORT reads)
    InitUART();
    InitI2C();
    
    /* Initialize IO ports and external peripherals */
    restoreConfig();
    InitApp();

    while(1)
        Loop();
}
