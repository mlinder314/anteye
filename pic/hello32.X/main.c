/* 
 * File:   main.c
 * Author: Matthias
 *
 * Created on July 4, 2014, 12:45 AM
 */

#include <stdio.h>
#include <stdlib.h>

#include <xc.h>

#include <plib.h>
/*
 * 
 */
int main(int argc, char** argv) {
    AD1PCFG = -1;
    
    TRISBbits.TRISB1 = 0;
    TRISBbits.TRISB3 = 0;
    int i = 0;
    while (1) {
        LATBbits.LATB1 ^= 1;
        LATBbits.LATB3 ^= 1;
        for (i = 0; i < 500000; i++) ;
    }
    return (EXIT_SUCCESS);
}

