/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>         /* For true/false definition */

#include "system.h"
#include <libpic30.h>
#include <PPS.h>
#include <uart.h>
#include <ports.h>
#include <pmp.h>

#include "user.h"            /* variables/params used by user.c */

/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/

void InitApp(void)
{
    PrintString("\r\n\r\n");
    PrintString("= AntEye SlaveBoard =\r\n");
    PrintString("=====================\r\n");

    // Setup LED output
    TRISAbits.TRISA2 = 0;
    PORTAbits.RA2 = 1; // Enable LED at start

    /**************** Reset Camera Module ****************/
    // Reset camera (active low)
    TRISAbits.TRISA4 = 0; // reset camera
    PORTAbits.RA4 = 0; // drive low
    __delay_ms(10);
    PORTAbits.RA4 = 1; // drive high

    /**************** Setup Camera Module Config ****************/
    __delay_ms(10); // Wait for Module to load

    // Check that we are talking to the right module
    uint8_t midh = I2CRead(CAM, CAM_MIDH);
    uint8_t midl = I2CRead(CAM, CAM_MIDL);
    if (midh != CAM_MIDH_EXPECTED || midl != CAM_MIDL_EXPECTED)
    {
        PrintString("I2C Camera error:\r\n");
        PrintString("  MIDH = "); PrintNumber(midh, 100); PrintString("\r\n");
        PrintString("  MIDL = "); PrintNumber(midl, 100); PrintString("\r\n");
        while (1) ;
    }
    PrintString("I2C Camera detected\r\n");
    PrintString("  PID = "); PrintNumber(I2CRead(CAM, CAM_PID), 100); PrintString("\r\n");
    PrintString("  VER = "); PrintNumber(I2CRead(CAM, CAM_VER), 100); PrintString("\r\n");

    // Setup Clock Speed (need to get to 24 MHz: 8 * 6 / 2 )
    // ** 5 hz:
    I2CSet(CAM, CAM_CLKRC, 0xFF, (0b00 << 6) | (2-1));
    I2CSet(CAM, CAM_COM14,  (0b111), 0b101); // PCLK div 32 (scales down by 0.5) for 5hz

    // ** 10 hz:
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // mul 6
    //I2CSet(CAM, CAM_CLKRC, 0xFF, (0b00 << 6) | (6-1));
    //I2CSet(CAM, CAM_COM14,  (0b111), 0b110); // PCLK div 64 (scales down by 0.25) for 10hz
    
    // ** 20 hz:
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // mul 6
    //I2CSet(CAM, CAM_CLKRC, 0xFF, (0b00 << 6) | (3-1));
    //I2CSet(CAM, CAM_COM14,  (0b111), 0b111); // PCLK div 128 (scales down by 0.125) for 20hz

    // ** 30 hz
    //I2CSet(CAM, CAM_DBLV, 0b11 << 6, 0b10 << 6); // mul 6
    //I2CWrite(CAM, CAM_CLKRC, 2-1); // div 2 = 30 Hz / 24 MHz
   
    // Setup Clock Signals
    I2CSet(CAM, CAM_COM10, (1 << 4), 0xFF); // PCLK reverse (inactive = low)
    I2CSet(CAM, CAM_COM10, (1 << 5), 0xFF); // PCLK does not toggle during HBLANK
    I2CSet(CAM, CAM_COM12, (1 << 7), 0x00); // No HREF during low VSYNC
    I2CSet(CAM, CAM_COM14,  (1 << 4), 0xFF); // Enable PCLK div
    I2CSet(CAM, CAM_COM14,  (1 << 3), 0xFF); // Enable manual scaling

    // Setup Frame size (640x480 -> 80x60, div 8)
    I2CWrite(CAM, CAM_SCALING_DCWCTR, 0b11111111); // round, div 8:8
    I2CSet(CAM, CAM_SCALING_PCLK_DIV, 0b1111, 0b0011); // dsp enabled, clock div 8
    I2CSet(CAM, CAM_COM3, (1 << 2), 0xFF); // enable down sampling

    // Fix Resolution (75x60 -> 80x60)
    I2CSet(CAM, CAM_HSTART, 0xFF, 0x0F); // empirically determined
    I2CSet(CAM, CAM_HSTOP, 0xFF, 0x04);

    // Scale down further (digitally, 80x60 -> 40x30)
    I2CSet(CAM, CAM_SCALING_XSC, 0b1111111, 32); // no scale, changed by pclk
    I2CSet(CAM, CAM_SCALING_YSC, 0b1111111, 127); // *0.25
    I2CSet(CAM, CAM_COM3, (1 << 3), 0xFF); // enable down zooming

    // Setup Image Format
    I2CSet(CAM, CAM_COM7, 0b101, 0b100); // rgb
    I2CSet(CAM, CAM_COM15, 0b11110000, 0b11010000); // rgb 565, 00-FF
    I2CSet(CAM, CAM_TSLB, (1 << 3), 0x00); // swap byte order LH -> HL

    // DEBUG: Scale to 40x30
    //I2CSet(CAM, CAM_SCALING_YSC, 0b1111111, 64); // *0.5
    //I2CSet(CAM, CAM_COM14,  (0b111), 0b100); // PCLK div 16
    //I2CSet(CAM, CAM_CLKRC, 0xFF, (0b00 << 6) | (4-1)); // 1 MHz

    // DEBUG: Scale to 80x60
    I2CSet(CAM, CAM_COM3, (1 << 3), 0x00); // disable down zooming
    I2CSet(CAM, CAM_COM14,  (0b111), 0b011); // PCLK div 8
    I2CSet(CAM, CAM_CLKRC, 0xFF, (0b00 << 6) | (8-1)); // 1 MHz

    // DEBUG: Test Pattern
    //I2CSet(CAM, CAM_SCALING_YSC, (1 << 7), 0xFF); // 8 bar test pattern

    PrintString("I2C Camera configured\r\n");

    /**************** Setup Camera Clock Triggers, Data Ports ****************/
    // Setup PLCK input (RB14)
    TRISBbits.TRISB14 = 1;
    EnablePullUpCN12;
    //EnableCN12;

    // Setup HREF input (RB15)
    TRISBbits.TRISB15 = 1;
    EnablePullUpCN11;
    //EnableCN11;

    // Setup VSYNC input (RA1)
    TRISAbits.TRISA1 = 1;
    EnablePullUpCN3;
    EnableCN3;

    // Setup PMP module for pixel readout:
    // 8 bit buffered legacy slave mode with write strobe + cable select
    PMPClose();
    uint16_t pmp_control = BIT_PMP_ON | BIT_SIDL_OFF | BIT_BE_OFF | BIT_RD_WR_ON |
             BIT_USE_CS2_CS1 | BIT_CS1_HI | BIT_RD_WR_HI;
    uint16_t pmp_mode = BIT_IRQ_BUF | BIT_INC_ADDR_AUTO | BIT_DATA_8 | 
            BIT_MODE_SLAVE | BIT_WAITM_0_TCY;
    uint16_t pmp_port = 0;
    uint16_t pmp_addr = BIT_CS1_ON;
    uint8_t pmp_intr = 0;
    PMPOpen(pmp_control, pmp_mode, pmp_port, pmp_addr, pmp_intr);

    /**************** Get Ready ****************/
    // Flush UART buffer
    PrintString("Beginning Main Loop...\r\n");
    Flush();

    // Enable pin change interrupts (causes heavy CPU load)
    ConfigIntCN(INT_ENABLE & INT_PRI_3);

    // Enable PMP interrupt
    IEC2bits.PMPIE = 1;
    IPC11bits.PMPIP = 2; // service with high prio since fast action needed
}

static uint16_t pulsesDiscarded = 0;
static uint16_t bytesRead = 0;

static uint16_t frame[20*15 + 256 /* overflow buffer */];
static uint16_t frameIndex = 0;

void __attribute__ ((interrupt,no_auto_psv)) _PMPInterrupt(void) {
    if (mPMPIsBufferOverflow) {
        pulsesDiscarded++;
        mPMPClearBufferOverflow;
        PMDIN1;
        PMDIN2;
        return;
    }
    else if (!mPMPIsBufferFull)
        return;
    
    unsigned int pixel1 = PMDIN1;
    unsigned int pixel2 = PMDIN2;
    //frame[frameIndex++] = pixel1;
    //frame[frameIndex++] = pixel2;
    PrintChar(pixel1 & 0xFF);
    PrintChar(pixel1 >> 8);
    PrintChar(pixel2 & 0xFF);
    PrintChar(pixel2 >> 8);
    
    bytesRead += 4;
}

void __attribute__ ((interrupt,no_auto_psv)) _CNInterrupt(void) {
    static uint16_t f = 0;

    InputChange_Clear_Intr_Status_Bit;

    // Check VSYNC (RA1)
    if (PORTAbits.RA1) {
        frameIndex = 0;

        // frame change
        PrintString("EOF\r\n");
        PrintString(" r"); PrintNumber(bytesRead, 1000);
        PrintString(" d"); PrintNumber(pulsesDiscarded, 100);
        PrintString(" f"); PrintNumber(f, 100);
        PrintString(" BOF");
        LATAbits.LATA2 ^= 1;
        bytesRead = 0;
        pulsesDiscarded = 0;
        f++;
    }
}

void Loop(void)
{
}