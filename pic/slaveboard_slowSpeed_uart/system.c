/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>          /* For uint32_t definition */
#include <stdbool.h>         /* For true/false definition */

#include "system.h"          /* variables/params used by system.c */

#include <PPS.h>
#include <uart.h>
#include <libpic30.h>
#include <i2c.h>

/******************************************************************************/
/* System Level Functions                                                     */
/*                                                                            */
/* Custom oscillator configuration funtions, reset source evaluation          */
/* functions, and other non-peripheral microcontroller initialization         */
/* functions get placed in system.c                                           */
/*                                                                            */
/******************************************************************************/

void ConfigureOscillator(void)
{
   CLKDIVbits.RCDIV = 0b000; // 8 MHz base clock * 4 PLL = 32/2 MHz
   CLKDIVbits.DOZEN = 0; // 1:1 peripheral clock
}

/******************************************************************************/
/* UART                                                            */
/******************************************************************************/

void InitUART(void) {
    // Reset port
    CloseUART1();

    // Link RP4 to UART TX
    iPPSOutput(OUT_PIN_PPS_RP4, OUT_FN_PPS_U1TX);

    // Configure Interrupts
    ConfigIntUART1(UART_TX_INT_EN & // enable interrupt
            UART_TX_INT_PR7 &  // priority x (has to be high to avoid buffer lock)
            UART_RX_INT_DIS); // disable RX interrupt

    // Enable standard simplex UART
    OpenUART1(UART_EN & UART_IDLE_CON & UART_UEN_00 & UART_DIS_ABAUD &
            UART_NO_PAR_8BIT & UART_1STOPBIT & UART_IrDA_DISABLE,
            UART_TX_ENABLE & UART_INT_TX_BUF_EMPTY & UART_SYNC_BREAK_ENABLED &
            UART_IrDA_POL_INV_ZERO,
            FCY / (4L*UART_BAUD) - 1);
}

void PrintChar(char chr) {
    // WARN: Does not check for overflows / buffer overruns

    // Find the next buffer position
    UART_TX_RING[UART_TX_WRITE_INDEX] = chr;
    UART_TX_WRITE_INDEX = (UART_TX_WRITE_INDEX + 1) % UART_TX_SIZE;

    // If no transmission is in progress, start transmitting
    if (U1STAbits.TRMT)
        IFS0bits.U1TXIF = 1; // causes interrupt
}
void PrintString(char* str) {
    while (str[0]) {
        PrintChar(str[0]);
        str++;
    }
}

void PrintNumber(uint16_t num, uint16_t start) {
    int i = start;
    while (i) {
        PrintChar('0' + ((num / i) % 10));
        i /= 10;
    }
}

void DiscardBuffer() {
    UART_TX_READ_INDEX = UART_TX_WRITE_INDEX;
}

void Flush(void) {
    uint16_t oldPrio = IPC3bits.U1TXIP;
    IPC3bits.U1TXIP = 7;
    while (UART_TX_READ_INDEX != UART_TX_WRITE_INDEX && !U1STAbits.TRMT) ;
    IPC3bits.U1TXIP = oldPrio;
}

void __attribute__ ((interrupt,no_auto_psv)) _U1TXInterrupt(void)
{
    // Clear interrupt flag
    U1TX_Clear_Intr_Status_Bit;

    // Check if there is more data to send, and if we have free buffer space
    while (UART_TX_READ_INDEX != UART_TX_WRITE_INDEX && !U1STAbits.UTXBF) {
        //WriteUART1(UART_TX_RING[UART_TX_INDEX++]);
        U1TXREG = UART_TX_RING[UART_TX_READ_INDEX];
        UART_TX_READ_INDEX = (UART_TX_READ_INDEX + 1) % UART_TX_SIZE;
    }
}

/******************************************************************************/
/* I2C (Uses BitBanging since the PIC implementation is buggy                 */
/******************************************************************************/

#define SCL_LOW { LATBbits.LATB3 = 0; TRISBbits.TRISB3 = 0; }
#define SCL_HIGH { TRISBbits.TRISB3 = 1; }
#define SCL PORTBbits.RB3
#define SDA_LOW { LATBbits.LATB2 = 0; TRISBbits.TRISB2 = 0; }
#define SDA_HIGH { TRISBbits.TRISB2 = 1; }
#define SDA PORTBbits.RB2

// Causes a single I2C clock and reads/writes the SDA bit
uint8_t I2CClockTick(uint8_t sda) {
    // Pull SCL low
    SCL_LOW;

    // Set SDA bit
    if (sda) SDA_HIGH
    else SDA_LOW;
    I2C_DELAY;
    I2C_DELAY;
    
    // Release SCL and wait until it is high again
    SCL_HIGH;
    while (!SCL) {
        SCL_HIGH;
    }

    // Limit Bus Speed a bit
    I2C_DELAY;
    uint8_t res = SDA;
    I2C_DELAY;

    // Pull SCL low again
    SCL_LOW;

    return res;
}

void InitI2C(void) {
    TRISBbits.TRISB3 = 1; // RB3 -> SCL input

    // Clear bus by toggling SDA (stop condition)
    int i;
    for (i = 0; i < 2; i++) {
        SDA_LOW;
        __delay_us(100);
        SDA_HIGH;
        __delay_us(100);
    }
}

void I2CWrite(uint8_t device, uint8_t reg, uint8_t value) {
    int i;

    // Start Bit: Pull SDA low, leave SCL up
    SDA_LOW;
    I2C_DELAY;

    // Write device addr bit
    for (i = 6; i >= 0; i--) {
        I2CClockTick((device >> i) & 1);
    }
    I2CClockTick(0); // R/W = Write
    if (I2CClockTick(0xFF)) PrintString("I2C Addr NACK");

    // Write register bits
    for (i = 7; i >= 0; i--) {
        I2CClockTick((reg >> i) & 1);
    }
    if (I2CClockTick(0xFF)) PrintString("I2C reg NACK");

    // Write value bits
    for (i = 7; i >= 0; i--) {
        I2CClockTick((value >> i) & 1);
    }
    if (I2CClockTick(0xFF)) PrintString("I2C val NACK");

    // Stop Condition: Pull SDA down, then release SCL up & release SDA
    SDA_LOW;
    I2C_DELAY;
    SCL_HIGH;
    I2C_DELAY;
    SDA_HIGH;
    I2C_DELAY;
}

uint8_t I2CRead(uint8_t device, uint8_t reg) {
    int i;

    // Start Bit: Pull SDA low, leave SCL up
    SDA_LOW;
    I2C_DELAY;

    // Write device addr bit
    for (i = 6; i >= 0; i--) {
        I2CClockTick((device >> i) & 1);
    }
    I2CClockTick(0); // R/W = Write
    if (I2CClockTick(0xFF)) PrintString("I2C Addr NACK");

    // Write register bit
    for (i = 7; i >= 0; i--) {
        I2CClockTick((reg >> i) & 1);
    }
    if (I2CClockTick(0xFF)) PrintString("I2C reg NACK");

    // Restart Condition: Pull SDA down while SCL up
    SCL_HIGH;
    I2C_DELAY;
    SDA_LOW;
    I2C_DELAY;

    // Write device addr bit
    for (i = 6; i >= 0; i--) {
        I2CClockTick((device >> i) & 1);
    }
    I2CClockTick(1); // R/W = Read
    if (I2CClockTick(0xFF)) PrintString("I2C Addr2 NACK");

    // Read data
    uint8_t data = 0;
    for (i = 7; i >= 0; i--) {
        data = (data << 1) | I2CClockTick(0xFF);
    }
    I2CClockTick(1); // send NACK

    // Stop Condition: Pull SDA down, then release SCL up & release SDA
    SDA_LOW;
    I2C_DELAY;
    SCL_HIGH;
    I2C_DELAY;
    SDA_HIGH;
    I2C_DELAY;

    return data;
}

void I2CSet(uint8_t device, uint8_t reg, uint8_t mask, uint8_t value) {
    // Perform a Read-Modify-Write
    uint8_t content = I2CRead(device, reg);
    content &= ~mask;
    content |= value & mask;
    I2CWrite(device, reg, content);

    // Validate content
    uint8_t verify = I2CRead(device, reg);
    if (verify != content) {
        PrintString("I2CSet Mismatch:\r\n");
        PrintString("  Register = "); PrintNumber(reg, 100); PrintString("\r\n");
        PrintString("  Expected = "); PrintNumber(content, 100); PrintString("\r\n");
        PrintString("  Actual   = "); PrintNumber(verify, 100); PrintString("\r\n");
        while (1) {}
    }
}