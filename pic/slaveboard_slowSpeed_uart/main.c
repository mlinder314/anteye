/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
#if defined(__XC16__)
    #include <xc.h>
#elif defined(__C30__)
    #if defined(__PIC24E__)
    	#include <p24Exxxx.h>
    #elif defined (__PIC24F__)||defined (__PIC24FK__)
	#include <p24Fxxxx.h>
    #elif defined(__PIC24H__)
	#include <p24Hxxxx.h>
    #endif
#endif

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition       */

#include "system.h"        /* System funct/params, like osc/peripheral config */
#include "user.h"          /* User funct/params, such as InitApp              */

#include <wdt.h>

/******************************************************************************/
/* Global Variable Declaration                                                */
/******************************************************************************/

unsigned char UART_TX_RING[UART_TX_SIZE];
volatile uint16_t UART_TX_READ_INDEX = 0;
volatile uint16_t UART_TX_WRITE_INDEX = 0;

/******************************************************************************/
/* Main Program                                                               */
/******************************************************************************/

int16_t main(void)
{
    EnableWDT(WDT_DISABLE);

    /* Configure the oscillator for the device */
    ConfigureOscillator();

    /* Initialize subcomponents */
    AD1PCFG = -1; // disable all analog pins (necessary for PORT reads)
    InitUART();
    InitI2C();
    
    /* Initialize IO ports and peripherals */
    InitApp();

    while(1)
    {
        Loop();
    }
}
