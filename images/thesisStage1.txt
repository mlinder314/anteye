================
slave board
================
* need 1 cpu/camera to reduce data
* have to design offline -> use large MCU with reserves
* try to use hardware interfaces for bit handling -> parallel master port
* which MCU? need at least ~23 mhz processing power
* avrs = good & easy to use, but slim selection + expensive
* TI = hard to get
* pic = good selection & variable hardware pinout -> can change after producing pcb
* soic? easier to solder by hand. but takes up too much space -> QFN and hot air gun
* need professionally etched pcb

* broken connections -> hard to figure out
* need proper hotairgun setting -> burnt boards, loose connections
* no answer from camera -> why? .. hard to debug!
* chips = error resistant .. heat resistant..

* reset line wrong in datasheet (inverted -> no signal)
* fpc connector bottom mount (although it looks like its dual mount/has pins on both sides; 45 degree angle -> need to replace with top mount)

* pins as analog by default
* i2c buggy (silicon error)

* problem: high PCLK = uC cannot handle the data
* Using PMP speeds things up (since buffered), but still too much data
* scaling is non-linear (still high pclk, but large gap inbetween)
* issue: sampling faster than PCK = 8 mhz is hard ->   
    BOFEOF r=08298 u=00000 d=02761 f=01288
* 80*60*2=9600 pixels expected:
    BOFEOF r=02684 u=00000 d=01315 f=04792 @ 24 MHz/30 fps
* camera only reports zero at low clock speeds

* weird clock alignment issues (PCLK not linked to HREF -> loose of rows, ..
  -> could count HREF, but pixel counting = takes too much time
* PCLK div determines how many pixels per HSYNC -> need div 16
* hstart/stop only control the shown window, not the HSYNC signal

* Trying to solve the HSYNC/HREF thingy:


	image: 80*60 = 4800 pixels (9600 bytes)

	=== OLD ===
	60 HREFs
	9006 pixels

	=> we count 9000 bytes, we send 9244, we should see 9600, logic analyzer sees 9006
	=> fix this uart bug first:
		if i send 4500, 4500 pixels arrive
		if i send 9000, 9240 pixels arrive

		=> multi-threading/sync problem in the UART code; need to use two different indices for the UART ring buffer


	* "PCLK Reverse" makes the image look nice (less pixel noise) 
		-> makes sense since it's otherwise HIGH when idle (which is a read strobe for us)

	== AFTER SWITCHING TO HSYNC ==
	514 HREFs
	90000 pixels

	* Can Disable HSYNC alltogether (using [0-1] of EXHCH)
	* Can only adjust Pixels via PCLK divider (need pclk 16 for 80 pixels/ro)
	* Cannot adjust HREFs (can add more, but cannot remove them?)

	80*60: HSTART = 0x09, HSTOP = 0x00

* Can scale image down using digital zoomeout (SCALING_YSC) and PCLK
* Raspberry Pi as I2C controller
* FPS correlates directly with width

* we can handle: 8us edge-to-edge -> 128 clock cycles per byte
	-> that means we have 21 clock cycles @ 30hz


* synchronous uart loop works better
* interrupt bug: did not clear flag -> caused endless loop -> now can sample with 20 Hz @ 40x30
* race condition: 8 * 1 / 1 would work, 8 * 4 / 4 not -> 62ns difference -> PMP overflow caused hang
* even counting pixels in the ISR is too much
* cannot do better:

161:                ** few clock cycles to deal with the data. */
162:               void __attribute__ ((interrupt,no_auto_psv,shadow)) _PMPInterrupt(void) {
163:                   // Read pixels ASAP so that module  can continue
164:                   unsigned int pixel1 = PMDIN1;
000ABA  803040     MOV PMDIN1, W0
165:                   unsigned int pixel2 = PMDIN2;
000ABC  803057     MOV PMDIN2, W7
166:                   mPMP_Clear_Intr_Status_Bit;
000ABE  A9A089     BCLR 0x89, #5
167:               

* can now do: 21 Hz @ 20 x 30, or 12 Hz @ 40 x 30
* At this frequency we can execute 14.88 clock cycles in the 0.93us pixel tick
  --> We need 4 clock cycles to enter the interrupt, and 3 cycles to execute the instructions = 7 clock cycles; we execute 13 instructions in total (17 instructions combined delay)
--> this means we should be able to deal with 0.6us clock ticks though?



ISR optimizations:

C code --> 17 cycles (with optimizations)
=============

asm code 1 --> 10 cycles
=============

__PMPInterrupt: 
    push.s ; save w0-w3 registers to shadows (warning - only one copy exists)
    mov _framePixelPtr, w0 ; load pointer - must be absolute
    mov PMDIN1, w1
    mov PMDIN2, w2
    mov w1,[w0++]
    mov w2,[w0++]
    mov w0, _framePixelPtr ; save new pointer
    pop.s ; restore registers
    bclr IFS2, #PMPIF ; whatever you need to clear interrupt
    retfie

asm code 2 --> 6 cycles
=============

;; Interrupt implementation WITH two reserved registers: 6 cycles
__PMPInterrupt:
    mov PMDIN1, w12 ; pixel buffer
    mov w12,[w13++] ; pixel buffer -> frame pixel ptr
    mov PMDIN2, w12
    mov w12,[w13++]
    bclr IFS2, #PMPIF ; whatever you need to clear interrupt
    retfie


more asm
==============

 ;; 2+2+2+2 = 8 cycles
    ;mov PMDIN1, w12 ; pixel buffer
    ;mov w12,[w13++] ; pixel buffer -> frame pixel ptr
    ;mov PMDIN2, w12
    ;mov w12,[w13++]
    ;bclr IFS2, #PMPIF ; clear interrupt

    ;; 2+2+3 = 7 cycles
    ;mov PMDIN1, w10
    ;mov PMDIN2, w11
    ;mov.d w10, [w12++]
    ;bclr IFS2, #PMPIF ; clear interrupt

    ;; 2+2 = 4 cycles -- as close as it'll get to real DMA
    mov [w13++],[w12++] ; move PMDIN1 and re-point w12 to PMDIN2
    bclr IFS2, #PMPIF
    mov [w13--],[w12++] ; move PMDIN2 and re-point w12 to PMDIN1


SPI:
* Use mode=11 since pulling up is "harder" than setting a pin to zero