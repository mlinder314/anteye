#include <SPI.h>

void setup() {
  Serial.begin(9600);
  Serial.println("Arduino here. Watching SPI:");
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE3);
  SPI.setClockDivider(SPI_CLOCK_DIV16);
}  

int i = 0;

void loop() {
  unsigned int res = SPI.transfer('x');
  Serial.print((char)res);
  if (i++ % 80 == 0)
    Serial.println();
  else
    Serial.print(" ");
}
